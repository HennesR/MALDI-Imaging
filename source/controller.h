#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QQuickView>

#include "dataset.h"
#include "parallelcoordinatesspace.h"
#include "projectionspace.h"
#include "scatterspace.h"
#include "boxplotspace.h"
#include "spectrumspace.h"
#include "imagespace.h"

class Controller : public QObject
{
    Q_OBJECT
public:
    Controller( const QQuickView& view );

private:
    DataSet*                    _dataSet                    = nullptr;
    ParallelCoordinatesSpace*   _parallelCoordinatesSpace   = nullptr;
    ProjectionSpace*            _projectionSpace            = nullptr;
    ScatterSpace*               _scatterSpace               = nullptr;
    BoxPlotSpace*               _boxPlotSpace               = nullptr;
    SpectrumSpace*              _spectrumSpace              = nullptr;
    ImageSpace*                 _imageSpace                 = nullptr;

private slots:
    void openFile( QUrl fileUrl );
};

#endif // CONTROLLER_H
