#include "controller.h"

#include <fstream>
#include <iostream>

#ifdef __APPLE__
#define MALDI_DATA_FILE "/Users/florian/Desktop/GitLab/MALDI-Imaging/dataset/BN.txt"
#define DATA_FILE_COORDINATES "/Users/florian/Desktop/GitLab/MALDI-Imaging/dataset/bn2p50_tsne.csv"

#else
#define MALDI_DATA_FILE "../dataset/BN.txt"
#define DATA_FILE_COORDINATES "../dataset/bn2p50_tsne.csv"

#endif

Controller::Controller( const QQuickView& view )
{
    // Find children
    _dataSet = view.findChild<DataSet*>( "dataSet" );

    _parallelCoordinatesSpace = view.findChild<ParallelCoordinatesSpace*>( "parallelCoordinatesSpace" );
    _parallelCoordinatesSpace->setDataSet( _dataSet );

    _projectionSpace = view.findChild<ProjectionSpace*>( "projectionSpace" );
    _projectionSpace->initialize();

    _scatterSpace = view.findChild<ScatterSpace*>( "scatterSpace" );
    _scatterSpace->setDataSet( _dataSet );

    _boxPlotSpace = view.findChild<BoxPlotSpace*>( "boxPlotSpace" );
    _boxPlotSpace->setDataSet( _dataSet );

    _spectrumSpace = view.findChild<SpectrumSpace*>( "spectrumSpace" );
    _spectrumSpace->setDataSet( _dataSet );

    _imageSpace = view.findChild<ImageSpace*>( "imageSpace" );
    _imageSpace->setDataSet( _dataSet );

    // Establish connections
    QObject::connect( view.rootObject(), SIGNAL( openFile(QUrl) ), this, SLOT( openFile(QUrl) ));
    QObject::connect(_projectionSpace, &ProjectionSpace::hoveredLandmarksCluster, [this]( const QVector<std::pair<unsigned int, double>>& areasOfInfluence, QColor color ){
        pixelCluster cluster { color, QVector<pixel>( areasOfInfluence.size() ) };
        for( int i = 0; i < areasOfInfluence.size(); ++i ) {
            cluster.pixels[i] = { static_cast<int>( areasOfInfluence[i].first ), static_cast<float>( areasOfInfluence[i].second ) };
        }
        _imageSpace->setHighlight( { cluster } );
    });
    QObject::connect( _projectionSpace, &ProjectionSpace::selectScatterSpaceTSNEPoints, [this]( const QVector<pointCluster>& pointCluster )
    {
        std::cout << pointCluster.size() << std::endl;
        std::cout << pointCluster[0].points.size() << std::endl;

        _scatterSpace->setData(pointCluster, _dataSet->width(), _dataSet->height());
    });
    QObject::connect( _scatterSpace, &ScatterSpace::selectedPointsChanged, [this]( QVector<pixelCluster> clusters ) {
        _imageSpace->setHighlight( clusters );
    });
    QObject::connect( _boxPlotSpace, &BoxPlotSpace::dimensionsChanged, [this]( const QVector<int>& dimensions ) {
        _parallelCoordinatesSpace->setDimensions( dimensions );
    });
    QObject::connect( _spectrumSpace, &SpectrumSpace::selectedDimensionsChanged, [this]( const QVector<int>& dimensions ){
        _imageSpace->setDimensions( dimensions );
    });



    //---===### TEMPORARY ###===---//
    this->openFile( QUrl::fromLocalFile( MALDI_DATA_FILE ) );

    std::ifstream coordinates( DATA_FILE_COORDINATES );
    if( !coordinates ){
        qDebug() << "Failed to load file: " << DATA_FILE_COORDINATES;
        std::exit( EXIT_FAILURE );
    }

    pointCluster cluster { Qt::red, {} };
    cluster.points.resize( _dataSet->pixelCount() );
    for( int i = 0; i < cluster.points.size(); ++i ) {
        point& point = cluster.points[i];
        coordinates >> point.position.x >> point.position.y;
        point.pixelIndex = i;
        point.opacity = 1.0f;
    }
    coordinates.close();
    _scatterSpace->setData( { cluster }, _dataSet->width(), _dataSet->height() );
}

void Controller::openFile( QUrl fileUrl )
{
    qDebug() << "Loading file: " << fileUrl.toLocalFile();
    _dataSet->freeResources();
    _dataSet->loadFromFile( fileUrl.toLocalFile().toLocal8Bit().constData() );
    qDebug() << "image width:" << _dataSet->width() << ", image height:" << _dataSet->height() << ", mz-value count:" << _dataSet->dimensionCount();

    _projectionSpace->setDataSet( _dataSet );
}
