#include "boxplotspace.h"
#include "./external/qcustomplot.h"
#include <algorithm>
#include <numeric>

BoxPlotSpace::~BoxPlotSpace()
{
    delete _customPlot;
}
void BoxPlotSpace::setDataSet( const DataSet* pDataSet )
{
    _dataSet = pDataSet;
    QObject::connect( pDataSet, &DataSet::dataChanged, [this]{
        QVector<int> allPixels( _dataSet->pixelCount() );
        std::iota( allPixels.begin(), allPixels.end(), 0 );

        _allPixelsGroup.reset( new Group( "Missing Name", QColor() ) );
        _allPixelsGroup->setPixels( std::move( allPixels ) );

        this->updateGroupData();
    });
    QObject::connect( pDataSet, &DataSet::groupsCleared, [this]{
        for( auto group : _inputGroups ) QObject::disconnect( group, nullptr, this, nullptr );
        _inputGroups.clear();

        this->updateGroupData();
    });
}

void BoxPlotSpace::toggleInputGroup( Group* pGroup )
{
    if( this->containsInputGroup( pGroup ) ) {
        QObject::disconnect( pGroup, nullptr, this, nullptr );

        _inputGroups.removeOne( pGroup );
        if( _usingAllPixelGroup ) {
            _inputGroups.clear();
            _usingAllPixelGroup = false;
        }
        else {
            _inputGroups.push_back( _allPixelsGroup.get() );
            _usingAllPixelGroup = true;
        }

    } else if( !_inputGroups.size() || _usingAllPixelGroup ) {
        QObject::connect( pGroup, SIGNAL( pixelsChanged() ), this, SLOT( updateGroupData() ) );
        QObject::connect( pGroup, SIGNAL( colorChanged() ), this, SLOT( updateGroupColors() ) );

        _inputGroups.push_back( pGroup );
        if( _usingAllPixelGroup ) {
            _inputGroups.removeOne( _allPixelsGroup.get() );
            _usingAllPixelGroup = false;
        }
        else {
            _inputGroups.push_back( _allPixelsGroup.get() );
            _usingAllPixelGroup = true;
        }
    };

    this->updateGroupData();
}
bool BoxPlotSpace::containsInputGroup( Group* pGroup ) const
{
    return _inputGroups.contains( pGroup );
}

void BoxPlotSpace::updateGroupData()
{
    _customPlot->clearPlottables();
    if( !_inputGroups.size() ) {
        this->update();
        emit dimensionsChanged( {} );
        return;
    }

    QVector<int> dimensions = _dataSet->findImportantDimensions( _inputGroups[0], _inputGroups[1] );

    dimensions.resize( _dimensionCount );
    const int dimensionCount = dimensions.size();

    for( int i = 0; i < _inputGroups.size(); ++i )
    {
        QCPStatisticalBox* boxPlot = new QCPStatisticalBox( _customPlot->xAxis, _customPlot->yAxis );

        const auto& group = _inputGroups[i];
        const int pixelCount = group->pixels().size();
        if( !pixelCount ) continue;

        QVector<float> intensities( pixelCount );
        const auto begin = intensities.begin(), end = intensities.end();

        QVector<double> keys( dimensionCount ), minimum( dimensionCount ), lowerQuartile( dimensionCount ),
                median( dimensionCount ), upperQuartile( dimensionCount ), maximum( dimensionCount );

        for( int d = 0; d < dimensionCount; ++d )
        {
            // Gather intensities
            const int dimension = dimensions[d];
            for( int j = 0; j < pixelCount; ++j )
                intensities[j] = _dataSet->intensity( group->pixels()[j], dimension );

            // Set key
            keys[d] = 1.0 + 2.0 * d + ( i + 1.0 ) / 2;

            // Find minimum
            auto nth = intensities.begin();
            std::nth_element( begin, nth, end );
            minimum[d] = static_cast<double>( *nth );

            // Find lower quartile
            nth = intensities.end() - static_cast<int>( pixelCount / (4.0f/3.0f) ) - 1;
            std::nth_element( begin, nth, end );
            lowerQuartile[d] = static_cast<double>( *nth );

            // Find median
            nth = intensities.end() - static_cast<int>( pixelCount / 2.0f ) - 1;
            std::nth_element( begin, nth, end );
            median[d] = static_cast<double>( *nth );

            // Find upper quartile
            nth = intensities.end() - static_cast<int>( pixelCount / 4.0f ) - 1;
            std::nth_element( begin, nth, end );
            upperQuartile[d] = static_cast<double>( *nth );

            // Find maximum
            nth = intensities.end() - 1;
            std::nth_element( begin, nth, end );
            maximum[d] = static_cast<double>( *nth );
        }

        boxPlot->setData( keys, minimum, lowerQuartile, median, upperQuartile, maximum, true );
    }

    // Change x- and y-axis
    QVector<double> positions( dimensionCount );
    QVector<QString> labels( dimensionCount );
    for( int i = 0; i < dimensionCount; ++i ) {
        positions[i] = 1.5 + 2.0 * i;
        labels[i] = QString::number( static_cast<double>( _dataSet->mzValue( dimensions[i] ) ) );
    }
    QSharedPointer<QCPAxisTickerText> ticker( new QCPAxisTickerText );
    ticker->setTicks( positions, labels );
    _customPlot->xAxis->setTicker( ticker );
    _customPlot->xAxis->setRange( 0.0, 1.0 + 2.0 * dimensionCount );
    _customPlot->yAxis->rescale();

    this->updateGroupColors();
    emit dimensionsChanged( dimensions );
}
void BoxPlotSpace::updateGroupColors()
{
    for( int i = 0; i < _customPlot->plottableCount(); ++i )
        _customPlot->plottable( i )->setBrush( _inputGroups[i]->color() );

    this->update();
}

void BoxPlotSpace::componentComplete()
{
    QQuickPaintedItem::componentComplete();
    QObject::connect( this, SIGNAL( dimensionCountChanged() ), this, SLOT( updateGroupData() ) );

    _customPlot = new QCustomPlot;
    _customPlot->xAxis->setTickLabelRotation( 60.0 );
}
void BoxPlotSpace::paint( QPainter* painter )
{
    painter->drawPixmap( QPoint(), _customPlot->toPixmap( static_cast<int>( this->width() ), static_cast<int>( this->height() ) ) );
}
