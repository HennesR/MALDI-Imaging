#ifndef LASSOTOOL_H
#define LASSOTOOL_H

#include <QQuickPaintedItem>
#include <QPainter>

class LassoTool : public QQuickPaintedItem
{
public:
    void hoverMoveEvent(QHoverEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void paint(QPainter* painter) override;

    const QPolygonF& normalizedPolygon() const;
    QPolygon polygon() const;
    bool finished() const;
    void clear();

private:
    QPointF toNormalized(QPoint point) const;
    QPoint toScreen(QPointF point) const;

    QPoint      _hovered    = {};
    QPolygonF   _polygon    = {};
    bool        _finished   = false;
};

#endif // LASSOTOOL_H
