#include "lassotool.h"

void LassoTool::hoverMoveEvent(QHoverEvent* event)
{
    _hovered = event->pos();
    if(!_finished) this->update();
}
void LassoTool::mousePressEvent(QMouseEvent* event)
{
    Q_UNUSED(event)

    if( _finished ) {
        this->clear();
        _polygon.append(toNormalized(_hovered));
    } else if( _polygon.size() && QLineF(toScreen(_polygon[0]), _hovered).length() <= 5.0 ){
        _finished = true;
    } else {
        _polygon.append(toNormalized(_hovered));
    }
    this->update();
}
void LassoTool::paint(QPainter* painter)
{
    if(_polygon.size())
    {
        painter->setPen( QColor( 255, 240, 0 ) );
        painter->setRenderHint(QPainter::Antialiasing, true);

        const QPolygon polygon = this->polygon();
        painter->drawPolyline(polygon);

        if(_finished) {
            painter->drawLine(polygon.back(), polygon[0]);
        } else if( QLineF(polygon[0], _hovered).length() <= 5.0 ) {
            painter->drawLine(polygon.back(), polygon[0]);
            painter->drawEllipse(polygon[0], 3, 3);
        } else {
            painter->drawLine(polygon.back(), _hovered);
        }
    }
}

const QPolygonF& LassoTool::normalizedPolygon() const
{
    return _polygon;
}
QPolygon LassoTool::polygon() const
{
    QPolygon polygon(_polygon.size());
    for(int i = 0; i < _polygon.size(); ++i)
        polygon[i] = toScreen(_polygon[i]);

    return polygon;
}
bool LassoTool::finished() const
{
    return _finished;
}
void LassoTool::clear()
{
    _polygon.clear();
    _finished = false;
}

QPointF LassoTool::toNormalized(QPoint point) const
{
    return QPointF( point.x() / size().width(), point.y() / size().height() );
}
QPoint LassoTool::toScreen(QPointF point) const
{
    return QPoint(point.x() * size().width(), point.y() * size().height());
}
