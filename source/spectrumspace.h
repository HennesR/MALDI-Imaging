#ifndef SPECTRUMSPACE_H
#define SPECTRUMSPACE_H

#include <QQuickPaintedItem>
#include "dataset.h"

class QCustomPlot;
class QCPBars;

class SpectrumSpace : public QQuickPaintedItem
{
    Q_OBJECT
public:
    ~SpectrumSpace() override;
    void setDataSet( const DataSet* pDataSet );

    Q_INVOKABLE void toggleInputGroup( Group* pGroup );
    Q_INVOKABLE bool containsInputGroup( Group* pGroup ) const;

signals:
    void selectedDimensionsChanged( const QVector<int>& dimensions );

private slots:
    void updateGroupData();
    void updateGroupColors();

private:
    void componentComplete() override;
    void paint( QPainter* painter ) override;
    void wheelEvent( QWheelEvent* event ) override;
    void mousePressEvent( QMouseEvent* event ) override;

    const DataSet*          _dataSet = nullptr;
    QVector<const Group*>   _inputGroups {};

    QCustomPlot*    _customPlot = nullptr;
    QCPBars*        _barChart   = nullptr;
};

#endif // SPECTRUMSPACE_H
