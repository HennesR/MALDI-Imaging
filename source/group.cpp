#include "group.h"
#include <QDebug>

Group::Group( QString name, QColor color ) : _name( std::move( name ) ), _color( color )
{}

bool Group::loadFromFile( std::ifstream& in )
{
    // Read name
    int32_t nameSize;
    if( !in.read( reinterpret_cast<char*>( &nameSize ), sizeof( int32_t ) ) ) return false;
    char* name = new char[nameSize + 1];
    if( !in.read( name, nameSize ) ) return false;
    _name = QString( name );
    delete[] name;

    // Read group color
    struct { uint8_t r, g, b, a; } color;
    if( !in.read( reinterpret_cast<char*>( &color ), sizeof( color ) ) ) return false;
    _color = QColor( color.r, color.g, color.b, color.a );

    // Read group pixels
    int32_t pixelCount;
    if( !in.read( reinterpret_cast<char*>( &pixelCount ), sizeof( int32_t ) ) ) return false;
    QVector<int32_t> pixels( pixelCount );
    if( !in.read( reinterpret_cast<char*>( pixels.data() ), pixelCount * sizeof( int32_t ) ) ) return false;

    _pixels.resize( pixelCount );
    for( int32_t i = 0; i < pixelCount; ++i )
        _pixels[i] = pixels[i];

    return true;
}
void Group::saveToFile( std::ofstream& out ) const
{
    // Write name
    int32_t nameSize = _name.size();
    out.write( reinterpret_cast<const char*>( &nameSize ), sizeof( int32_t ) );
    out.write( _name.toLocal8Bit().constData(), nameSize );

    // Write color
    struct{ uint8_t r, g, b, a; } color;
    color.r = _color.red();
    color.g = _color.green();
    color.b = _color.blue();
    color.a = _color.alpha();
    out.write( reinterpret_cast<const char*>( &color ), sizeof( color ) );

    // Write pixels
    int32_t pixelCount = _pixels.size();
    QVector<int32_t> pixels( pixelCount );
    for( int32_t i = 0; i < pixelCount; ++i )
        pixels[i] = _pixels[i];
    out.write( reinterpret_cast<const char*>( &pixelCount ), sizeof( int32_t ) );
    out.write( reinterpret_cast<const char*>( pixels.data() ), pixelCount * sizeof( int32_t ) );
}

const QString& Group::name() const
{
    return _name;
}
void Group::setName( QString name )
{
    _name = std::move( name );
    emit nameChanged();
}

QColor Group::color() const
{
    return _color;
}
void Group::setColor( QColor color )
{
    _color = color;
    emit colorChanged();
}

const QVector<int>& Group::pixels() const
{
    return _pixels;
}
void Group::setPixels( QVector<int> pixels )
{
    _pixels = std::move( pixels );
    emit pixelsChanged();
}
void Group::addPixels( const QVector<int>& pixels )
{
    bool changed = false;
    for( auto pixel : pixels ) {
        if( !_pixels.contains( pixel ) ) {
            _pixels.append( pixel );
            changed = true;
        }
    }

    if( changed ) emit pixelsChanged();
}
void Group::removePixels( const QVector<int>& pixels )
{
    bool changed = false;
    for( auto pixel : pixels )
        if( _pixels.removeOne( pixel ) )
            changed = true;

    if( changed ) emit pixelsChanged();
}
