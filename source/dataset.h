#ifndef DATASET_H
#define DATASET_H

#include <QObject>
#include <QList>
#include <QDebug>
#include <QUrl>
#include "group.h"

class DataSet : public QObject
{
    Q_OBJECT
    Q_PROPERTY( QList<QObject*> groups READ groups NOTIFY groupsChanged )

public:
    void loadFromFile(const char* filepath);
    void freeResources();
    void sortByMzValues();

    float*          mzValues = nullptr;
    float*          mzIntensities = nullptr;
    unsigned int    mzValuesSize;
    unsigned int    spectrumsSize;
    unsigned int    imageWidth;
    unsigned int    imageHeight;



    int width() const;
    int height() const;
    int pixelCount() const;
    int dimensionCount() const;

    float mzValue( int dimension ) const;
    float intensity( int pixel, int dimension ) const;

    Q_INVOKABLE void loadGroups( QUrl fileUrl );
    Q_INVOKABLE void saveGroups( QUrl fileUrl ) const;

    QList<QObject*> groups();
    Q_INVOKABLE void addGroup();
    void addGroup( QString name );

    QVector<int> findImportantDimensions( const Group* a, const Group* b ) const;

signals:
    void dataChanged();

    void groupsChanged();
    void groupsCleared();

private:
    void clearGroups();

    QString         _name {};
    QVector<Group*> _groups {};
};

#endif // DATASET_H
