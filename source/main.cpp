
#include <QGuiApplication>
#include <QApplication>
#include <QQuickView>

#include <iostream>
#include "controller.h"
#include "lassotool.h"

#include "shared/Point/Point.h"


int main(int argc, char *argv[])
{
    srand(time(NULL));

    qmlRegisterType<DataSet>("MALDI_Imaging", 1, 0, "DataSet");
    qmlRegisterType<Group>("MALDI_Imaging", 1, 0, "Group");

    qmlRegisterType<ParallelCoordinatesSpace>("MALDI_Imaging", 1, 0, "ParallelCoordinatesSpace");
    qmlRegisterType<ProjectionSpace>("MALDI_Imaging", 1, 0, "ProjectionSpace");
    qmlRegisterType<ScatterSpace>("MALDI_Imaging", 1, 0, "ScatterSpace");
    qmlRegisterType<BoxPlotSpace>("MALDI_Imaging", 1, 0, "BoxPlotSpace");
    qmlRegisterType<SpectrumSpace>("MALDI_Imaging", 1, 0, "SpectrumSpace");
    qmlRegisterType<ImageSpace>("MALDI_Imaging", 1, 0, "ImageSpace");
    qmlRegisterType<LassoTool>("MALDI_Imaging", 1, 0, "LassoTool");

    QApplication app(argc, argv);
    QQuickView view(QUrl("qrc:/qml/main.qml"));
    view.setTitle("MALDI-Imaging");
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    Controller controller( view );

    view.show();
    return app.exec();
}
