QT += quick printsupport
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    CyteGuideTreeNode/calculateAreaOfInfluence/calculateAreaOfInfluence.cpp \
    CyteGuideTreeNode/drillDown/drillDown.cpp \
    CyteGuideTreeNode/saveAreaOfInfluence/saveAreaOfInfluence.cpp \
    CyteGuideTreeNode/tSNE/bhtsne/bhtsne.cpp \
    CyteGuideTreeNode/tSNE/bhtsne/sptree.cpp \
    CyteGuideTreeNode/tSNE/bhtsne/tsne_main.cpp \
    CyteGuideTreeNode/tSNE/calculateDistanceMatrix/calculateDistanceMatrix.cpp \
    CyteGuideTreeNode/tSNE/calculateHighDimensionalSimilaritiesMatrix/calculateHighDimensionalSimilaritiesMatrix.cpp \
    CyteGuideTreeNode/tSNE/calculateKullbackLeiblerDivergence/calculateKullbackLeiblerDivergence.cpp \
    CyteGuideTreeNode/tSNE/calculateLowDimensionalSimilaritiesMatrix/calculateLowDimensionalSimilaritiesMatrix.cpp \
    CyteGuideTreeNode/tSNE/generateRandomPoints/generateRandomPoints.cpp \
    CyteGuideTreeNode/tSNE/normalizePoints/normalizePoints.cpp \
    CyteGuideTreeNode/tSNE/printSimilarityMatrix/printSimilarityMatrix.cpp \
    CyteGuideTreeNode/tSNE/tSNE.cpp \
    CyteGuideTreeNode/CyteGuideTreeNode.cpp \
    external/qcustomplot.cpp \
    hierachicalSNE/calculateDistancesToNeighbours/calculateDistancesToNeighbours.cpp \
    hierachicalSNE/calculateInfluenceMatrix/calculateInfluenceMatrix.cpp \
    hierachicalSNE/calculateKNeighboursSimilarities/calculateKNeighboursSimilarities.cpp \
    hierachicalSNE/calculateLandmarks/calculateEquilibriumDistribution/calculateEquilibriumDistribution.cpp \
    hierachicalSNE/calculateLandmarks/selectLandmarks/selectLandmarks.cpp \
    hierachicalSNE/calculateLandmarks/calculateLandmarks.cpp \
    hierachicalSNE/calculateNextTransitionMatrix/calculateNextTransitionMatrix.cpp \
    hierachicalSNE/calculateSimilaritiesMatrix/calculateSimilaritiesMatrix.cpp \
    hierachicalSNE/calculateWeights/calculateWeights.cpp \
    hierachicalSNE/constructKNNGraph/constructKMeansTree/constructKMeansTree.cpp \
    hierachicalSNE/constructKNNGraph/searchKMeansTree/searchKMeansTree.cpp \
    hierachicalSNE/constructKNNGraph/shared/KMeansTreeNode/KMeansTreeNode.cpp \
    hierachicalSNE/constructKNNGraph/constructKNNGraph.cpp \
    hierachicalSNE/shared/KNNGraph/KNNGraphNode/KNNGraphNode.cpp \
    hierachicalSNE/shared/KNNGraph/KNNGraph.cpp \
    hierachicalSNE/shared/loadVector/loadVector.cpp \
    hierachicalSNE/shared/MarkovChain/MarkovChain.cpp \
    hierachicalSNE/shared/saveVector/saveVector.cpp \
    hierachicalSNE/hierachicalSNE.cpp \
    shared/HSNEResults/HSNEResult.cpp \
    shared/kMeans/kMeans.cpp \
    shared/Matrix/Matrix.cpp \
    shared/Point/Point.cpp \
    shared/printProgress/printProgress.cpp \
    boxplotspace.cpp \
    controller.cpp \
    dataset.cpp \
    group.cpp \
    imagespace.cpp \
    lassotool.cpp \
    main.cpp \
    parallelcoordinatesspace.cpp \
    projectionspace.cpp \
    scatterspace.cpp \
    spectrumspace.cpp \
    alglib/alglibinternal.cpp \
    alglib/alglibmisc.cpp \
    alglib/ap.cpp \
    alglib/dataanalysis.cpp \
    alglib/diffequations.cpp \
    alglib/fasttransforms.cpp \
    alglib/integration.cpp \
    alglib/interpolation.cpp \
    alglib/linalg.cpp \
    alglib/optimization.cpp \
    alglib/solvers.cpp \
    alglib/specialfunctions.cpp \
    alglib/statistics.cpp



RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    CyteGuideTreeNode/calculateAreaOfInfluence/calculateAreaOfInfluence.hpp \
    CyteGuideTreeNode/drillDown/drillDown.hpp \
    CyteGuideTreeNode/saveAreaOfInfluence/saveAreaOfInfluence.hpp \
    CyteGuideTreeNode/tSNE/bhtsne/sptree.h \
    CyteGuideTreeNode/tSNE/bhtsne/tsne.h \
    CyteGuideTreeNode/tSNE/bhtsne/vptree.h \
    CyteGuideTreeNode/tSNE/calculateDistanceMatrix/calculateDistanceMatrix.hpp \
    CyteGuideTreeNode/tSNE/calculateHighDimensionalSimilaritiesMatrix/calculateHighDimensionalSimilaritiesMatrix.hpp \
    CyteGuideTreeNode/tSNE/calculateKullbackLeiblerDivergence/calculateKullbackLeiblerDivergence.hpp \
    CyteGuideTreeNode/tSNE/calculateLowDimensionalSimilaritiesMatrix/calculateLowDimensionalSimilaritiesMatrix.hpp \
    CyteGuideTreeNode/tSNE/generateRandomPoints/generateRandomPoints.hpp \
    CyteGuideTreeNode/tSNE/normalizePoints/normalizePoints.hpp \
    CyteGuideTreeNode/tSNE/printSimilarityMatrix/printSimilarityMatrix.hpp \
    CyteGuideTreeNode/tSNE/tSNE.h \
    CyteGuideTreeNode/CyteGuideTreeNode.hpp \
    external/qcustomplot.h \
    hierachicalSNE/calculateDistancesToNeighbours/calculateDistancesToNeighbours.hpp \
    hierachicalSNE/calculateInfluenceMatrix/calculateInfluenceMatrix.hpp \
    hierachicalSNE/calculateKNeighboursSimilarities/calculateKNeighboursSimilarities.hpp \
    hierachicalSNE/calculateLandmarks/calculateEquilibriumDistribution/calculateEquilibriumDistribution.hpp \
    hierachicalSNE/calculateLandmarks/selectLandmarks/selectLandmarks.hpp \
    hierachicalSNE/calculateLandmarks/calculateLandmarks.hpp \
    hierachicalSNE/calculateNextTransitionMatrix/calculateNextTransitionMatrix.hpp \
    hierachicalSNE/calculateSimilaritiesMatrix/calculateSimilaritiesMatrix.hpp \
    hierachicalSNE/calculateWeights/calculateWeights.hpp \
    hierachicalSNE/constructKNNGraph/constructKMeansTree/constructKMeansTree.hpp \
    hierachicalSNE/constructKNNGraph/searchKMeansTree/searchKMeansTree.hpp \
    hierachicalSNE/constructKNNGraph/shared/KMeansTreeNode/KMeansTreeNode.hpp \
    hierachicalSNE/constructKNNGraph/constructKNNGraph.hpp \
    hierachicalSNE/shared/KNNGraph/KNNGraphNode/KNNGraphNode.hpp \
    hierachicalSNE/shared/KNNGraph/KNNGraph.hpp \
    hierachicalSNE/shared/loadVector/loadVector.hpp \
    hierachicalSNE/shared/MarkovChain/MarkovChain.hpp \
    hierachicalSNE/shared/saveVector/saveVector.hpp \
    hierachicalSNE/hierachicalSNE.hpp \
    shared/HSNEResults/HSNEResult.hpp \
    shared/kMeans/kMeans.hpp \
    shared/Matrix/Matrix.hpp \
    shared/Point/Point.h \
    shared/printProgress/printProgress.hpp \
    boxplotspace.h \
    controller.h \
    dataset.h \
    group.h \
    imagespace.h \
    lassotool.h \
    parallelcoordinatesspace.h \
    projectionspace.h \
    scatterspace.h \
    shared.h \
    spectrumspace.h \
    utility.h \
    alglib/alglibinternal.h \
    alglib/alglibmisc.h \
    alglib/ap.h \
    alglib/dataanalysis.h \
    alglib/diffequations.h \
    alglib/fasttransforms.h \
    alglib/integration.h \
    alglib/interpolation.h \
    alglib/linalg.h \
    alglib/optimization.h \
    alglib/solvers.h \
    alglib/specialfunctions.h \
    alglib/statistics.h \
    alglib/stdafx.h


DISTFILES +=
