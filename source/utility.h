#ifndef UTILITY_H
#define UTILITY_H

#include <vector>
#include <string>

inline std::vector<std::string> tokenizeString( const std::string& str, const std::string& delims = " " )
{
    std::vector<std::string> tokens;
    std::size_t current, previous = 0;
    current = str.find_first_of( delims );

    while( current != std::string::npos )
    {
        tokens.push_back( str.substr( previous, current - previous ) );
        previous = current + 1;
        current = str.find_first_of( delims, previous );
    }

    tokens.push_back( str.substr( previous, current - previous ) );

    return tokens;
}

#endif // UTILITY_H
