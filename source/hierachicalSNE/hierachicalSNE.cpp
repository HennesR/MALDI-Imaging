//
//  hierachicalSNE.cpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "hierachicalSNE.hpp"

#include "../shared/Point/Point.h"
#include "../shared/Matrix/Matrix.hpp"

#include "shared/saveVector/saveVector.hpp"
#include "shared/loadVector/loadVector.hpp"
#include "constructKNNGraph/constructKNNGraph.hpp"
#include "calculateDistancesToNeighbours/calculateDistancesToNeighbours.hpp"
#include "calculateKNeighboursSimilarities/calculateKNeighboursSimilarities.hpp"
#include "calculateSimilaritiesMatrix/calculateSimilaritiesMatrix.hpp"
#include "calculateLandmarks/calculateLandmarks.hpp"
#include "calculateInfluenceMatrix/calculateInfluenceMatrix.hpp"
#include "calculateWeights/calculateWeights.hpp"
#include "calculateNextTransitionMatrix/calculateNextTransitionMatrix.hpp"

#include <vector>
#include <iostream>
#include <cstdio>
#include <ctime>
#include <fstream>


bool isFileExistent(std::string fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}

void hierachicalSNE(HSNEResults* hsneResults, std::string savesBaseDirectory, Point* points, unsigned int maximumScale, unsigned int pointsSize, unsigned pointsDimension, unsigned int numberOfNeighbours, double landmarksTreshhold)
{
    std::string distanceMatrixFilePath = savesBaseDirectory + "distance_matrix.txt";

    if(!isFileExistent(distanceMatrixFilePath))
    {
        unsigned int* neighboursIndices = new unsigned int[pointsSize * numberOfNeighbours];
        constructKNNGraph(neighboursIndices, points, pointsSize, pointsDimension, numberOfNeighbours);

        std::cout << "KNN Graph fertig!" << std::endl;

        double* distancesToNeighbours = new double[pointsSize * numberOfNeighbours];
        calculateDistancesToNeighbours(distancesToNeighbours, points, pointsSize, neighboursIndices, numberOfNeighbours);

        Matrix distanceMatrix(pointsSize, pointsSize);

        for(unsigned int i=0; i<pointsSize; i++)
        {
            for(unsigned int j=0; j<numberOfNeighbours; j++)
            {
                double value = distancesToNeighbours[i * numberOfNeighbours + j];
                distanceMatrix.set(i, neighboursIndices[i * numberOfNeighbours + j], value);
            }
        }

        distanceMatrix.saveToFile(distanceMatrixFilePath);

        delete[] neighboursIndices;
        delete[] distancesToNeighbours;
    }

    Matrix distanceMatrix(pointsSize, pointsSize);
    distanceMatrix.loadFromFile(distanceMatrixFilePath);

    double* distancesToNeighbours = new double[pointsSize * numberOfNeighbours];
    unsigned int* neighboursIndices = new unsigned int[pointsSize * numberOfNeighbours];

    for(unsigned int i=0; i<pointsSize; i++)
    {
        std::vector<std::pair<unsigned int, double>> column = distanceMatrix.atColumn(i);
        int counter = 0;

        for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
        {
            std::pair<unsigned int, double> entry = *it;
            distancesToNeighbours[i * numberOfNeighbours + counter] = entry.second;
            neighboursIndices[i * numberOfNeighbours + counter] = entry.first;

            counter++;
        }
    }

    double* neigboursSimilarities = new double[pointsSize * numberOfNeighbours];
    calculateKNeighboursSimilarities(neigboursSimilarities, distancesToNeighbours, pointsSize, neighboursIndices, numberOfNeighbours);

    // set up lowest scale landMarks = normal points
    for(unsigned int i=0; i<pointsSize; i++)
    {
        hsneResults->localLandmarksIndices[0].push_back(i);
    }

    // t = transitionMatrix = similaritiesMatrix = Finite Markov Chain
    std::vector<double>* weightsAtAllScales = new std::vector<double>[maximumScale];

    hsneResults->transitionMatrices[0] = calculateSimilaritiesMatrix(neigboursSimilarities, pointsSize, neighboursIndices, numberOfNeighbours);
    weightsAtAllScales[0] = calculateWeights(pointsSize);

    hsneResults->transitionMatrices[0].saveToFile(savesBaseDirectory + "transition_matrix_0_" + std::to_string(numberOfNeighbours) + "_" + std::to_string(landmarksTreshhold) + ".txt");

    for(unsigned int scale=1; scale < maximumScale; scale++)
    {
        // IMPORTANT: influenceMatrices[0] doesn't exist!
        {
            std::string landmarksVectorFilePath = savesBaseDirectory + "landmark_vector_" + std::to_string(scale) + "_" + std::to_string(numberOfNeighbours) + "_" + std::to_string(landmarksTreshhold) + ".txt";

            if(isFileExistent(landmarksVectorFilePath))
            {
                hsneResults->localLandmarksIndices[scale] = loadVector(landmarksVectorFilePath);
            }
            else
            {
                hsneResults->localLandmarksIndices[scale] = calculateLandmarks(hsneResults->transitionMatrices[scale - 1], landmarksTreshhold);
                saveVector(landmarksVectorFilePath, hsneResults->localLandmarksIndices[scale]);
            }

            std::cout << "LM: " << hsneResults->localLandmarksIndices[scale].size() << std::endl;
        }

        {
            std::string influenceMatrixFilePath = savesBaseDirectory + "influence_matrix_" + std::to_string(scale) + "_" + std::to_string(numberOfNeighbours) + "_" + std::to_string(landmarksTreshhold) + ".txt";

            if(isFileExistent(influenceMatrixFilePath))
            {
                std::cout << "Influence Matrix is existent" << std::endl;

                unsigned int lastLandmarksSize = (scale == 1) ? pointsSize : (unsigned int)(hsneResults->localLandmarksIndices[scale - 1].size());
                unsigned int currentLandmarksSize = (unsigned int)(hsneResults->localLandmarksIndices[scale].size());

                hsneResults->influenceMatrices[scale] = Matrix(lastLandmarksSize, currentLandmarksSize);
                hsneResults->influenceMatrices[scale].loadFromFile(influenceMatrixFilePath);
            }
            else
            {
                std::cout << "Influence Matrix is NOT existent" << std::endl;

                hsneResults->influenceMatrices[scale] = calculateInfluenceMatrix(hsneResults->transitionMatrices[scale - 1], hsneResults->localLandmarksIndices[scale]);
                hsneResults->influenceMatrices[scale].saveToFile(influenceMatrixFilePath);
            }
        }

        weightsAtAllScales[scale] = calculateWeights(hsneResults->influenceMatrices[scale], weightsAtAllScales, scale);

        {
            std::string transitionMatrixFilePath = savesBaseDirectory + "transition_matrix_" + std::to_string(scale) + "_" + std::to_string(numberOfNeighbours) + "_" + std::to_string(landmarksTreshhold) + ".txt";

            if(isFileExistent(transitionMatrixFilePath))
            {
                std::cout << "Transition Matrix is existent" << std::endl;

                unsigned int currentLandmarksSize = (unsigned int)(hsneResults->localLandmarksIndices[scale].size());

                hsneResults->transitionMatrices[scale] = Matrix(currentLandmarksSize, currentLandmarksSize);
                hsneResults->transitionMatrices[scale].loadFromFile(transitionMatrixFilePath);
            }
            else
            {
                std::cout << "Transition Matrix is NOT existent" << std::endl;

                hsneResults->transitionMatrices[scale] = calculateNextTransitionMatrix(hsneResults->localLandmarksIndices[scale], hsneResults->influenceMatrices[scale], weightsAtAllScales[scale - 1], scale);
                hsneResults->transitionMatrices[scale].saveToFile(transitionMatrixFilePath);
            }
        }

        std::cout << (scale + 1) << ": " << hsneResults->localLandmarksIndices[scale].size() << std::endl;
    }

    delete[] distancesToNeighbours;
    delete[] neighboursIndices;
    delete[] neigboursSimilarities;
    delete[] weightsAtAllScales;


    std::cout << "hSNE finished! " << std::endl;
}
