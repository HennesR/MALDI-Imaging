//
//  MarkovChain.hpp
//  hSNE
//
//  Created by Florian Fechner on 07.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef MarkovChain_hpp
#define MarkovChain_hpp

#include <vector>

class MarkovChain
{
    public:
        std::pair<unsigned int, std::vector<std::pair<unsigned int, double>>>* entries; // pair<startIndex, vector<nonZeroPropabilityPoints>> & pair<destinationIndex, similarity>
        unsigned int entriesSize;
    
        MarkovChain();
        ~MarkovChain();
    
        void addEntry(unsigned int startIndex, std::vector<std::pair<unsigned int, double>> destinations);
};

#endif /* MarkovChain_hpp */
