//
//  loadVector.cpp
//  hSNE
//
//  Created by Florian Fechner on 26.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "loadVector.hpp"

#include <iostream>
#include <fstream>

std::vector<unsigned int> loadVector(std::string filePath)
{
    std::vector<unsigned int> result;
    
    std::string line;
    std::ifstream myfile (filePath);
    std::string delimiter = " ";
    
    if (myfile.is_open())
    {
        while(getline(myfile,line))
        {
            while(true)
            {
                int pos = (int) (line.find(delimiter));
                if(pos == std::string::npos) break;
                
                std::string token = line.substr(0, pos);
                line = line.substr(pos + 1);
                
                unsigned int value = std::stoi(token);
                
                result.push_back(value);
            }
        }
        
        myfile.close();
    }
    
    std::cout << "Vector was loaded from "<< filePath << std::endl;
    
    return result;
}
