//
//  loadVector.hpp
//  hSNE
//
//  Created by Florian Fechner on 26.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef loadVector_hpp
#define loadVector_hpp

#include <vector>
#include <string>

std::vector<unsigned int> loadVector(std::string filePath);

#endif /* loadVector_hpp */
