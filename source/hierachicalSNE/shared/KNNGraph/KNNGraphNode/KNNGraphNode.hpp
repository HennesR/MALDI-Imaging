//
//  KNNGraphNode.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef KNNGraphNode_hpp
#define KNNGraphNode_hpp

#include <vector>

class KNNGraphNode
{
    public:
        KNNGraphNode();
        std::vector<KNNGraphNode> neighbours;
};

#endif /* KNNGraphNode_hpp */
