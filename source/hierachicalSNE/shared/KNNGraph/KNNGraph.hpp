//
//  KNNGraph.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef KNNGraph_hpp
#define KNNGraph_hpp

#include "../../../shared/Point/Point.h"
#include "KNNGraphNode/KNNGraphNode.hpp"

class KNNGraph
{
    public:
        KNNGraph(Point* points, unsigned int pointsSize);
        void getKNeigboursOfPointWithIndex(unsigned int* indices, unsigned int k, unsigned int index);
    
    private:
        KNNGraphNode* nodes;
        unsigned int nodesSize;
};

#endif /* KNNGraph_hpp */
