//
//  saveVector.hpp
//  hSNE
//
//  Created by Florian Fechner on 25.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef saveVector_hpp
#define saveVector_hpp

#include <vector>
#include <string>

void saveVector(std::string filePath, std::vector<unsigned int> vector);

#endif /* saveVector_hpp */
