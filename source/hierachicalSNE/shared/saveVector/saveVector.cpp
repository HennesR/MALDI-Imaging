//
//  saveVector.cpp
//  hSNE
//
//  Created by Florian Fechner on 25.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "saveVector.hpp"
#include <fstream>
#include <iostream>

void saveVector(std::string filePath, std::vector<unsigned int> vector)
{
    std::ofstream fileStream;
    fileStream.open (filePath);
    
    for(std::vector<unsigned int>::iterator it = vector.begin(); it != vector.end(); ++it)
    {
        fileStream << *it << " ";
    }
    
    std::cout << "Unsigned int vector was saved under "<< filePath << std::endl;
    fileStream.close();
}
