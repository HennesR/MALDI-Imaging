//
//  calculateSimilaritiesMatrix.hpp
//  hSNE
//
//  Created by Florian Fechner on 07.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateSimilaritiesMatrix_hpp
#define calculateSimilaritiesMatrix_hpp

#include "../../shared/Matrix/Matrix.hpp"

Matrix calculateSimilaritiesMatrix(double* neigboursSimilarities, unsigned int pointsSize, unsigned int* neighboursIndices, unsigned int numberOfNeighbours);

#endif /* calculateSimilaritiesMatrix_hpp */
