//
//  calculateSimilaritiesMatrix.cpp
//  hSNE
//
//  Created by Florian Fechner on 07.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateSimilaritiesMatrix.hpp"

Matrix calculateSimilaritiesMatrix(double* neigboursSimilarities, unsigned int pointsSize, unsigned int* neighboursIndices, unsigned int numberOfNeighbours)
{
    Matrix result(pointsSize, pointsSize);
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<numberOfNeighbours; j++)
        {
            unsigned int neighbourIndex = neighboursIndices[i * numberOfNeighbours + j];
            double value = neigboursSimilarities[i * numberOfNeighbours + j];
            
            result.set(i, neighbourIndex, value);
        }
    }
    
    return result;
}
