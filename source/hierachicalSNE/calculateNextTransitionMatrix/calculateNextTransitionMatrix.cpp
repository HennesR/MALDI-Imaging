//
//  calculateNextTransitionMatrix.cpp
//  hSNE
//
//  Created by Florian Fechner on 08.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateNextTransitionMatrix.hpp"
#include "../../shared/printProgress/printProgress.hpp"

Matrix calculateNextTransitionMatrix(std::vector<unsigned int> landmarksIndices, Matrix influenceMatrix, std::vector<double> weights, unsigned int scale)
{
    unsigned int lastPointsSize = influenceMatrix.getWidth();
    unsigned int currentPointsSize = influenceMatrix.getHeight();
    
    Matrix transitionMatrix(currentPointsSize, currentPointsSize);
    
    for(unsigned int i=0; i<currentPointsSize; i++)
    {
        printProgress("calculate next transition Matrix", i, currentPointsSize);
        
        double divider = 0.0;
        
        for(unsigned int k=0; k<lastPointsSize; k++)
        {
            if(!influenceMatrix.isZero(k, i))
            {
                for(unsigned int l=0; l<currentPointsSize; l++)
                {
                    divider += influenceMatrix.at(k, i) * influenceMatrix.at(k, l) * weights.at(k);
                }
            }
        }
        
        for(unsigned int j=0; j<currentPointsSize; j++)
        {
            double counter = 0.0;
            
            for(unsigned int k=0; k<lastPointsSize; k++)
            {
                counter += influenceMatrix.at(k, i) * influenceMatrix.at(k, j) * weights.at(k);
            }
            
            double value = (divider == 0) ? 0 : (counter / divider);
            transitionMatrix.set(i, j, value);
        }
    }
    
    return transitionMatrix;
}
