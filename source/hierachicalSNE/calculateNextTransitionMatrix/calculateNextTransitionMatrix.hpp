//
//  calculateNextTransitionMatrix.hpp
//  hSNE
//
//  Created by Florian Fechner on 08.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateNextTransitionMatrix_hpp
#define calculateNextTransitionMatrix_hpp

#include "../../shared/Matrix/Matrix.hpp"

#include <vector>

Matrix calculateNextTransitionMatrix(std::vector<unsigned int> landmarksIndices, Matrix influenceMatrix, std::vector<double> weights, unsigned int scale);

#endif /* calculateNextTransitionMatrix_hpp */
