//
//  KMeansTreeNode.cpp
//  hSNE
//
//  Created by Florian Fechner on 13.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "KMeansTreeNode.hpp"

KMeansTreeNode::KMeansTreeNode(unsigned int dimensionality)
{
    initializePoint(&(this->center), dimensionality);
}

bool KMeansTreeNode::isLeaf()
{
    return (this->children.size() == 0);
}
