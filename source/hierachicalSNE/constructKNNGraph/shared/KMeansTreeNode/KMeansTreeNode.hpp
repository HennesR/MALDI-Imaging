//
//  KMeansTreeNode.hpp
//  hSNE
//
//  Created by Florian Fechner on 13.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef KMeansTreeNode_hpp
#define KMeansTreeNode_hpp

#include "../../../../shared/Point/Point.h"

#include <vector>

class KMeansTreeNode
{
    public:
        KMeansTreeNode(unsigned int dimensionality);
        Point center;
        std::vector<unsigned int> points;
        std::vector<KMeansTreeNode> children;
        bool isLeaf();
};

#endif /* KMeansTreeNode_hpp */
