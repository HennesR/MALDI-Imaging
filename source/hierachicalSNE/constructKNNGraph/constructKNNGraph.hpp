//
//  constructKNNGraph.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef constructKNNGraph_hpp
#define constructKNNGraph_hpp

#include "../../shared/Point/Point.h"

void constructKNNGraph(unsigned int* neigbours, Point* points, unsigned int pointsSize, unsigned int pointsDimension, unsigned int numberOfNeighbours);

#endif /* constructKNNGraph_hpp */
