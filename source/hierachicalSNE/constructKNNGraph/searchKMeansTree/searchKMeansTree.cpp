//
//  searchKMeansTree.cpp
//  hSNE
//
//  Created by Florian Fechner on 13.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "searchKMeansTree.hpp"

#include <iostream>
#include <limits>
#include <functional>
#include <queue>

auto cmp1 = [](std::pair<KMeansTreeNode, double> left, std::pair<KMeansTreeNode, double> right) { return (left.second > right.second); };
auto cmp2 = [](std::pair<unsigned int, double> left, std::pair<unsigned int, double> right) { return (left.second > right.second); };

void traverseKMeansTree(
                        Point* points, KMeansTreeNode node,
                        std::priority_queue<std::pair<KMeansTreeNode, double>, std::vector<std::pair<KMeansTreeNode, double>>, decltype(cmp1)>* priorityQueue,
                        std::priority_queue<std::pair<unsigned int, double>, std::vector<std::pair<unsigned int, double>>, decltype(cmp2)>* resultQueue,
                        unsigned int* count, unsigned int queryPointIndex);

std::queue<unsigned int> searchKMeansTree(unsigned int queryPointIndex, Point* points, unsigned int pointsSize, unsigned int pointsDimension, unsigned int numberOfNeighbours, KMeansTreeNode kMeansTree)
{
    unsigned int count = 0;

    std::priority_queue<std::pair<KMeansTreeNode, double>, std::vector<std::pair<KMeansTreeNode, double>>, decltype(cmp1)> priorityQueue(cmp1);
    std::priority_queue<std::pair<unsigned int, double>, std::vector<std::pair<unsigned int, double>>, decltype(cmp2)> resultQueue(cmp2);

    traverseKMeansTree(points, kMeansTree, &priorityQueue, &resultQueue, &count, queryPointIndex);

    while(!priorityQueue.empty() && count < numberOfNeighbours * 2)
    {
        std::pair<KMeansTreeNode, double> n = priorityQueue.top();
        priorityQueue.pop();

        traverseKMeansTree(points, n.first, &priorityQueue, &resultQueue, &count, queryPointIndex);
    }

    std::queue<unsigned int> r;

    // std::cout << "===" << std::endl;

    for(unsigned int i=0; i<numberOfNeighbours && !resultQueue.empty(); i++)
    {
        // std::cout << resultQueue.top().first << "   " << resultQueue.top().second << std::endl;

        r.push(resultQueue.top().first);
        resultQueue.pop();
    }

    return r;
}

void traverseKMeansTree(
                        Point* points, KMeansTreeNode node,
                        std::priority_queue<std::pair<KMeansTreeNode, double>, std::vector<std::pair<KMeansTreeNode, double>>, decltype(cmp1)>* priorityQueue,
                        std::priority_queue<std::pair<unsigned int, double>, std::vector<std::pair<unsigned int, double>>, decltype(cmp2)>* resultQueue,
                        unsigned int* count, unsigned int queryPointIndex)
{
    if(node.isLeaf())
    {
        unsigned int pointsInLeafSize = (unsigned int) node.points.size();

        for(unsigned int i=0; i<pointsInLeafSize; i++)
        {
            unsigned int pointIndex = node.points.at(i);
            Point* p = &points[pointIndex];
            Point* queryPoint = &points[queryPointIndex];
            double distance = queryPoint->distance(p);

            if(pointIndex != queryPointIndex)
            {
                resultQueue->push(std::make_pair(pointIndex, distance));
                *count = (*count) + 1;
            }
        }
    }
    else
    {
        unsigned int minDistanceIndex = 0;
        double minDistance = std::numeric_limits<double>::max();

        for(unsigned int i=0; i<node.children.size(); i++)
        {
            KMeansTreeNode child = node.children.at(i);
            Point* center = &(child.center);
            Point* queryPoint = &points[queryPointIndex];
            double distance = queryPoint->distance(center);

            if(distance < minDistance)
            {
                minDistance = distance;
                minDistanceIndex = i;
            }
        }

        for(unsigned int i=0; i<node.children.size(); i++)
        {
            if(i != minDistanceIndex)
            {
                Point* center = &(node.center);
                Point* queryPoint = &points[queryPointIndex];
                double distance = queryPoint->distance(center);

                priorityQueue->push(std::make_pair(node.children[i], distance));
            }
        }

        traverseKMeansTree(points, node.children[minDistanceIndex], priorityQueue, resultQueue, count, queryPointIndex);
    }
}
