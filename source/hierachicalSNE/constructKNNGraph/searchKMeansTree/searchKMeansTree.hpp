//
//  searchKMeansTree.hpp
//  hSNE
//
//  Created by Florian Fechner on 13.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef searchKMeansTree_hpp
#define searchKMeansTree_hpp

#include "../shared/KMeansTreeNode/KMeansTreeNode.hpp"

#include <queue>

std::queue<unsigned int> searchKMeansTree(unsigned int queryPointIndex, Point* points, unsigned int pointsSize, unsigned int pointsDimension, unsigned int numberOfNeighbours, KMeansTreeNode kMeansTree);

#endif /* searchKMeansTree_hpp */
