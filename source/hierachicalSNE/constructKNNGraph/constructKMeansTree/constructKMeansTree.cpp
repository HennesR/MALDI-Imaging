//
//  constructKMeansTree.cpp
//  hSNE
//
//  Created by Florian Fechner on 13.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "constructKMeansTree.hpp"

#include <vector>
#include <limits>
#include <iostream>

KMeansTreeNode constructKMeansTreeRecursively(Point* points, unsigned int pointsSize, unsigned int pointsDimension, std::vector<unsigned int> indices, unsigned int branchingFactor, unsigned int numberOfKMeansIterations, unsigned int randomSeed);

KMeansTreeNode constructKMeansTree(Point* points, unsigned int pointsSize, unsigned int pointsDimension, unsigned int branchingFactor, unsigned int numberOfKMeansIterations, unsigned int randomSeed)
{
    std::vector<unsigned int> indices;

    for(unsigned int i=0; i<pointsSize; i++)
    {
        indices.push_back(i);
    }

    return constructKMeansTreeRecursively(points, pointsSize, pointsDimension, indices, branchingFactor, numberOfKMeansIterations, randomSeed);
}

KMeansTreeNode constructKMeansTreeRecursively(Point* points, unsigned int pointsSize, unsigned int pointsDimension, std::vector<unsigned int> indices, unsigned int branchingFactor, unsigned int numberOfKMeansIterations, unsigned int randomSeed)
{
    unsigned int indicesSize = (unsigned int)(indices.size());

    if(indicesSize < branchingFactor)
    {
        KMeansTreeNode leaf(pointsDimension);
        leaf.points = indices;

        return leaf;
    }
    else
    {
        Point* means = new Point[branchingFactor];
        initializePointsArray(means, pointsDimension, branchingFactor);

        Point* filteredPoints = new Point[indicesSize];
        initializePointsArray(filteredPoints, pointsDimension, indicesSize);

        for(unsigned int i=0; i<indicesSize; i++)
        {
            unsigned int index = indices.at(i);
            filteredPoints[i] = points[index];
        }

        kMeans(means, filteredPoints, indicesSize, pointsDimension, branchingFactor, numberOfKMeansIterations);

        KMeansTreeNode innerNode(pointsDimension);

        std::vector<unsigned int>* clusters = new std::vector<unsigned int>[branchingFactor];

        for(unsigned int i=0; i<indices.size(); i++)
        {
            unsigned int minDistanceIndex = 0;
            double minDistance = std::numeric_limits<double>::max();

            for(unsigned int j=0; j<branchingFactor; j++)
            {
                double distance = points[indices.at(i)].distance(&means[j]);
                if(distance < minDistance)
                {
                    minDistance = distance;
                    minDistanceIndex = j;
                }
            }

            clusters[minDistanceIndex].push_back(indices[i]);
        }

        for(unsigned int i=0; i<branchingFactor; i++)
        {
            std::vector<unsigned int> cluster = clusters[i];
            KMeansTreeNode child = constructKMeansTreeRecursively(points, pointsSize, pointsDimension, cluster, branchingFactor, numberOfKMeansIterations, randomSeed);
            child.center = means[i];
            innerNode.children.push_back(child);
        }

        delete[] clusters;
        delete[] means;
        delete[] filteredPoints;

        return innerNode;
    }
}

