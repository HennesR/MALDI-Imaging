//
//  constructKMeansTree.hpp
//  hSNE
//
//  Created by Florian Fechner on 13.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef constructKMeansTree_hpp
#define constructKMeansTree_hpp

#include "../shared/KMeansTreeNode/KMeansTreeNode.hpp"
#include "../../../shared/kMeans/kMeans.hpp"

KMeansTreeNode constructKMeansTree(Point* points, unsigned int pointsSize, unsigned int pointsDimension, unsigned int branchingFactor, unsigned int numberOfKMeansIterations, unsigned int randomSeed);

#endif /* constructKMeansTree_hpp */
