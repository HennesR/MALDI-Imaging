//
//  constructKNNGraph.cpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "constructKNNGraph.hpp"

#include "../../shared/printProgress/printProgress.hpp"
#include "shared/KMeansTreeNode/KMeansTreeNode.hpp"
#include "constructKMeansTree/constructKMeansTree.hpp"
#include "searchKMeansTree/searchKMeansTree.hpp"

#include <limits>
#include <vector>
#include <iostream>

void constructKNNGraph(unsigned int* neighbours, Point* points, unsigned int pointsSize, unsigned int pointsDimension, unsigned int numberOfNeighbours)
{
    unsigned int branchingFactor = 5;
    unsigned int numberOfKMeansIterations = 100;
    unsigned int randomSeed = 0;

    KMeansTreeNode kMeansTree = constructKMeansTree(points, pointsSize, pointsDimension, branchingFactor, numberOfKMeansIterations, randomSeed);

    for(unsigned int i=0; i<pointsSize; i++)
    {
        printProgress("knn graph construction", i, pointsSize);

        std::queue<unsigned int> resultQueue = searchKMeansTree(i, points, pointsSize, pointsDimension, numberOfNeighbours, kMeansTree);

        for(unsigned int j=0; j<numberOfNeighbours; j++)
        {
            neighbours[i * numberOfNeighbours + j] = resultQueue.front();
            resultQueue.pop();
        }
    }
}

/*
void constructKNNGraphNaiive(unsigned int* neighbours, Point* points, unsigned int pointsSize, unsigned int pointsDimension, unsigned int numberOfNeighbours)
{
    for(unsigned int i=0; i<pointsSize; i++)
    {
        std::cout << "(" << i << "/" << pointsSize << ")" << std::endl;

        bool* alreadyPicked = new bool[pointsSize];

        for(unsigned int j=0; j<pointsSize; j++)
        {
            alreadyPicked[j] = false;
        }

        for(unsigned int k=0; k<numberOfNeighbours; k++)
        {
            unsigned int minIndex = std::numeric_limits<unsigned int>::max();
            double minDistance = std::numeric_limits<double>::max();

            for(unsigned int j=0; j<pointsSize; j++)
            {
                double distance = points[i].distance(&points[j]);

                if(distance < minDistance && !alreadyPicked[j] && i != j)
                {
                    minDistance = distance;
                    minIndex = j;
                }
            }

            neighbours[i * numberOfNeighbours + k] = minIndex;
            alreadyPicked[minIndex] = true;
        }

        delete[] alreadyPicked;
    }
}
*/
