//
//  calculateDistancesToNeighbours.cpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateDistancesToNeighbours.hpp"

#include <iostream>

void calculateDistancesToNeighbours(double* distancesToNeighbours, Point* points, unsigned int pointsSize, unsigned int* neighboursIndices, unsigned int numberOfNeighbours)
{
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int k=0; k<numberOfNeighbours; k++)
        {
            unsigned int j = neighboursIndices[i * numberOfNeighbours + k];
            distancesToNeighbours[i * numberOfNeighbours + k] = points[i].distance(&points[j]);
        }
    }
}

