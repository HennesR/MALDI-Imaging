//
//  calculateDistancesToNeighbours.hpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateDistancesToNeighbours_hpp
#define calculateDistancesToNeighbours_hpp

#include "../../shared/Point/Point.h"

void calculateDistancesToNeighbours(double* distancesToNeighbours, Point* points, unsigned int pointsSize, unsigned int* neighboursIndices, unsigned int numberOfNeighbours);

#endif /* calculateDistancesToNeighbours_hpp */
