//
//  hierachicalSNE.hpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef hierachicalSNE_hpp
#define hierachicalSNE_hpp

#include "../shared/Point/Point.h"
#include "../shared/Matrix/Matrix.hpp"
#include "../shared/HSNEResults/HSNEResult.hpp"

#include <vector>

void hierachicalSNE(HSNEResults* hsneResults, std::string savesBaseDirectory, Point* points, unsigned int maximumScale, unsigned int pointsSize, unsigned pointsDimension, unsigned int numberOfNeighbours, double landmarksTreshhold);

#endif /* hierachicalSNE_hpp */
