//
//  calculateKNeighboursSimilarities.hpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateKNeighboursSimilarities_hpp
#define calculateKNeighboursSimilarities_hpp

#include "../../shared/Point/Point.h"

void calculateKNeighboursSimilarities(double* similarities, double* distancesToNeighbours, unsigned int pointsSize, unsigned int* neighboursIndices, unsigned int numberOfNeighbours);

#endif /* calculateKNeighboursSimilarities_hpp */
