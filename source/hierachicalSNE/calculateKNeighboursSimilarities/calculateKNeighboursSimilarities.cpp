//
//  calculateKNeighboursSimilarities.cpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateKNeighboursSimilarities.hpp"

#include <limits>
#include <math.h>
#include <cmath>
#include <algorithm>

void calculateKNeighboursSimilarities(double* similarities, double* distancesToNeighbours, unsigned int pointsSize, unsigned int* neighboursIndices, unsigned int numberOfNeighbours)
{
    const double POSITIVE_DOUBLE_INFINITY = std::numeric_limits<double>::max();
    const double NEGATIVE_DOUBLE_INFINITY = std::numeric_limits<double>::min();
    
    const unsigned int MAX_ITERATIONS = 1000;
    const double TOLERANCE = 0.000001;
    
    const unsigned int perplexity = numberOfNeighbours / 3;
    
    // target entropy of distribution
    const double hTarget = log(perplexity);
    
    double* pRow = new double[pointsSize];
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        double betaMin = NEGATIVE_DOUBLE_INFINITY;
        double betaMax = POSITIVE_DOUBLE_INFINITY;
        
        double beta = 1.0; // initial value of precision
        bool done = false;
        
        // binary search for a good gaussian variance
        unsigned int num = 0;
        
        while(!done)
        {
            // compute entropy and kernel row with beta precision
            double pSum = 0.0;
            
            for(unsigned int j=0; j<numberOfNeighbours; j++)
            {
                // we dont care about diagonals
                double distance = distancesToNeighbours[i * numberOfNeighbours + j];
                double pj = ( i == j ) ? 0 : exp(pow(distance, 2) * beta);
                
                pRow[j] = pj;
                pSum += pj;
            }
            
            // normalize p and compute entropy
            double hHere = 0.0;
            
            for(unsigned int j=0; j<numberOfNeighbours; j++)
            {
                double pj = (pSum == 0) ? 0 : pRow[j] / pSum;
                pRow[j] = pj;
                
                if(pj > 1e-7)
                {
                    hHere -= pj * log(pj);
                }
            }
            
            // adjust beta based on result
            if(hHere > hTarget)
            {
                betaMin = beta;
                if(betaMax == POSITIVE_DOUBLE_INFINITY)
                {
                    beta = beta * 2.0;
                }
                else
                {
                    beta = (beta + betaMax) / 2.0;
                }
            }
            else
            {
                betaMax = beta;
                if(betaMin == NEGATIVE_DOUBLE_INFINITY)
                {
                    beta = beta / 2.0;
                }
                else
                {
                    beta = (beta + betaMin) / 2.0;
                }
            }
            
            // stopping conditions: too many tries or got a good precision
            num++;
            
            if(abs(hHere - hTarget) < TOLERANCE || num >= MAX_ITERATIONS)
            {
                done = true;
            }
        }
        
        // copy over the final prow to P at row i
        for(unsigned int j=0; j<numberOfNeighbours; j++)
        {
            similarities[i * numberOfNeighbours + j] = std::isnan(pRow[j]) ? 0 : pRow[j];
        }
    }
    
    double accumulation = 0.0;
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<numberOfNeighbours; j++)
        {
            accumulation += similarities[i * numberOfNeighbours + j];
        }
    }
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<numberOfNeighbours; j++)
        {
            if(accumulation > 0.0)
            {
                similarities[i * numberOfNeighbours + j] /= accumulation;
            }
            else
            {
                similarities[i * numberOfNeighbours + j] = 0.0;
            }
        }
    }

    delete[] pRow;
}
