//
//  calculateWeights.cpp
//  hSNE
//
//  Created by Florian Fechner on 08.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateWeights.hpp"
#include "../../shared/printProgress/printProgress.hpp"

#include <iostream>

std::vector<double> calculateWeights(unsigned int pointsSize)
{
    std::vector<double> weights;
    
    for(int i=0; i<pointsSize; i++)
    {
        weights.push_back(1.0);
    }
    
    return weights;
}

std::vector<double> calculateWeights(Matrix influenceMatrix, std::vector<double>* weightsAtAllScales, unsigned int scale)
{
    std::vector<double> weights;

    unsigned int currentPointsSize = influenceMatrix.getWidth();
    unsigned int nextPointsSize = influenceMatrix.getHeight();
    
    for(int i=0; i<nextPointsSize;  i++)
    {
        printProgress("calculate weights", i, nextPointsSize);
        
        double accumulation = 0.0;
        
        std::vector<std::pair<unsigned int, double>> column = influenceMatrix.atColumn(i) ;
        
        for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
        {
            std::pair<unsigned int, double> entry = *it;
            accumulation += entry.second * weightsAtAllScales[scale - 1].at(entry.first);
        }
        
        weights.push_back(accumulation);
    }
    
    return weights;
}
