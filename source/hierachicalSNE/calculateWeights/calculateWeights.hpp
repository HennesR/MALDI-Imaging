//
//  calculateWeights.hpp
//  hSNE
//
//  Created by Florian Fechner on 08.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateWeights_hpp
#define calculateWeights_hpp

#include "../../shared/Matrix/Matrix.hpp"

#include <vector>

std::vector<double> calculateWeights(unsigned int pointsSize);
std::vector<double> calculateWeights(Matrix influenceMatrix, std::vector<double>* weightsAtAllScales, unsigned int scale);

#endif /* calculateWeights_hpp */
