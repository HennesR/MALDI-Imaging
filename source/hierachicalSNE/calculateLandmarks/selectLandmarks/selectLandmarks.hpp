//
//  selectLandmarks.hpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef selectLandmarks_hpp
#define selectLandmarks_hpp

#include <vector>

std::vector<unsigned int> selectLandmarks(double* equilibriumDistribution, unsigned int pointsSize, double treshhold);

#endif /* selectLandmarks_hpp */
