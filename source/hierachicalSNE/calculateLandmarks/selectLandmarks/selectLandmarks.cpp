//
//  selectLandmarks.cpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "selectLandmarks.hpp"

#include <iostream>

std::vector<unsigned int> selectLandmarks(double* equilibriumDistribution, unsigned int pointsSize, double treshhold)
{
    std::vector<unsigned int> landmarks;
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        if(equilibriumDistribution[i] * pointsSize >= treshhold)
        {
            landmarks.push_back(i);
        }
    }
    
    return landmarks;
}
