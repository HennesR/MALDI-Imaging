//
//  calculateLandmarks.hpp
//  hSNE
//
//  Created by Florian Fechner on 07.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateLandmarks_hpp
#define calculateLandmarks_hpp

#include "../../shared/Matrix/Matrix.hpp"

#include <vector>

std::vector<unsigned int> calculateLandmarks(Matrix finiteMarkovChain, double landmarksTreshhold);

#endif /* calculateLandmarks_hpp */
