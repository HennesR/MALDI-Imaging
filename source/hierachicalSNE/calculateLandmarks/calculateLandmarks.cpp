//
//  calculateLandmarks.cpp
//  hSNE
//
//  Created by Florian Fechner on 07.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateLandmarks.hpp"
#include "selectLandmarks/selectLandmarks.hpp"
#include "calculateEquilibriumDistribution/calculateEquilibriumDistribution.hpp"

#include <iostream>

std::vector<unsigned int> calculateLandmarks(Matrix finiteMarkovChain, double landmarksTreshhold)
{
    unsigned int pointsSize = finiteMarkovChain.getWidth();
    
    double* equilibriumDistribution = new double[pointsSize];
    calculateEquilibriumDistribution(equilibriumDistribution, finiteMarkovChain);
    
    std::vector<unsigned int> landmarkIndices = selectLandmarks(equilibriumDistribution, pointsSize, landmarksTreshhold);
    
    delete[] equilibriumDistribution;

    return landmarkIndices;
}
