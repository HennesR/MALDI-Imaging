//
//  calculateEquilibriumDistribution.cpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateEquilibriumDistribution.hpp"

#include "../../../shared/printProgress/printProgress.hpp"

#include <stdlib.h>
#include <utility>
#include <vector>

// approximate calculation with Monte Carlo "randomWalk" Technique
void calculateEquilibriumDistribution(double* equilibriumDistribution, Matrix markovChain)
{
    unsigned int PRECISION = 10000;
    unsigned int AMOUNT_OF_RANDOM_WALKS = 100;
    unsigned int MAXIMUM_LENGTH = 50;
    
    unsigned int pointsSize = markovChain.getWidth();
    
    unsigned int* destinations = new unsigned int[pointsSize];
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        destinations[i] = 0;
    }
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        printProgress("calculate Equilibrium Distribution", i, pointsSize);
        
        for(unsigned int j=0; j<AMOUNT_OF_RANDOM_WALKS; j++)
        {
            unsigned int currentIndex = i;
            
            for(unsigned int k=0; k<MAXIMUM_LENGTH; k++)
            {
                unsigned int randomNumber = rand() % PRECISION;
                double normalizedRandomNumber = (double)randomNumber/ PRECISION;
                double accumulatedPropability = 0.0;
                unsigned int nextIndex = 0;
                double divider = 0.0;
                
                std::vector<std::pair<unsigned int, double>> column = markovChain.atColumn(currentIndex);
                
                for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
                {
                    divider += it->second;
                }
                
                for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
                {
                    accumulatedPropability += it->second / divider;
                    
                    if(accumulatedPropability >= normalizedRandomNumber)
                    {
                        nextIndex = it->first;
                        break; // nextIndex already found!
                    }
                }
                
                currentIndex = nextIndex;
            }
            
            destinations[currentIndex]++;
        }
    }
    
    double divider = 0.0;
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        divider += (double)(destinations[i]);
    }
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        equilibriumDistribution[i] = (double)(destinations[i]) / divider;
    }

    delete[] destinations;
}

