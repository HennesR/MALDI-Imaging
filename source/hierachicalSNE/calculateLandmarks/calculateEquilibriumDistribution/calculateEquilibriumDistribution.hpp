//
//  calculateEquilibriumDistribution.hpp
//  hSNE
//
//  Created by Florian Fechner on 02.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateEquilibriumDistribution_hpp
#define calculateEquilibriumDistribution_hpp

#include "../../../shared/Matrix/Matrix.hpp"

void calculateEquilibriumDistribution(double* equilibriumDistribution, Matrix markovChain);

#endif /* calculateEquilibriumDistribution_hpp */
