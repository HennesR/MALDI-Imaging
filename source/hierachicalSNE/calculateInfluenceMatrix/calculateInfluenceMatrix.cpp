//
//  calculateInfluenceMatrix.cpp
//  hSNE
//
//  Created by Florian Fechner on 03.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateInfluenceMatrix.hpp"

#include "../../shared/printProgress/printProgress.hpp"

#include <iostream>

// approximative monte carlo random walks algorithm
Matrix calculateInfluenceMatrix(Matrix markovChain, std::vector<unsigned int> landmarksIndices)
{
    unsigned int PRECISION = 10000;
    unsigned int AMOUNT_OF_RANDOM_WALKS = 1000;
    unsigned int MAXIMUM_LENGTH = 200;
    
    unsigned int landmarksSize = (unsigned int) landmarksIndices.size();
    unsigned int pointsSize = markovChain.getWidth();
    
    Matrix influenceMatrix(pointsSize, landmarksSize);
    
    std::cout << "ps: " << pointsSize << " lm: " << landmarksSize << std::endl;
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        printProgress("calculate Influence Matrix", i, pointsSize);
        
        unsigned int* destinations = new unsigned int[landmarksSize];
        unsigned int missingPoints = 0;
        
        for(unsigned int j=0; j<landmarksSize; j++)
        {
            destinations[j] = 0;
        }
        
        for(unsigned int j=0; j<AMOUNT_OF_RANDOM_WALKS; j++)
        {
            bool found = false;
            unsigned int currentIndex = i;
            unsigned int landmarkIndex = 0;
            
            for(unsigned int k=0; k<MAXIMUM_LENGTH && !found; k++)
            {
                int nextIndex = -1;
                unsigned int randomNumber = rand() % PRECISION;
                double normalizedRandomNumber = (double)(randomNumber) / PRECISION;
                double accumulatedPropability = 0.0;
                double divider = 0.0;
                
                std::vector<std::pair<unsigned int, double>> column = markovChain.atColumn(currentIndex);
                
                for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
                {
                    divider += it->second;
                }
                
                for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
                {
                    accumulatedPropability += it->second / divider;
                    
                    if(accumulatedPropability >= normalizedRandomNumber)
                    {
                        nextIndex = it->first;
                        break; // nextIndex already found!
                    }
                }
                
                if(nextIndex == -1)
                {
                    std::cout << "Error while calculating Influence Matrix by random walks: " << accumulatedPropability << " " << normalizedRandomNumber << std::endl;
                }
                else
                {
                     currentIndex = nextIndex;
                }
                
                for(unsigned int l=0; l<landmarksIndices.size(); l++)
                {
                    //std::cout << landmarksIndices.at(l) << std::endl;
                    //std::cout << currentIndex << std::endl;
                    
                    if(landmarksIndices.at(l) == currentIndex)
                    {
                        found = true;
                        landmarkIndex = l;
                    }
                }
            }
            
            if(found)
            {
                destinations[landmarkIndex] += 1;
            }
            else
            {
                missingPoints++;
            }
        }
        
        for(unsigned int j=0; j<landmarksSize; j++)
        {
            double propability = (AMOUNT_OF_RANDOM_WALKS - missingPoints == 0) ? 0 : (double)(destinations[j]) / (AMOUNT_OF_RANDOM_WALKS - missingPoints);
            
            influenceMatrix.set(i, j, propability);
        }

        delete[] destinations;
    }
    
    return influenceMatrix;
}
