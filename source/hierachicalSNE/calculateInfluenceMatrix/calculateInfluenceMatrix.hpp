//
//  calculateInfluenceMatrix.hpp
//  hSNE
//
//  Created by Florian Fechner on 03.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateInfluenceMatrix_hpp
#define calculateInfluenceMatrix_hpp

#include "../../shared/Matrix/Matrix.hpp"

#include <vector>

Matrix calculateInfluenceMatrix(Matrix markovChain, std::vector<unsigned int> landmarksIndices);

#endif /* calculateInfluenceMatrix_hpp */
