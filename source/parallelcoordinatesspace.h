#ifndef PRINCIPALCOMPONENTSPACE_H
#define PRINCIPALCOMPONENTSPACE_H

#include <QQuickPaintedItem>
#include "dataset.h"

class QCustomPlot;
class QCPBars;

class ParallelCoordinatesSpace : public QQuickPaintedItem
{
    Q_OBJECT
public:
    ~ParallelCoordinatesSpace() override;
    void setDataSet( const DataSet* pDataSet );
    void setDimensions( QVector<int> dimensions );

    Q_INVOKABLE void setOutputGroup( Group* pGroup );

private:
    void sectionsUpdated();
    void selectionUpdated();

    void componentComplete() override;
    void paint(QPainter* painter) override;

    void mousePressEvent( QMouseEvent* event ) override;
    void mouseMoveEvent( QMouseEvent* event ) override;
    void mouseReleaseEvent( QMouseEvent* event ) override;

    struct section { float begin, end; };

    const DataSet*  _dataSet        = nullptr;
    QCustomPlot*    _customPlot     = nullptr;
    Group*          _outputGroup    = nullptr;

    QPoint _selectBegin { -1, -1 }, _selectEnd { -1, -1 };

    QVector<int>        _dimensions {};
    QVector<section>    _sections {};
};

#endif // PRINCIPALCOMPONENTSPACE_H
