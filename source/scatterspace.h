#ifndef SCATTERSPACE_H
#define SCATTERSPACE_H

#include <QQuickFramebufferObject>
#include <QOpenGLExtraFunctions>
#include <QPropertyAnimation>
#include <QMatrix3x3>
#include <QPolygonF>

#include "dataset.h"
#include "shared.h"

class LassoTool;

class ScatterSpace : public QQuickFramebufferObject
{
private:
    Q_OBJECT
    Q_PROPERTY(float rotation MEMBER _rotation NOTIFY onPropertyChanged)

public:
    void setDataSet( const DataSet* pDataSet );
    void setData( QVector<pointCluster> clusters, int width, int height );

    Q_INVOKABLE void setOutputGroup( Group* pGroup );

signals:
    void selectedPointsChanged( QVector<pixelCluster> clusters );
    void onPropertyChanged();

private:
    static int HilbertCurve( int dimension, int x, int y );
    QQuickFramebufferObject::Renderer* createRenderer() const override;
    void componentComplete() override;

    void hoverEnterEvent( QHoverEvent* event ) override;
    void hoverLeaveEvent( QHoverEvent* event ) override;
    void hoverMoveEvent( QHoverEvent* event ) override;

    void mousePressEvent( QMouseEvent* event ) override;
    void keyPressEvent( QKeyEvent* event ) override;

    Group*      _outputGroup = nullptr;
    LassoTool*  _lasso = nullptr;
    QPolygonF   _selectionXY {}, _selectionYZ {};

    QVector<pointCluster> _clusters {};
    bool _clustersChanged = false;

    QPropertyAnimation  _rotationAnimation { this, "rotation" };
    float _rotation = 0.0f;

    //////////////
    // RENDERER //
    //////////////

    friend class ScatterSpace::Renderer;
    class Renderer : public QQuickFramebufferObject::Renderer
    {
    public:
        Renderer();
        ~Renderer() override;

    private:
        QOpenGLFramebufferObject* createFramebufferObject( const QSize& size ) override;
        void synchronize( QQuickFramebufferObject* item ) override;
        void render() override;

        struct color { GLfloat r, g, b; };

        GLuint _vertexShader, _fragmentShader, _shaderProgram, _pointsVBO, _VAO;

        QMatrix3x3      _viewMatrix {};
        QVector<color>  _colors {};
        GLsizei         _pointCount = 0;
    };
};

#endif // SCATTERSPACE_H
