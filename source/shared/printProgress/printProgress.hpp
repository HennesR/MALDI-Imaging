//
//  printProgress.hpp
//  hSNE
//
//  Created by Florian Fechner on 21.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef printProgress_hpp
#define printProgress_hpp

#include <iostream>
#include <string>

void printProgress(std::string name, unsigned int step, unsigned int totalSteps);

#endif /* printProgress_hpp */
