//
//  printProgress.cpp
//  hSNE
//
//  Created by Florian Fechner on 21.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "printProgress.hpp"

void printProgress(std::string name, unsigned int step, unsigned int totalSteps)
{
    unsigned int percent = (unsigned int)(((double)(step + 1.0)) / totalSteps * 100);
    std::cout << name << ": (" << (step + 1) << " / " << totalSteps << ") = "<< percent << "%" << std::endl;
}
