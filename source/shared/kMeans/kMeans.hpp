//
//  kMeans.hpp
//  hSNE
//
//  Created by Florian Fechner on 12.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef kMeans_hpp
#define kMeans_hpp

#include "../../shared/Point/Point.h"

void kMeans(Point* means, Point* data, unsigned int pointsSize, unsigned int pointsDimension, unsigned int k, unsigned int number_of_iterations);

#endif /* kMeans_hpp */
