// modified version of http://www.goldsborough.me/c++/python/cuda/2017/09/10/20-32-46-exploring_k-means_in_python,_c++_and_cuda/

#include "kMeans.hpp"
#include "../../shared/printProgress/printProgress.hpp"

#include <algorithm>
#include <cstdlib>
#include <limits>
#include <random>
#include <vector>

void kMeans(Point* means, Point* data, unsigned int pointsSize, unsigned int pointsDimension, unsigned int k, unsigned int number_of_iterations)
{
    static std::mt19937 random_number_generator(0);
    std::uniform_int_distribution<size_t> indices(0, pointsSize - 1);
    
    // Pick centroids as random points from the dataset.
    for(unsigned int i = 0; i<k; i++)
    {
        means[i] = data[indices(random_number_generator)];
    }
    
    std::vector<size_t> assignments(pointsSize);
    for (size_t iteration = 0; iteration < number_of_iterations; ++iteration)
    {
        printProgress("kMeans", iteration, number_of_iterations);
        
        // Find assignments.
        for (size_t point = 0; point < pointsSize; ++point)
        {
            double best_distance = std::numeric_limits<double>::max();
            size_t best_cluster = 0;
            for (size_t cluster = 0; cluster < k; ++cluster)
            {
                const double distance = data[point].distance(&means[cluster]);
                if (distance < best_distance)
                {
                    best_distance = distance;
                    best_cluster = cluster;
                }
            }
            assignments[point] = best_cluster;
        }
        
        // Sum up and count points for each cluster.
        Point* new_means = new Point[k];
        initializePointsArray(new_means, pointsDimension, k);
        
        std::vector<size_t> counts(k, 0);
        for (size_t point = 0; point < pointsSize; ++point)
        {
            const auto cluster = assignments[point];
            new_means[cluster] = new_means[cluster] + data[point];
            counts[cluster] += 1;
        }
        
        // Divide sums by counts to get new centroids.
        for (size_t cluster = 0; cluster < k; ++cluster)
        {
            // Turn 0/0 into 0/1 to avoid zero division.
            const auto count = std::max<size_t>(1, counts[cluster]);
            means[cluster] = new_means[cluster] * (1.0 / count);
        }
        
        delete[] new_means;
    }
}
