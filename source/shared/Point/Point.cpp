
#include "Point.h"

#include <cmath>
#include <iostream>
#include <iostream>

Point::Point()
{
    this->coordinates = nullptr;
}

Point::Point(const Point &p)
{
    //delete[] this->coordinates;
    
    this->dimensionality = p.dimensionality;
    this->coordinates = new double[this->dimensionality];
    
    for(int i=0; i<this->dimensionality; i++)
    {
        this->coordinates[i] = p.coordinates[i];
    }
}

Point::Point(double* coordinates, unsigned int dimensionality)
{
    this->coordinates = coordinates;
    this->dimensionality = dimensionality;
}

Point::~Point()
{
    delete[] this->coordinates;
}

Point Point::operator+(const Point &b)
{
    double* resultCoordinates = new double[this->dimensionality];

    for(int i=0; i<this->dimensionality; i++)
    {
        resultCoordinates[i] = this->coordinates[i] + b.coordinates[i];
    }
    
    Point result(resultCoordinates, this->dimensionality);

    return result;
}

Point Point::operator-(const Point &b)
{
    double* resultCoordinates = new double[this->dimensionality];

    for(int i=0; i<this->dimensionality; i++)
    {
        resultCoordinates[i] = this->coordinates[i] - b.coordinates[i];
    }

    Point result(resultCoordinates, this->dimensionality);
    
    return result;
}

Point Point::operator*(const double factor)
{
    double* resultCoordinates = new double[this->dimensionality];

    for(int i=0; i<this->dimensionality; i++)
    {
        resultCoordinates[i] = this->coordinates[i] * factor;
    }

    Point result(resultCoordinates, this->dimensionality);
    
    return result;
}

double Point::distance(Point* b)
{
    double accumulation = 0.0;

    for(int i=0; i<this->dimensionality; i++)
    {
        accumulation = accumulation + pow(this->coordinates[i] - b->coordinates[i], 2);
    }

    return std::sqrt(accumulation);
}

bool Point::operator==(const Point &b)
{
    for(int i=0; i<this->dimensionality; i++)
    {
        if(this->coordinates[i] != b.coordinates[i])
        {
            return false;
        }
    }

    return true;
}

bool Point::operator!=(const Point &b)
{
    for(int i=0; i<this->dimensionality; i++)
    {
        if(this->coordinates[i] != b.coordinates[i])
        {
            return true;
        }
    }

    return false;
}

Point& Point::operator=(const Point &p)
{
    delete[] this->coordinates;
    
    this->dimensionality = p.dimensionality;
    this->coordinates = new double[this->dimensionality];
    
    for(int i=0; i<this->dimensionality; i++)
    {
        this->coordinates[i] = p.coordinates[i];
    }
    
    return *this;
}

void initializePointsArray(Point* pointsArray, unsigned int dimensionality, unsigned int size)
{
    for(unsigned int i=0; i<size; i++)
    {
        initializePoint(&pointsArray[i], dimensionality);
    }
}

void initializePoint(Point* point, unsigned int dimensionality)
{
    double* coordinates = new double[dimensionality];

    for(unsigned int i=0; i<dimensionality; i++)
    {
        coordinates[i] = 0;
    }
    
    point->dimensionality = dimensionality;
    point->coordinates = coordinates;
}
