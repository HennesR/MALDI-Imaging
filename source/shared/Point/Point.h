#ifndef POINT_H
#define POINT_H

#include <string>

class Point
{
    public:

        double* coordinates;
        unsigned int dimensionality;

        Point();
        Point(double* coordinates, unsigned int dimensionality);
        Point(const Point &p2);
        ~Point();

        double distance(Point* b);

        Point operator+ (const Point &b);
        Point operator- (const Point &b);
        Point operator* (const double factor);

        bool operator==(const Point &b);
        bool operator!=(const Point &b);
    
        Point& operator = (const Point &p);
};

void initializePoint(Point* point, unsigned int dimensionality);
void initializePointsArray(Point* pointsArray, unsigned int dimensionality, unsigned int size);

#endif // POINT_H
