//
//  Matrix.hpp
//  hSNE
//
//  Created by Florian Fechner on 07.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef Matrix_hpp
#define Matrix_hpp

#include <map>
#include <vector>
#include <utility>

class Matrix
{
public:
    Matrix();
    Matrix(const Matrix &m2);
    Matrix(unsigned int width, unsigned int height);
    ~Matrix();
    
    const unsigned int getWidth() const;
    const unsigned int getHeight() const;
    const std::vector<std::pair<unsigned int, double>> atColumn(unsigned int x) const;
    double at(unsigned int x, unsigned int y);
    bool isZero(unsigned int x, unsigned int y);
    void set(unsigned int x, unsigned int y, double value);
    void saveToFile(std::string filepath);
    void loadFromFile(std::string filepath);
    
    Matrix& operator = (const Matrix &m);
    
    std::vector<std::pair<unsigned int, double>>* columns;
    
private:
    unsigned int width;
    unsigned int height;
    std::map<unsigned int, unsigned int>* columnsMaps;
    
};

#endif /* Matrix_hpp */
