//
//  Matrix.cpp
//  hSNE
//
//  Created by Florian Fechner on 07.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "Matrix.hpp"

#include "../printProgress/printProgress.hpp"

#include <fstream>
#include <iostream>

Matrix::Matrix()
{
    this->width = 0;
    this->height = 0;
}

Matrix::Matrix(unsigned int width, unsigned int height)
{
    this->width = width;
    this->height = height;
    
    this->columns = new std::vector<std::pair<unsigned int, double>>[width];
    this->columnsMaps = new std::map<unsigned int, unsigned int>[width];
}

Matrix::Matrix(const Matrix &m2)
{
    this->width = m2.getWidth();
    this->height = m2.getHeight();
    
    this->columns = new std::vector<std::pair<unsigned int, double>>[this->width];
    this->columnsMaps = new std::map<unsigned int, unsigned int>[this->width];
    
    for(unsigned int i=0; i<this->width; i++)
    {
        this->columns[i] = m2.atColumn(i);
        this->columnsMaps[i] = m2.columnsMaps[i];
    }
}

Matrix::~Matrix()
{
    if(this->width > 1 && this->height > 1)
    {
        delete[] this->columns;
        delete[] this->columnsMaps;
    }
}

const unsigned int Matrix::getWidth() const
{
    return this->width;
}

const unsigned int Matrix::getHeight() const
{
    return this->height;
}

double Matrix::at(unsigned int x, unsigned int y)
{
    std::map<unsigned int, unsigned int>::iterator realIndexIterator = this->columnsMaps[x].find(y);
    
    if(realIndexIterator == this->columnsMaps[x].end())
    {
        return 0.0;
    }
    else
    {
        unsigned int realIndex = this->columnsMaps[x][y];
        double value = this->columns[x].at(realIndex).second;
        
        return value;
    }
}

bool Matrix::isZero(unsigned int x, unsigned int y)
{
    std::map<unsigned int, unsigned int>::iterator realIndexIterator = this->columnsMaps[x].find(y);
    
    return (realIndexIterator == this->columnsMaps[x].end());
}

const std::vector<std::pair<unsigned int, double>> Matrix::atColumn(unsigned int x) const
{
    return this->columns[x];
}

void Matrix::set(unsigned int x, unsigned int y, double value)
{
    double epsilon = 0.00000001;
    
    if(value - epsilon < 0.0)
    {
        // do nothing : TODO
    }
    else
    {
        this->columns[x].push_back(std::make_pair(y, value));
        this->columnsMaps[x].insert(std::pair<unsigned int, unsigned int>(y, this->columns[x].size() - 1));
    }
}

void Matrix::saveToFile(std::string filepath)
{
    std::ofstream fileStream;
    fileStream.open (filepath);
    
    for(unsigned int x=0; x<this->width; x++)
    {
        std::vector<std::pair<unsigned int, double>> column = this->atColumn(x);
        
        for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
        {
            std::pair<unsigned int, double> entry = *it;
            
            unsigned int index = entry.first;
            double value = entry.second;
            
            fileStream << index << ":" << value << " ";
        }
        
        fileStream << "\n";
    }
    
    fileStream.close();
    
    std::cout << "Matrix saved under "<< filepath << std::endl;
}

void Matrix::loadFromFile(std::string filepath)
{
    std::string line;
    std::ifstream myfile (filepath);
    std::string delimiter = " ";
    
    unsigned int x = 0;

    if (myfile.is_open())
    {
        while(getline(myfile,line))
        {
            printProgress("load matrix", x, this->width);
            
            while(true)
            {
                int pos = (int) (line.find(delimiter));
                if(pos == std::string::npos) break;

                std::string token = line.substr(0, pos);
                line = line.substr(pos + 1);
                
                int delimiterPosition = (int)(token.find(":"));
                std::string indexString = token.substr(0, delimiterPosition);
                std::string valueString = token.substr(delimiterPosition + 1);
                
                unsigned int index = std::stoi(indexString);
                double value = std::stod(valueString);

                this->set(x, index, value);
            }
            
            x++;
        }

        myfile.close();
    }
    
    std::cout << "Matrix was loaded from "<< filepath << std::endl;
}

Matrix& Matrix::operator=(const Matrix &m2)
{
    this->width = m2.getWidth();
    this->height = m2.getHeight();
    
    this->columns = new std::vector<std::pair<unsigned int, double>>[this->width];
    this->columnsMaps = new std::map<unsigned int, unsigned int>[this->width];
    
    for(unsigned int i=0; i<this->width; i++)
    {
        this->columns[i] = m2.atColumn(i);
        this->columnsMaps[i] = m2.columnsMaps[i];
    }
    
    return *this;
}


