
//
//  HSNEResult.cpp
//  hSNE
//
//  Created by Florian Fechner on 28.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "HSNEResult.hpp"

HSNEResults::HSNEResults(unsigned int maximumScale)
{
    this->maximumScale = maximumScale;
    this->influenceMatrices = new Matrix[maximumScale];
    this->localLandmarksIndices = new std::vector<unsigned int>[maximumScale];
    this->globalLandmarksIndices = new std::vector<unsigned int>[maximumScale];
    this->transitionMatrices = new Matrix[maximumScale];
}

HSNEResults::~HSNEResults()
{
    delete[] this->influenceMatrices;
    delete[] this->localLandmarksIndices;
    delete[] this->globalLandmarksIndices;
    delete[] this->transitionMatrices;
}
