//
//  HSNEResult.hpp
//  hSNE
//
//  Created by Florian Fechner on 28.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef HSNEResult_hpp
#define HSNEResult_hpp

#include <vector>
#include "../../shared/Matrix/Matrix.hpp"

class HSNEResults
{
public:
    HSNEResults(unsigned int maximumScale);
    ~HSNEResults();
    std::vector<unsigned int>* localLandmarksIndices;
    std::vector<unsigned int>* globalLandmarksIndices;
    Matrix* transitionMatrices;
    Matrix* influenceMatrices;
    unsigned int maximumScale;
};

#endif /* HSNEResult_hpp */
