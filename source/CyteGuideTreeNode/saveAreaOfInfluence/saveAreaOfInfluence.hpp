//
//  saveAreaOfInfluence.hpp
//  hSNE
//
//  Created by Florian Fechner on 03.12.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef saveAreaOfInfluence_hpp
#define saveAreaOfInfluence_hpp

#include <string>
#include <utility>
#include <vector>

void saveAreaOfInfluence(std::string filePath, std::vector<std::pair<unsigned, double>> areaOfInfluence);

#endif /* saveAreaOfInfluence_hpp */
