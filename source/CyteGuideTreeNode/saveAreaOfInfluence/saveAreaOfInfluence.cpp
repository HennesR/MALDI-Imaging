//
//  saveAreaOfInfluence.cpp
//  hSNE
//
//  Created by Florian Fechner on 03.12.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "saveAreaOfInfluence.hpp"

#include <fstream>
#include <iostream>

void saveAreaOfInfluence(std::string filePath, std::vector<std::pair<unsigned, double>> areaOfInfluence)
{
    std::ofstream fileStream;
    fileStream.open (filePath);
    
    for(std::vector<std::pair<unsigned, double>>::iterator it = areaOfInfluence.begin(); it != areaOfInfluence.end(); ++it)
    {
        fileStream << (*it).first << ":" << (*it).second << " ";
    }
    
    std::cout << "Area of Influence was saved under "<< filePath << std::endl;
    fileStream.close();
}
