//
//  calculateLowDimensionalSimilaritiesMatrix.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateLowDimensionalSimilaritiesMatrix_hpp
#define calculateLowDimensionalSimilaritiesMatrix_hpp

#include "../../../shared/Point/Point.h"

void calculateLowDimensionalSimilaritiesMatrix(double* q, double* p, Point* y, unsigned int pointsSize);

#endif /* calculateLowDimensionalSimilaritiesMatrix_hpp */
