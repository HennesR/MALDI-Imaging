//
//  calculateLowDimensionalSimilaritiesMatrix.cpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateLowDimensionalSimilaritiesMatrix.hpp"

#include <math.h>
#include <algorithm>

void calculateLowDimensionalSimilaritiesMatrix(double* q, double* p, Point* y, unsigned int pointsSize)
{
    double accumulation = 0.0;
    
    for(unsigned int k=0; k<pointsSize; k++)
    {
        for(unsigned int l=0; l<pointsSize; l++)
        {
            if(k != l)
            {
                accumulation += std::max(pow(1 + pow(y[k].distance(&y[l]), 2), -1), 1e-100);
            }
        }
    }
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        q[i * pointsSize + i] = 0.0;
        
        for(unsigned int j=i+1; j<pointsSize; j++)
        {
            double lowDimensionalSimilarity = (accumulation == 0) ? 1e-100 : std::max(pow(1 + pow(y[i].distance(&y[j]), 2), -1) / accumulation, 1e-100);
            
            q[i * pointsSize + j] = lowDimensionalSimilarity;
            q[j * pointsSize + i] = lowDimensionalSimilarity;
        }
    }
}
