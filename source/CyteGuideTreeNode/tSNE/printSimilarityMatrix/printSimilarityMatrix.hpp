//
//  printSimilarityMatrix.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef printSimilarityMatrix_hpp
#define printSimilarityMatrix_hpp

void printSimilarityMatrix(double* similarityMatrix, unsigned pointsSize);

#endif /* printSimilarityMatrix_hpp */
