//
//  printSimilarityMatrix.cpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "printSimilarityMatrix.hpp"

#include <iostream>

void printSimilarityMatrix(double* similarityMatrix, unsigned pointsSize)
{
    double accumulator = 0.0;
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<pointsSize; j++)
        {
            std::cout << similarityMatrix[i * pointsSize + j] << "\t\t";
            
            accumulator += similarityMatrix[i * pointsSize + j];
        }
        
        std::cout << std::endl;
    }
    
    std::cout << "total: " << accumulator << std::endl;
    std::cout << std::endl << std::endl;
}
