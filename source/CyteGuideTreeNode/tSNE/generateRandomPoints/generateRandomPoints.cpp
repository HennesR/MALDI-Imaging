//
//  generateRandomPoints.cpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "generateRandomPoints.hpp"

#include <stdlib.h>
#include <math.h>
#include <algorithm>

void generateRandomPoints(Point* embeddedPoints, unsigned int pointsSize, unsigned int pointsDimension)
{
    unsigned int PRECISION = 10000;
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        double* coordinates = new double[pointsDimension];
        
        for(unsigned int j=0; j<pointsDimension; j++)
        {
            unsigned int randomNumber = rand() % PRECISION;
            double normalizedRandomNumber = (double)randomNumber/ PRECISION;
            coordinates[j] = normalizedRandomNumber;
        }
        
        embeddedPoints[i].dimensionality = pointsDimension;
        embeddedPoints[i].coordinates = coordinates;
    }
}
