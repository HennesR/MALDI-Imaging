//
//  generateRandomPoints.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef generateRandomPoints_hpp
#define generateRandomPoints_hpp

#include "../../../shared/Point/Point.h"

void generateRandomPoints(Point* embeddedPoints, unsigned int pointsSize, unsigned int pointsDimension);

#endif /* generateRandomPoints_hpp */
