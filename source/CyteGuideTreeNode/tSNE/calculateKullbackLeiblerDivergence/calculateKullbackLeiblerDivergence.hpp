//
//  calculateKullbackLeiblerDivergence.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateKullbackLeiblerDivergence_hpp
#define calculateKullbackLeiblerDivergence_hpp

float calculateKullbackLeiblerDivergence(double* p, double* q, unsigned int pointsSize);

#endif /* calculateKullbackLeiblerDivergence_hpp */
