//
//  calculateKullbackLeiblerDivergence.cpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateKullbackLeiblerDivergence.hpp"

#include <math.h>
#include <algorithm>

float calculateKullbackLeiblerDivergence(double* p, double* q, unsigned int pointsSize)
{
    double accumulation = 0.0;
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<pointsSize; j++)
        {
            if(i != j)
            {
                accumulation += p[j * pointsSize + i] * log(p[j * pointsSize + i] / q[j * pointsSize + i]);
                accumulation += (1 - p[j * pointsSize + i]) * log(( 1 - p[j * pointsSize + i]) / ( 1 - q[j * pointsSize + i]));
            }
        }
    }
    
    return accumulation;
}
