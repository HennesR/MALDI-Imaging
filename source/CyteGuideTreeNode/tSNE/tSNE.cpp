
#include "tSNE.h"

#include <math.h>
#include <algorithm>
#include <iostream>
#include <string>

#include "calculateDistanceMatrix/calculateDistanceMatrix.hpp"
#include "calculateHighDimensionalSimilaritiesMatrix/calculateHighDimensionalSimilaritiesMatrix.hpp"
#include "calculateLowDimensionalSimilaritiesMatrix/calculateLowDimensionalSimilaritiesMatrix.hpp"
#include "generateRandomPoints/generateRandomPoints.hpp"
#include "normalizePoints/normalizePoints.hpp"
#include "calculateKullbackLeiblerDivergence/calculateKullbackLeiblerDivergence.hpp"
#include "printSimilarityMatrix/printSimilarityMatrix.hpp"

#include "../../shared/printProgress/printProgress.hpp"

#include "bhtsne/tsne.h"

void tSNE(Point* normalizedPoints, Point* dataPoints, unsigned int pointsSize, unsigned int pointsDimension, unsigned int perplexity, unsigned int maximumIterations)
{    
    if(pointsSize >= 50)
    {
        // BARNETS-HUT TSNE Library

        Point* y = new Point[pointsSize];
        initializePointsArray(y, 2, pointsSize);

        double* data = new double[pointsSize * pointsDimension];

        for(unsigned int i=0; i<pointsSize; i++)
        {
            for(unsigned int j=0; j<pointsDimension; j++)
            {
                data[i * pointsDimension + j] = dataPoints[i].coordinates[j];
            }
        }

        double fPerplexity = (double) perplexity;
        int origN = pointsSize;
        int N = pointsSize;
        int D = pointsDimension;
        int no_dims = 2;
        int max_iter = maximumIterations;
        double theta = 0.5;
        int rand_seed = -1;
        TSNE* tsne = new TSNE();

        // Read the parameters and the dataset
        // if(tsne->load_data(&data, &origN, &D, &no_dims, &theta, &fPerplexity, &rand_seed, &max_iter))
        {
            // Make dummy landmarks
            N = origN;
            int* landmarks = (int*) malloc(N * sizeof(int));
            if(landmarks == NULL) { printf("Memory allocation failed!\n"); exit(1); }
            for(int n = 0; n < N; n++) landmarks[n] = n;

            // Now fire up the SNE implementation
            double* Y = (double*) malloc(N * no_dims * sizeof(double));
            double* costs = (double*) calloc(N, sizeof(double));
            if(Y == NULL || costs == NULL) { printf("Memory allocation failed!\n"); exit(1); }
            tsne->run(data, N, D, Y, no_dims, perplexity, theta, rand_seed, false, max_iter);

            for(unsigned int i=0; i<pointsSize; i++)
            {
                for(unsigned int j=0; j<no_dims; j++)
                {
                    y[i].coordinates[j] =  Y[i * no_dims + j];
                }
            }

            // Clean up the memory
            free(data); data = NULL;
            free(Y); Y = NULL;
            free(costs); costs = NULL;
            free(landmarks); landmarks = NULL;
        }

        delete(tsne);

        normalizePoints(normalizedPoints, y, pointsSize, 2);
    }
    else
    {
        // SELF IMPLEMENTED

        std::cout << "tsne start: " << pointsSize << std::endl;

        const double LEARNING_RATE = 1.0;

        double* distanceMatrix = new double[pointsSize * pointsSize];
        calculateDistanceMatrix(dataPoints, distanceMatrix, pointsSize, pointsDimension);

        //printSimilarityMatrix(distanceMatrix, pointsSize);

        Point* y = new Point[pointsSize];
        initializePointsArray(y, 2, pointsSize);

        // high dimensional similarities matrix
        double* p = new double[pointsSize * pointsSize];
        calculateHighDimensionalSimilaritiesMatrix(distanceMatrix, p, pointsSize, perplexity);

        delete[] distanceMatrix;

        //printSimilarityMatrix(p, pointsSize);

        // initialize the embedding with random points
        generateRandomPoints(y, pointsSize, 2);

        // low dimensional similarities matrix
        double* q = new double[pointsSize * pointsSize];

        Point* notNormalizePoints = new Point[pointsSize];
        initializePointsArray(notNormalizePoints, 2, pointsSize);

        Point* momentumA = new Point[pointsSize];
        initializePointsArray(momentumA, 2, pointsSize);
        Point* momentumB = new Point[pointsSize];
        initializePointsArray(momentumB, 2, pointsSize);

        for(unsigned int h=0; h<maximumIterations; h++)
        {
            printProgress("tSNE", h, maximumIterations);

            calculateLowDimensionalSimilaritiesMatrix(q, p, y, pointsSize);

            Point* nextY = new Point[pointsSize];
            initializePointsArray(nextY, 2, pointsSize);

            for(unsigned int i=0; i<pointsSize; i++)
            {
                Point accumulation;
                initializePoint(&accumulation, 2);

                for(unsigned int j=0; j<pointsSize; j++)
                {
                    if(i != j)
                    {
                        Point a = y[i] - y[j];
                        double distance = y[i].distance(&y[j]);
                        double b = 1.0/(1 + pow(distance, 2));
                        double c = p[j * pointsSize + i] - q[j * pointsSize + i];

                        accumulation = accumulation + a * b * c;
                    }
                }

                Point gradient;
                initializePoint(&gradient, 2);
                gradient = accumulation * 4;

                if(h == 0)
                {
                    nextY[i] = y[i] - gradient * LEARNING_RATE;

                    momentumA[i] = gradient;
                }

                if(h == 1)
                {
                    Point momentumAImpact = momentumA[i] * LEARNING_RATE * 0.7;

                    nextY[i] = y[i] - gradient * LEARNING_RATE - momentumAImpact;

                    momentumB[i] = momentumA[i];
                    momentumA[i] = gradient;
                }

                if(h >= 2)
                {
                    Point momentumAImpact = momentumA[i] * LEARNING_RATE * 0.7;
                    Point momentumBImpact = momentumB[i] * LEARNING_RATE * 0.3;

                    nextY[i] = y[i] - gradient * LEARNING_RATE - momentumAImpact - momentumBImpact;

                    momentumB[i] = momentumA[i];
                    momentumA[i] = gradient;
                }
            }

            for(unsigned int i=0; i<pointsSize; i++)
            {
                y[i] = nextY[i];
            }

            delete[] nextY;
        }

        normalizePoints(normalizedPoints, y, pointsSize, 2);

        std::cout <<  "Kullback Leibler Divergence = " <<  calculateKullbackLeiblerDivergence(p, q, pointsSize) << std::endl;

        //printSimilarityMatrix(q, pointsSize);

        delete[] notNormalizePoints;
        delete[] momentumA;
        delete[] momentumB;
        delete[] q;
        delete[] p;
        delete[] y;
    }

    std::cout <<  "tSNE finished" << std::endl;
}
