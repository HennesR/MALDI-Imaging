#ifndef TSNE_H
#define TSNE_H

#include "../../shared/Point/Point.h"

void tSNE(Point* embeddedPoints, Point* dataPoints, unsigned int pointsSize, unsigned int pointsDimension, unsigned int perplexity, unsigned int maximumIterations);

#endif // TSNE_H
