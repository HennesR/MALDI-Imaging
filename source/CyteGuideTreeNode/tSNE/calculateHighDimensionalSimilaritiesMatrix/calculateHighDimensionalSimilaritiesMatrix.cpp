//
//  calculateHighDimensionalSimilaritiesMatrix.cpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateHighDimensionalSimilaritiesMatrix.hpp"

#include <math.h>
#include <limits>
#include <algorithm>

void calculateHighDimensionalSimilaritiesMatrix(double* distanceMatrix, double* similaritiesMatrix, unsigned int pointsSize, unsigned int perplexity)
{
    const double POSITIVE_DOUBLE_INFINITY = std::numeric_limits<double>::max();
    const double NEGATIVE_DOUBLE_INFINITY = std::numeric_limits<double>::min();
    
    const unsigned int MAX_ITERATIONS = 1000;
    const double TOLERANCE = 0.00001;
    
    // target entropy of distribution
    const double hTarget = log(perplexity);
    
    // temporary propability Matrix
    double* p = new double[pointsSize * pointsSize];
    
    double* pRow = new double[pointsSize];
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        double betaMin = NEGATIVE_DOUBLE_INFINITY;
        double betaMax = POSITIVE_DOUBLE_INFINITY;
        
        double beta = 1; // initial value of precision
        bool done = false;
        
        // binary search for a good gaussian variance
        unsigned int num = 0;
        
        while(!done)
        {
            // compute entropy and kernel row with beta precision
            double pSum = 0.0;
            
            for(unsigned int j=0; j<pointsSize; j++)
            {
                // we dont care about diagonals
                double pj = ( i == j ) ? 0 : exp(-pow(distanceMatrix[i * pointsSize + j], 2) * beta);
                
                pRow[j] = pj;
                pSum += pj;
            }
            
            // normalize p and compute entropy
            double hHere = 0.0;
            
            for(unsigned int j=0; j<pointsSize; j++)
            {
                double pj = (pSum == 0) ? 0 : pRow[j] / pSum;
                pRow[j] = pj;
                
                if(pj > 1e-7)
                {
                    hHere -= pj * log(pj);
                }
            }
            
            // adjust beta based on result
            if(hHere > hTarget)
            {
                betaMin = beta;
                if(betaMax == POSITIVE_DOUBLE_INFINITY)
                {
                    beta = beta * 2;
                }
                else
                {
                    beta = (beta + betaMax) / 2;
                }
            }
            else
            {
                betaMax = beta;
                if(betaMin == NEGATIVE_DOUBLE_INFINITY)
                {
                    beta = beta / 2;
                }
                else
                {
                    beta = (beta + betaMin) / 2;
                }
            }
            
            // stopping conditions: too many tries or got a good precision
            num++;
            
            if(abs(hHere - hTarget) < TOLERANCE || num >= MAX_ITERATIONS)
            {
                done = true;
            }
        }
        
        // copy over the final prow to P at row i
        for(unsigned int j=0; j<pointsSize; j++)
        {
            p[i * pointsSize + j] = pRow[j];
        }
    }
    
    // symmetrize P and normalize it to sum to 1 over all ij
    unsigned int n2 = pointsSize * 2;
    
    double accumulation = 0.0;
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<pointsSize; j++)
        {
            similaritiesMatrix[i * pointsSize + j] = ( p[i * pointsSize + j] + p[j * pointsSize + i] ) / n2;
            accumulation += similaritiesMatrix[i * pointsSize + j];
        }
    }
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<pointsSize; j++)
        {
            similaritiesMatrix[i * pointsSize + j] /= accumulation;
        }
    }
    
    delete[] pRow;
    delete[] p;
}
