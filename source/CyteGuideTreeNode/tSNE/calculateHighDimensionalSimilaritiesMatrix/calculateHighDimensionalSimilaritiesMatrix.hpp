//
//  calculateHighDimensionalSimilaritiesMatrix.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateHighDimensionalSimilaritiesMatrix_hpp
#define calculateHighDimensionalSimilaritiesMatrix_hpp

void calculateHighDimensionalSimilaritiesMatrix(double* distanceMatrix, double* similaritiesMatrix, unsigned int pointsSize, unsigned int perplexity);

#endif /* calculateHighDimensionalSimilaritiesMatrix_hpp */
