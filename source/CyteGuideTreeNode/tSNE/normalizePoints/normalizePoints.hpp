//
//  normalizePoints.hpp
//  hSNE
//
//  Created by Florian Fechner on 03.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef normalizePoints_hpp
#define normalizePoints_hpp

#include "../../../shared/Point/Point.h"

void normalizePoints(Point* normalizedPoints, Point* notNormalizedPoints, unsigned int pointsSize, unsigned int pointsDimension);

#endif /* normalizePoints_hpp */
