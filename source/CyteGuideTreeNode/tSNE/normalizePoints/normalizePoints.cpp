//
//  normalizePoints.cpp
//  hSNE
//
//  Created by Florian Fechner on 03.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "normalizePoints.hpp"

#include <limits>
#include <cmath>
#include <iostream>

void normalizePoints(Point* normalizedPoints, Point* notNormalizedPoints, unsigned int pointsSize, unsigned int pointsDimension)
{
    const double POSITIVE_DOUBLE_INFINITY = std::numeric_limits<double>::max();
    const double NEGATIVE_DOUBLE_INFINITY = std::numeric_limits<double>::min();
    
    double* minCoordinates = new double[pointsDimension];
    double* maxCoordinates = new double[pointsDimension];
    
    Point minCorner(minCoordinates, pointsDimension);
    initializePoint(&minCorner, pointsDimension);
    
    Point maxCorner(maxCoordinates, pointsDimension);
    initializePoint(&maxCorner, pointsDimension);
    
    for(unsigned int i=0; i<pointsDimension; i++)
    {
        minCorner.coordinates[i] = POSITIVE_DOUBLE_INFINITY;
        maxCorner.coordinates[i]  = NEGATIVE_DOUBLE_INFINITY;
    }
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<pointsDimension; j++)
        {
            double coordinate = notNormalizedPoints[i].coordinates[j];
            
            if(coordinate < minCorner.coordinates[j]) minCorner.coordinates[j] = coordinate;
            if(coordinate > maxCorner.coordinates[j]) maxCorner.coordinates[j] = coordinate;
        }
    }
    
    double distance = std::max(abs(minCorner.coordinates[0] - maxCorner.coordinates[0]), abs(minCorner.coordinates[1] - maxCorner.coordinates[1]));
    
    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<pointsDimension; j++)
        {
            normalizedPoints[i].coordinates[j] = (notNormalizedPoints[i].coordinates[j]  - minCorner.coordinates[j]) / distance;
        }
    }
    
    delete[] minCoordinates;
    delete[] maxCoordinates;
}

