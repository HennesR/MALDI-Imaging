//
//  calculateDistanceMatrix.hpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateDistanceMatrix_hpp
#define calculateDistanceMatrix_hpp

#include "../../../shared/Point/Point.h"

void calculateDistanceMatrix(Point* dataPoints, double* distanceMatrix, unsigned int pointsSize, unsigned int pointsDimension);

#endif /* calculateDistanceMatrix_hpp */
