//
//  calculateDistanceMatrix.cpp
//  hSNE
//
//  Created by Florian Fechner on 01.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateDistanceMatrix.hpp"

#include <iostream>

void calculateDistanceMatrix(Point* dataPoints, double* distanceMatrix, unsigned int pointsSize, unsigned int pointsDimension)
{
    for(int i=0; i<pointsSize; i++)
    {
        distanceMatrix[i * pointsSize + i] = 0.0;
        
        for(int j=i+1; j<pointsSize; j++)
        {
            Point* p1 = &dataPoints[i];
            Point* p2 = &dataPoints[j];
            
            distanceMatrix[i * pointsSize + j] = p1->distance(p2);
            distanceMatrix[j * pointsSize + i] = p1->distance(p2);
        }
    }
}
