//
//  drillDown.hpp
//  hSNE
//
//  Created by Florian Fechner on 22.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef drillDown_hpp
#define drillDown_hpp

#include <vector>
#include "../../shared/Matrix/Matrix.hpp"

std::vector<unsigned int> drillDown(unsigned int currentScale, std::vector<unsigned int> selectedPointsIndices, std::vector<unsigned int>* landmarksIndices, Matrix* influenceMatrices,   double treshhold);
std::vector<std::pair<unsigned int, double>> drillDownWithoutTreshhold(unsigned int currentScale, std::vector<std::pair<unsigned int, double>> selectedLandmarks, std::vector<unsigned int>* landmarksIndices, Matrix* influenceMatrices);

#endif /* drillDown_hpp */
