//
//  drillDown.cpp
//  hSNE
//
//  Created by Florian Fechner on 22.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "drillDown.hpp"

#include "../../shared/printProgress/printProgress.hpp"

std::vector<unsigned int> drillDown(unsigned int currentScale, std::vector<unsigned int> selectedLandmarks, std::vector<unsigned int>* landmarksIndices, Matrix* influenceMatrices,  double treshhold)
{
    std::vector<unsigned int> result;
    
    std::vector<unsigned int> landmarksIndicesAtLowerScale = landmarksIndices[currentScale - 1];
    std::vector<unsigned int> landmarksIndicesAtCurrentScale = landmarksIndices[currentScale];
    
    Matrix influenceMatrixAtCurrentScale = influenceMatrices[currentScale];
    
    for(unsigned int i=0; i<landmarksIndicesAtLowerScale.size(); i++)
    {
        std::vector<std::pair<unsigned int, double>> column = influenceMatrixAtCurrentScale.atColumn(i);
        
        double accumulation = 0.0;
        
        for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
        {
            std::pair<unsigned int, double> entry = *it;
        
            for(std::vector<unsigned int>::iterator it2 = selectedLandmarks.begin(); it2 != selectedLandmarks.end(); ++it2)
            {
                if((*it2) == landmarksIndicesAtCurrentScale.at(entry.first))
                {
                    accumulation += entry.second;
                }
            }
        }
        
        if(accumulation >= treshhold)
        {
            result.push_back(landmarksIndicesAtLowerScale[i]);
        }
    }
    
    return result;
}

std::vector<std::pair<unsigned int, double>> drillDownWithoutTreshhold(unsigned int currentScale, std::vector<std::pair<unsigned int, double>> selectedLandmarks, std::vector<unsigned int>* landmarksIndices, Matrix* influenceMatrices)
{
    const double EPSILON = 0.01;

    std::vector<std::pair<unsigned int, double>> result;

    std::vector<unsigned int> landmarksIndicesAtLowerScale = landmarksIndices[currentScale - 1];
    std::vector<unsigned int> landmarksIndicesAtCurrentScale = landmarksIndices[currentScale];

    Matrix influenceMatrixAtCurrentScale = influenceMatrices[currentScale];

    for(unsigned int i=0; i<landmarksIndicesAtLowerScale.size(); i++)
    {
        std::vector<std::pair<unsigned int, double>> column = influenceMatrixAtCurrentScale.atColumn(i);

        double accumulation = 0.0;

        for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
        {
            std::pair<unsigned int, double> entry = *it;

            for(std::vector<std::pair<unsigned int, double>>::iterator it2 = selectedLandmarks.begin(); it2 != selectedLandmarks.end(); ++it2)
            {
                if(it2->first == landmarksIndicesAtCurrentScale.at(entry.first))
                {
                    accumulation += entry.second * it2->second;
                }
            }
        }

        if(accumulation >= EPSILON)
        {
            result.push_back(std::make_pair(landmarksIndicesAtLowerScale[i], accumulation));
        }
    }

    return result;
}
