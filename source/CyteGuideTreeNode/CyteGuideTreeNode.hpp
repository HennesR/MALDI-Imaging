
#ifndef CyteGuideTreeNode_hpp
#define CyteGuideTreeNode_hpp

#include <vector>

#include "../shared/Point/Point.h"
#include "../shared/Matrix/Matrix.hpp"
#include "../shared/HSNEResults/HSNEResult.hpp"

class CyteGuideTreeNode
{
public:
    CyteGuideTreeNode();
    CyteGuideTreeNode(unsigned int scale, unsigned int maximumScale, Point* points, unsigned int pointsSize, unsigned int pointsDimension, HSNEResults* hsneResults, unsigned int index, unsigned int depth, unsigned int imageWidth);
    CyteGuideTreeNode(unsigned int scale, unsigned int maximumScale, Point* points, unsigned int pointsDimension, std::vector<unsigned int> filteredPointsIndices, HSNEResults* hsneResults, unsigned int index, unsigned int depth, unsigned int imageWidth);

    void saveToFile(std::string filePath);
    void loadFromFile(std::string filePath, unsigned int index, unsigned int depth, Point* points, unsigned int pointsDimension, unsigned int imageWidth);

    std::vector<std::tuple<unsigned int, double, double, double>> calculateTSNEEmbeddingByLandmarkIndices(unsigned int scale, std::vector<unsigned int> selectedIndices, HSNEResults* hsneResults, Point* points, unsigned int pointsDimension, double treshhold);

    std::vector<std::vector<std::pair<unsigned int, double>>> containingPoints;

    std::vector<double> spatialStandardDeviations;
    std::vector<double> mzStandardDeviations;
    std::vector<double> amountOfContainingPoints;

    double spatialStandardDeviationsMin;
    double mzStandardDeviationsMin;
    double amountOfContainingPointsMin;

    double spatialStandardDeviationsMax;
    double mzStandardDeviationsMax;
    double amountOfContainingPointsMax;

    std::vector<std::pair<double, double>> embeddedPoints;
    std::vector<unsigned int> clusterMap;
    unsigned int numberOfClusters;
    std::vector<std::vector<std::pair<unsigned int, double>>> areasOfInfluence;

    std::vector<unsigned int> globalLandmarkIndices;
    unsigned int maximumKMeansIterations;
    unsigned int maximumTSNEIterations;
    double drillDownTreshhold;
    unsigned int index;
    unsigned int depth;

    std::vector<CyteGuideTreeNode> children;
    std::vector<bool> opened;

private:
    void buildUp(unsigned int scale, unsigned int maximumScale, Point* points, unsigned int pointsDimension, std::vector<unsigned int> filteredPointsIndices, HSNEResults* hsneResults, unsigned int index, unsigned int depth, unsigned int imageWidth);
    void setLandmarkStatistics(Point* points, unsigned int pointsDimension, unsigned int imageWidth);
};

#endif /* CyteGuideTreeNode_hpp */
