//
//  calculateAreaOfInfluence.hpp
//  hSNE
//
//  Created by Florian Fechner on 03.12.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#ifndef calculateAreaOfInfluence_hpp
#define calculateAreaOfInfluence_hpp

#include <utility>
#include <vector>

#include "../../shared/Matrix/Matrix.hpp"

std::vector<std::pair<unsigned int, double>> calculateAreaOfInfluence(unsigned int currentScale, std::vector<unsigned int> selectedLandmarks, std::vector<unsigned int>* landmarksIndices, Matrix* influenceMatrices);

#endif /* calculateAreaOfInfluence_hpp */
