//
//  calculateAreaOfInfluence.cpp
//  hSNE
//
//  Created by Florian Fechner on 03.12.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "calculateAreaOfInfluence.hpp"

#include "../../shared/printProgress/printProgress.hpp"


std::vector<std::pair<unsigned int, double>> calculateAreaOfInfluence(unsigned int currentScale, std::vector<unsigned int> selectedLandmarks, std::vector<unsigned int>* landmarksIndices, Matrix* influenceMatrices)
{
    std::vector<std::pair<unsigned int, double>> influences;
    std::vector<std::pair<unsigned int, double>> temp;
    
    for(unsigned int i=0; i<selectedLandmarks.size(); i++)
    {
        influences.push_back(std::make_pair(selectedLandmarks.at(i), 1.0));
    }
    
    for(unsigned int h=currentScale; h > 0; h--)
    {
        printProgress("calculate Area of Influence", currentScale - h, currentScale);
        
        std::vector<unsigned int> landmarksIndicesAtLowerScale = landmarksIndices[h - 1];
        std::vector<unsigned int> landmarksIndicesAtCurrentScale = landmarksIndices[h];
        
        Matrix influenceMatrixAtCurrentScale = influenceMatrices[h];
        
        for(unsigned int i=0; i<landmarksIndicesAtLowerScale.size(); i++)
        {
            std::vector<std::pair<unsigned int, double>> column = influenceMatrixAtCurrentScale.atColumn(i);
            
            double accumulation = 0.0;
            
            for(std::vector<std::pair<unsigned int, double>>::iterator it = column.begin(); it != column.end(); ++it)
            {
                std::pair<unsigned int, double> entry = *it;
                
                for(std::vector<std::pair<unsigned int, double>>::iterator it2 = influences.begin(); it2 != influences.end(); ++it2)
                {
                    if((*it2).first == landmarksIndicesAtCurrentScale.at(entry.first))
                    {
                        accumulation += entry.second * (*it2).second;
                    }
                }
            }
            
            if(accumulation > 0.0)
            {
                temp.push_back(std::make_pair(landmarksIndicesAtLowerScale.at(i), accumulation));
            }
        }
        
        influences = temp;
    }
    
    return influences;
}
