//
//  CyteGuideTreeNode.cpp
//  hSNE
//
//  Created by Florian Fechner on 26.11.18.
//  Copyright © 2018 Florian Fechner. All rights reserved.
//

#include "CyteGuideTreeNode.hpp"

#include "drillDown/drillDown.hpp"
#include "calculateAreaOfInfluence/calculateAreaOfInfluence.hpp"
#include "saveAreaOfInfluence/saveAreaOfInfluence.hpp"
#include "tSNE/tSNE.h"
#include "../shared/kMeans/kMeans.hpp"
#include "../shared/printProgress/printProgress.hpp"

#include <string>
#include <algorithm>
#include <iostream>
#include <limits>
#include <cmath>
#include <vector>
#include <fstream>

struct vec2
{
    double x, y;
};

vec2 calculateSpatialMean(std::vector<std::pair<unsigned int, double>> containingPointIndices, unsigned int imageWidth)
{
    vec2 sum;
    double total = 0.0;

    for(unsigned int i=0; i<containingPointIndices.size(); i++)
    {
        unsigned int index = containingPointIndices.at(i).first;
        double weight = containingPointIndices.at(i).second;

        unsigned int x = index % imageWidth;
        unsigned int y = index / imageWidth;

        sum.x += x * weight;
        sum.y += y * weight;

        total += weight;
    }

    vec2 result;
    result.x = sum.x / total;
    result.y = sum.y / total;

    return result;
}

double calculateSpatialStandardDeviation(std::vector<std::pair<unsigned int, double>> containingPointIndices, unsigned int imageWidth)
{
    double diff = 0.0;
    double total = 0.0;
    vec2 mean = calculateSpatialMean(containingPointIndices, imageWidth);

    for(unsigned int i=0; i<containingPointIndices.size(); i++)
    {
        unsigned int index = containingPointIndices.at(i).first;
        double weight = containingPointIndices.at(i).second;

        unsigned int x = index % imageWidth;
        unsigned int y = index / imageWidth;

        double distance = sqrt(pow(mean.x - x, 2) + pow(mean.y - y, 2));

        diff += pow(distance, 2) * weight;

        total += weight;
    }

    return sqrt(diff * (1.0 / total));
}

std::vector<double> calculateSpatialStandardDeviations(std::vector<std::vector<std::pair<unsigned int, double>>> containingPointsIndices, unsigned int imageWidth)
{
    std::vector<double> result;

    for(unsigned int i=0; i<containingPointsIndices.size(); i++)
    {
        result.push_back(calculateSpatialStandardDeviation(containingPointsIndices.at(i), imageWidth));
    }

    return result;
}

Point calculateMZMean(std::vector<std::pair<unsigned int, double>> containingPointIndices, Point* points, unsigned int pointsDimension)
{
    Point sum;
    double total = 0.0;
    initializePoint(&sum, pointsDimension);

    for(unsigned int i=0; i<containingPointIndices.size(); i++)
    {
        unsigned int index = containingPointIndices.at(i).first;
        double weight = containingPointIndices.at(i).second;

        sum = sum + points[index] * weight;
        total += weight;
    }

    return (sum * (1.0 / total));
}

double calculateMZStandardDeviation(std::vector<std::pair<unsigned int, double>> containingPointIndices, Point* points, unsigned int pointsDimension)
{
    double diff = 0.0;
    double total = 0.0;
    Point mean = calculateMZMean(containingPointIndices, points, pointsDimension);

    for(unsigned int i=0; i<containingPointIndices.size(); i++)
    {
        unsigned int index = containingPointIndices.at(i).first;
        double weight = containingPointIndices.at(i).second;

        diff += pow(mean.distance(&points[index]), 2) * weight;

        total += weight;
    }

    return sqrt(diff * (1.0 / total));
}

std::vector<double> calculateMZStandardDeviations(std::vector<std::vector<std::pair<unsigned int, double>>> containingPointsIndices, Point* points, unsigned int pointsDimension)
{
    std::vector<double> result;

    for(unsigned int i=0; i<containingPointsIndices.size(); i++)
    {
        result.push_back(calculateMZStandardDeviation(containingPointsIndices.at(i), points, pointsDimension));
    }

    return result;
}

std::vector<unsigned int> drillDownToBottom(unsigned int currentScale, std::vector<unsigned int> selectedLandmarks, std::vector<unsigned int>* landmarksIndices, Matrix* influenceMatrices,  double treshhold)
{
    std::vector<unsigned int> result;

    for(unsigned int i=currentScale; i>0; i--)
    {
        result = drillDown(i, selectedLandmarks, landmarksIndices, influenceMatrices, treshhold);
        selectedLandmarks = result;
    }

    return result;
}

std::vector<std::pair<unsigned int, double>> drillDownToBottomWithoutTreshhold(unsigned int currentScale, std::vector<unsigned int> selectedLandmarks, std::vector<unsigned int>* landmarksIndices, Matrix* influenceMatrices)
{
    std::vector<std::pair<unsigned int, double>> base;
    std::vector<std::pair<unsigned int, double>> result;

    for(unsigned int i=0; i<selectedLandmarks.size(); i++)
    {
        base.push_back(std::make_pair(selectedLandmarks.at(i), 1.0));
    }

    for(unsigned int i=currentScale; i>0; i--)
    {
        result = drillDownWithoutTreshhold(i, base, landmarksIndices, influenceMatrices);
        base = result;
    }

    return result;
}

void filterPointsArrayByIndices(Point* filteredPoints, Point* points, std::vector<unsigned int> indices)
{
    for(unsigned int i=0; i<indices.size(); i++)
    {
        filteredPoints[i] = points[indices.at(i)];
    }
}

std::vector<unsigned int> getGlobalLandmarkIndices(std::vector<unsigned int> selected, std::vector<unsigned int>* localLandmarkIndices, unsigned int scale)
{
    if(scale == 0) return selected;

    std::vector<unsigned int> result;

    for(unsigned int i=0; i<selected.size(); i++)
    {
        unsigned int index = selected.at(i);

        for(int j=scale-1; j>=0; j--)
        {
            index = localLandmarkIndices[j].at(index);
        }

        result.push_back(index);
    }

    return result;
}

CyteGuideTreeNode::CyteGuideTreeNode()
{

}

CyteGuideTreeNode::CyteGuideTreeNode(unsigned int scale, unsigned int maximumScale, Point* points, unsigned int pointsSize, unsigned int pointsDimension, HSNEResults* hsneResults, unsigned int index, unsigned int depth, unsigned int imageWidth)
{
    this->numberOfClusters = 4;

    this->index = index;
    this->depth = depth;

    this->maximumKMeansIterations = 2000;
    this->maximumTSNEIterations = 10000;
    this->drillDownTreshhold = 0.65;

    this->buildUp(scale, maximumScale, points, pointsDimension, hsneResults->localLandmarksIndices[maximumScale], hsneResults, index, depth, imageWidth);
    setLandmarkStatistics(points, pointsDimension, imageWidth);
}

CyteGuideTreeNode::CyteGuideTreeNode(unsigned int scale, unsigned int maximumScale, Point* points, unsigned int pointsDimension, std::vector<unsigned int> filteredPointsIndices, HSNEResults* hsneResults, unsigned int index, unsigned int depth, unsigned int imageWidth)
{
    this->numberOfClusters = 4;

    this->index = index;
    this->depth = depth;

    this->maximumKMeansIterations = 2000;
    this->maximumTSNEIterations = 10000;
    this->drillDownTreshhold = 0.65;

    this->buildUp(scale, maximumScale, points, pointsDimension, filteredPointsIndices, hsneResults, index, depth, imageWidth);
    setLandmarkStatistics(points, pointsDimension, imageWidth);
}

/*
void CyteGuideTreeNode::clusterAutomatically()
{

}
*/

void CyteGuideTreeNode::buildUp(unsigned int scale, unsigned int maximumScale, Point* points, unsigned int pointsDimension, std::vector<unsigned int> selectedLandmarkIndices, HSNEResults* hsneResults, unsigned int index, unsigned int depth, unsigned int imageWidth)
{
    if(selectedLandmarkIndices.size() > 0)
    {
        Point* means = new Point[this->numberOfClusters];
        initializePointsArray(means, pointsDimension, this->numberOfClusters);

        unsigned int filteredPointsSize = (unsigned int)(selectedLandmarkIndices.size());
        Point* filteredPoints = new Point[filteredPointsSize];

        if(scale > 0)
        {
            for(unsigned int i=0; i<selectedLandmarkIndices.size(); i++)
            {
                printProgress("drill down to bottom", i, selectedLandmarkIndices.size());

                std::vector<unsigned int> tempIndices;
                tempIndices.push_back(selectedLandmarkIndices.at(i));

                std::vector<std::pair<unsigned int, double>> containingPointsIndices = drillDownToBottomWithoutTreshhold(scale, tempIndices, hsneResults->localLandmarksIndices, hsneResults->influenceMatrices);

                this->containingPoints.push_back(containingPointsIndices);
            }
        }

        this->globalLandmarkIndices = getGlobalLandmarkIndices(selectedLandmarkIndices, hsneResults->localLandmarksIndices, scale);

        for(unsigned int i=0; i<filteredPointsSize; i++)
        {
            filteredPoints[i] = points[this->globalLandmarkIndices.at(i)];
        }

        kMeans(means, filteredPoints, filteredPointsSize, pointsDimension, this->numberOfClusters, maximumKMeansIterations);

        std::cout << "kmeans finished"<< std::endl;

        for(unsigned int i=0; i<filteredPointsSize; i++)
        {
            unsigned int minDistanceIndex = 0;
            double minDistance = std::numeric_limits<double>::max();

            for(unsigned int j=0; j<this->numberOfClusters; j++)
            {
                if(filteredPoints[i].distance(&means[j]) < minDistance)
                {
                    minDistance = filteredPoints[i].distance(&means[j]);
                    minDistanceIndex = j;
                }
            }

            this->clusterMap.push_back(minDistanceIndex);
        }

        std::cout << "cluster finished"<< std::endl;

        if(scale > 0)
        {
            this->areasOfInfluence.resize(this->numberOfClusters);

            for(unsigned int i=0; i<this->numberOfClusters; i++)
            {
                std::vector<unsigned int> pointsIndicesOfCluster;

                for(unsigned int j=0; j<selectedLandmarkIndices.size(); j++)
                {
                    unsigned int clusterIndex = clusterMap.at(j);

                    if(clusterIndex == i)
                    {
                        pointsIndicesOfCluster.push_back(selectedLandmarkIndices.at(j));
                    }
                }

                this->areasOfInfluence[i] = calculateAreaOfInfluence(scale, pointsIndicesOfCluster, hsneResults->localLandmarksIndices, hsneResults->influenceMatrices);

                std::vector<unsigned int> drillDownedPointsIndices = drillDown(scale, pointsIndicesOfCluster, hsneResults->localLandmarksIndices, hsneResults->influenceMatrices, drillDownTreshhold);

                std::cout << "drilldown finished" << std::endl;

                unsigned int nextIndex = index * this->numberOfClusters + i;

                CyteGuideTreeNode child(scale - 1, maximumScale, points, pointsDimension, drillDownedPointsIndices, hsneResults, nextIndex, depth + 1, imageWidth);
                this->children.push_back(child);
            }
        }

        Point* dataPoints = new Point[selectedLandmarkIndices.size()];
        initializePointsArray(dataPoints, pointsDimension, selectedLandmarkIndices.size());
        Point* embeddedPoints = new Point[selectedLandmarkIndices.size()];
        initializePointsArray(embeddedPoints, 2, selectedLandmarkIndices.size());
        std::vector<unsigned int> globalFilteredPointsIndices = getGlobalLandmarkIndices(selectedLandmarkIndices, hsneResults->localLandmarksIndices, scale);

        for(unsigned int i=0; i<selectedLandmarkIndices.size(); i++)
        {
            dataPoints[i] = points[globalFilteredPointsIndices[i]];
        }

        tSNE(embeddedPoints, dataPoints, selectedLandmarkIndices.size(), pointsDimension, 5, maximumTSNEIterations);

        std::cout << "tSNE finished!"<< std::endl;

        for(unsigned int i=0; i<selectedLandmarkIndices.size(); i++)
        {
            std::pair<double, double> embeddedPoint(embeddedPoints[i].coordinates[0], embeddedPoints[i].coordinates[1]);
            this->embeddedPoints.push_back(embeddedPoint);
        }

        delete[] means;
        delete[] embeddedPoints;
        delete[] dataPoints;
        delete[] filteredPoints;
    }
}

void CyteGuideTreeNode::setLandmarkStatistics(Point* points, unsigned int pointsDimension, unsigned int imageWidth)
{
    for(unsigned int i=0; i<this->containingPoints.size(); i++)
    {
        double amount = 0.0;
        std::vector<std::pair<unsigned int, double>> row = this->containingPoints.at(i);

        for(unsigned int j=0; j<row.size(); j++)
        {
            amount += row.at(j).second;
        }

        this->amountOfContainingPoints.push_back(amount);
    }

    this->amountOfContainingPointsMin = (this->amountOfContainingPoints.empty()) ? 0.0 : *std::min_element(this->amountOfContainingPoints.begin(), this->amountOfContainingPoints.end());
    this->amountOfContainingPointsMax = (this->amountOfContainingPoints.empty()) ? 0.0 : *std::max_element(this->amountOfContainingPoints.begin(), this->amountOfContainingPoints.end());

    this->mzStandardDeviations = calculateMZStandardDeviations(this->containingPoints, points, pointsDimension);
    this->mzStandardDeviationsMin = (this->mzStandardDeviations.empty()) ? 0.0 : *std::min_element(this->mzStandardDeviations.begin(), this->mzStandardDeviations.end());
    this->mzStandardDeviationsMax = (this->mzStandardDeviations.empty()) ? 0.0 : *std::max_element(this->mzStandardDeviations.begin(), this->mzStandardDeviations.end());

    this->spatialStandardDeviations = calculateSpatialStandardDeviations(this->containingPoints, imageWidth);
    this->spatialStandardDeviationsMin = (this->spatialStandardDeviations.empty()) ? 0.0 : *std::min_element(this->spatialStandardDeviations.begin(), this->spatialStandardDeviations.end());
    this->spatialStandardDeviationsMax = (this->spatialStandardDeviations.empty()) ? 0.0 : *std::max_element(this->spatialStandardDeviations.begin(), this->spatialStandardDeviations.end());
}

std::vector<std::tuple<unsigned int, double, double, double>> CyteGuideTreeNode::calculateTSNEEmbeddingByLandmarkIndices(unsigned int scale, std::vector<unsigned int> selectedIndices, HSNEResults* hsneResults, Point* points, unsigned int pointsDimension, double treshhold)
{
    std::vector<std::pair<unsigned int, double>> tsneReadyIndices = drillDownToBottomWithoutTreshhold(scale, selectedIndices, hsneResults->localLandmarksIndices, hsneResults->influenceMatrices);
    std::vector<std::pair<unsigned int, double>> filteredTsneReadyIndices;

    for(unsigned int i=0; i<tsneReadyIndices.size(); i++)
    {
        if(tsneReadyIndices.at(i).first > treshhold)
        {
            filteredTsneReadyIndices.push_back(tsneReadyIndices.at(i));
        }
    }

    Point* tsneReadyPoints = new Point[filteredTsneReadyIndices.size()];
    initializePointsArray(tsneReadyPoints, pointsDimension, filteredTsneReadyIndices.size());

    for(unsigned int i=0; i<filteredTsneReadyIndices.size(); i++)
    {
       tsneReadyPoints[i] = points[filteredTsneReadyIndices.at(i).first];
    }

    Point* resultPoints = new Point[filteredTsneReadyIndices.size()];
    initializePointsArray(resultPoints, 2, filteredTsneReadyIndices.size());

    tSNE(resultPoints, tsneReadyPoints, filteredTsneReadyIndices.size(), pointsDimension, 15, 1000);

    std::vector<std::tuple<unsigned int, double, double, double>> result;

    for(unsigned int i=0; i<filteredTsneReadyIndices.size(); i++)
    {
        result.push_back(std::make_tuple(filteredTsneReadyIndices[i].first, filteredTsneReadyIndices[i].second, resultPoints[i].coordinates[0], resultPoints[i].coordinates[1]));
    }

    return result;
}

void CyteGuideTreeNode::saveToFile(std::string filePath)
{
    /*
     std::vector<std::pair<double, double>> embeddedPoints;
     std::vector<unsigned int> clusterMap;
     unsigned int numberOfClusters;
     std::vector<std::vector<std::pair<unsigned int, double>>> areasOfInfluence;

     unsigned int maximumKMeansIterations;
     unsigned int maximumTSNEIterations;
     double drillDownTreshhold;
     unsigned int index;
     unsigned int depth;
     */

    std::ofstream fileStream;
    fileStream.open(filePath + "cyteGuideNode_" + std::to_string(this->index) + "_" + std::to_string(this->depth) + ".txt");

    for(std::vector<std::pair<double, double>>::iterator it = this->embeddedPoints.begin(); it != this->embeddedPoints.end(); ++it)
    {
        fileStream << it->first << "," << it->second << " ";
    }

    fileStream << "\n";

    for(std::vector<unsigned int >::iterator it = this->clusterMap.begin(); it != this->clusterMap.end(); ++it)
    {
        fileStream << *it << " ";
    }

    fileStream << "\n";
    fileStream << this->numberOfClusters << "\n";

    for(std::vector<std::vector<std::pair<unsigned int, double>>>::iterator it = this->areasOfInfluence.begin(); it != this->areasOfInfluence.end(); ++it)
    {
        for(std::vector<std::pair<unsigned int, double>>::iterator it2 = it->begin(); it2 != it->end(); ++it2)
        {
            fileStream << it2->first << "," << it2->second << " ";
        }

        fileStream << ":";
    }

    fileStream << "\n";

    for(std::vector<std::vector<std::pair<unsigned int, double>>>::iterator it = this->containingPoints.begin(); it != this->containingPoints.end(); ++it)
    {
        for(std::vector<std::pair<unsigned int, double>>::iterator it2 = it->begin(); it2 != it->end(); ++it2)
        {
            fileStream << it2->first << "," << it2->second << " ";
        }

        fileStream << ":";
    }

    fileStream << "\n";

    fileStream << this->maximumKMeansIterations << "\n";
    fileStream << this->maximumTSNEIterations << "\n";
    fileStream << this->drillDownTreshhold << "\n";
    fileStream << this->index << "\n";
    fileStream << this->depth << "\n";

    for(std::vector<unsigned int >::iterator it = this->globalLandmarkIndices.begin(); it != this->globalLandmarkIndices.end(); ++it)
    {
        fileStream << *it << " ";
    }

    fileStream << "\n";

    fileStream.close();

    for(unsigned int i=0; i<this->children.size(); i++)
    {
        CyteGuideTreeNode child = this->children.at(i);
        child.saveToFile(filePath);
    }
}

void CyteGuideTreeNode::loadFromFile(std::string filePath, unsigned int index, unsigned int depth, Point* points, unsigned int pointsDimension, unsigned int imageWidth)
{
    /*
     std::vector<std::pair<double, double>> embeddedPoints;
     std::vector<unsigned int> clusterMap;
     unsigned int numberOfClusters;
     std::vector<std::vector<std::pair<unsigned int, double>>> areasOfInfluence;

     unsigned int maximumKMeansIterations;
     unsigned int maximumTSNEIterations;
     double drillDownTreshhold;
     unsigned int index;
     unsigned int depth;
    */

    std::string line;
    std::ifstream myfile (filePath + "cyteGuideNode_" + std::to_string(index) + "_" + std::to_string(depth) + ".txt");
    unsigned int lineIndex = 0;

    if (myfile.is_open())
    {
        while(getline(myfile, line))
        {
            if(lineIndex == 0)
            {
                int pos1 = (int) (line.find(" "));

                while(pos1 >= 0)
                {
                    std::string token = line.substr(0, pos1);

                    int pos2 = (int) (line.find(","));
                    std::string firstToken = token.substr(0, pos2);
                    std::string secondToken = token.substr(pos2 + 1);

                    double first = std::stod(firstToken);
                    double second = std::stod(secondToken);

                    this->embeddedPoints.push_back(std::make_pair(first, second));

                    line = line.substr(pos1 + 1);
                    pos1 = (int) (line.find(" "));
                }
            }

            if(lineIndex == 1)
            {
                int pos = (int) (line.find(" "));

                while(pos >= 0)
                {
                    std::string token = line.substr(0, pos);
                    if(token.size() <= 0) break;
                    unsigned int value = std::stoi(token);

                    this->clusterMap.push_back(value);
                    pos = (int) (line.find(" "));
                    line = line.substr(pos + 1);
                }
            }

            if(lineIndex == 2)
            {
                this->numberOfClusters = std::stoi(line);
            }

            if(lineIndex == 3)
            {
                //areas of influence

                int pos1 = (int) (line.find(":"));

                while(pos1 >= 0)
                {
                    std::string token1 = line.substr(0, pos1);
                    int pos2 = (int) (token1.find(" "));
                    std::vector<std::pair<unsigned int, double>> areaOfInfluence;

                    while(pos2 >= 0)
                    {
                        std::string token2 = token1.substr(0, pos2);
                        int pos3 = (int) (token2.find(","));

                        std::string firstToken = token2.substr(0, pos3);
                        std::string secondToken = token2.substr(pos3 + 1);

                        unsigned int first = std::stoi(firstToken);
                        double second = std::stod(secondToken);

                        areaOfInfluence.push_back(std::make_pair(first, second));

                        token1 = token1.substr(pos2 + 1);
                        pos2 = (int) (token1.find(" "));
                    }

                    this->areasOfInfluence.push_back(areaOfInfluence);

                    line = line.substr(pos1 + 1);
                    pos1 = (int) (line.find(":"));
                }
            }

            if(lineIndex == 4)
            {
                int pos1 = (int) (line.find(":"));

                while(pos1 >= 0)
                {
                    std::string token1 = line.substr(0, pos1);
                    int pos2 = (int) (token1.find(" "));
                    std::vector<std::pair<unsigned int, double>> containingPointsToOneLandmark;

                    while(pos2 >= 0)
                    {
                        std::string token2 = token1.substr(0, pos2);
                        int pos3 = (int) (token2.find(","));

                        std::string firstToken = token2.substr(0, pos3);
                        std::string secondToken = token2.substr(pos3 + 1);

                        unsigned int first = std::stoi(firstToken);
                        double second = std::stod(secondToken);

                        containingPointsToOneLandmark.push_back(std::make_pair(first, second));

                        token1 = token1.substr(pos2 + 1);
                        pos2 = (int) (token1.find(" "));
                    }

                    this->containingPoints.push_back(containingPointsToOneLandmark);

                    line = line.substr(pos1 + 1);
                    pos1 = (int) (line.find(":"));
                }

                setLandmarkStatistics(points, pointsDimension, imageWidth);
            }

            if(lineIndex == 5)
            {
                this->maximumKMeansIterations = std::stoi(line);
            }

            if(lineIndex == 6)
            {
                this->maximumTSNEIterations = std::stoi(line);
            }

            if(lineIndex == 7)
            {
                this->drillDownTreshhold = std::stod(line);
            }

            if(lineIndex == 8)
            {
                this->index = std::stoi(line);
            }

            if(lineIndex == 9)
            {
                this->depth = std::stoi(line);
            }

            if(lineIndex == 10)
            {
                int pos = (int) (line.find(" "));

                while(pos >= 0)
                {
                    std::string token = line.substr(0, pos);
                    if(token.size() <= 0) break;
                    unsigned int value = std::stoi(token);

                    this->globalLandmarkIndices.push_back(value);
                    pos = (int) (line.find(" "));
                    line = line.substr(pos + 1);
                }
            }

            lineIndex++;
        }

        myfile.close();
    }

    for(unsigned int i=0; i<this->numberOfClusters && this->depth < 2; i++)
    {
        CyteGuideTreeNode child;
        child.loadFromFile(filePath, index * this->numberOfClusters + i, depth + 1, points, pointsDimension, imageWidth);

        this->children.push_back(child);
    }
}
