#ifndef PROJECTIONSPACE_H
#define PROJECTIONSPACE_H

#include <QQuickPaintedItem>
#include <QPainter>

#include "dataset.h"
#include "lassotool.h"

#include "shared.h"
#include "shared/Point/Point.h"
#include "CyteGuideTreeNode/CyteGuideTreeNode.hpp"


class ProjectionSpace : public QQuickPaintedItem
{
    Q_OBJECT
public:
    void initialize();
    void setDataSet(DataSet* dataSet);
    void setEmbeddedPoints(Point* embeddedPoints, unsigned int* embeddedPointsIndices, unsigned int pointsSize);
    void paint(QPainter* p) override;
    ~ProjectionSpace();

signals:
    void hoveredLandmarksCluster(const QVector<std::pair<unsigned int, double>>&, const QColor);
    void selectScatterSpaceTSNEPoints(const QVector<pointCluster>&);

private:
    void hoverMoveEvent(QHoverEvent* event) override;
    void mousePressEvent(QMouseEvent* event) override;
    void wheelEvent(QWheelEvent* event) override;

    void mousePressEventForMiniMap(qreal x, qreal y, qreal size);

    void paintCyteGuideNodeEmbedding(QPainter* p, CyteGuideTreeNode* cyteGuideNode, qreal x, qreal y, qreal size, qreal centerX, qreal centerY, double angle, unsigned int depth);
    void paintCyteGuideNode(QPainter* p, CyteGuideTreeNode* cyteGuideNode, double centerX, double centerY, unsigned int depth, qreal radius, double startAngle, double endAngle);

    void activateAmountOfBelongingPointsDisplayMode();
    void activateStdDevOfMzSpaceDisplayMode();
    void activateStdDevOfSpatialSpaceDisplayMode();

    void setLineWidth(double width);
    void setPointRadius(double radius);

    HSNEResults* hsneResults;
    LassoTool* lasso = nullptr;

    unsigned int displayMode;
    double zoom;
    int hoveredClusterIndex;

    double lineWidth;
    double pointRadius;

    std::vector<unsigned int> selectedCyteGuidePath;
    CyteGuideTreeNode cyteGuideTreeRoot;
    DataSet* dataSet;

};

#endif // PROJECTIONSPACE_H
