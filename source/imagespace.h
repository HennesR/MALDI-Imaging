#ifndef IMAGESPACE_H
#define IMAGESPACE_H

#include <QQuickPaintedItem>
#include <QPainter>
#include "shared.h"
#include "dataset.h"

class LassoTool;

class ImageSpace : public QQuickPaintedItem
{
    Q_OBJECT
public:
    void setDataSet( const DataSet* pDataSet );

    void setDimensions( const QVector<int>& dimensions );
    void setHighlight( const QVector<pixelCluster>& clusters );

    Q_INVOKABLE void toggleInputGroup( Group* pGroup );
    Q_INVOKABLE bool containsInputGroup( Group* pGroup ) const;
    Q_INVOKABLE void setOutputGroup( Group* pGroup );

private slots:
    void updateGroupData();

private:
    void componentComplete() override;
    void paint( QPainter* painter ) override;
    void hoverMoveEvent( QHoverEvent* event ) override;
    void mousePressEvent( QMouseEvent* event ) override;

    const DataSet*  _dataSet    = nullptr;
    LassoTool*      _lasso      = nullptr;

    QVector<const Group*> _inputGroups {};
    Group* _outputGroup = nullptr;

    QVector<uint32_t>   _backgroundData {};
    QImage              _backgroundImage {};

    QVector<uint32_t>   _groupsData {};
    QImage              _groupsImage {};

    QVector<uint32_t>   _highlightData {};
    QImage              _highlightImage {};
};

#endif // IMAGESPACE_H
