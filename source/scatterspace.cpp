#include "scatterspace.h"
#include "lassotool.h"

#include <QOpenGLFramebufferObjectFormat>
#include <QOpenGLExtraFunctions>
#include <QPropertyAnimation>
#include <QtMath>
#include <limits>
#include <cmath>

void ScatterSpace::setDataSet( const DataSet* pDataSet )
{
    QObject::connect( pDataSet, &DataSet::groupsCleared, [this]{
        this->setOutputGroup( nullptr );
    });
}
void ScatterSpace::setData( QVector<pointCluster> clusters, int width, int height )
{
    _lasso->clear();
    _selectionXY.clear();
    _selectionYZ.clear();

    // Find minimum and maximum values
    float maxX = std::numeric_limits<float>::min(), minX = std::numeric_limits<float>::max();
    float maxY = std::numeric_limits<float>::min(), minY = std::numeric_limits<float>::max();
    for( const auto& cluster : clusters ) {
        for( const auto& point : cluster.points ) {
            if( point.position.x > maxX ) maxX = point.position.x;
            else if( point.position.x < minX ) minX = point.position.x;

            if( point.position.y > maxY ) maxY = point.position.y;
            else if( point.position.y < minY ) minY = point.position.y;
        }
    }

    // Search next highest power of 2
    const int dimension = static_cast<int>( qNextPowerOfTwo( qMax( width, height ) ) );

    // Map coordinates between -1 and 1 | Calculate z base on hilbert curve
    for( auto& cluster : clusters ) {
        for( auto& point : cluster.points ) {
            point.position.x = ( point.position.x - minX ) / ( maxX - minX ) * 1.8f - 0.9f;
            point.position.y = ( point.position.y - minY ) / ( maxY - minY ) * 1.8f - 0.9f;

            const int x = point.pixelIndex % width, y = point.pixelIndex / width;
            point.position.z = ScatterSpace::HilbertCurve( dimension, x, y ) / static_cast<float>( dimension * dimension ) * 1.8f - 0.9f;
        }
    }

    _clusters = std::move( clusters );
    _clustersChanged = true;
    _rotation = 0.0f;
    this->update();
}
void ScatterSpace::setOutputGroup( Group* pGroup )
{
    _outputGroup = pGroup;
    _selectionXY.clear();
    _selectionYZ.clear();
    this->update();
}

int ScatterSpace::HilbertCurve( int dimension, int x, int y )
{
    int index = 0;

    for( int level = dimension / 2; level > 0; level /= 2 )
    {
        bool rotateX = (x & level) > 0;
        bool rotateY = (y & level) > 0;

        index += level * level * ( ( 3 * rotateX ) ^ rotateY );

        if( !rotateY ) {
            if( rotateX ) {
                x = level - 1 - x;
                y = level - 1 - y;
            }

            int tmp = x;
            x = y;
            y = tmp;
        }
    }

    return index;
}
QQuickFramebufferObject::Renderer* ScatterSpace::createRenderer() const
{
    return new ScatterSpace::Renderer;
}
void ScatterSpace::componentComplete()
{
    QQuickFramebufferObject::componentComplete();

    this->setAcceptHoverEvents( true );
    this->setAcceptedMouseButtons( Qt::LeftButton );

    _rotationAnimation.setDuration( 750 );
    _lasso = this->findChild<LassoTool*>( "scatterSpace_lasso" );

    QObject::connect( this, &ScatterSpace::onPropertyChanged, [this]{ this->update(); } );
}

void ScatterSpace::hoverEnterEvent( QHoverEvent* event )
{
    Q_UNUSED( event )
    this->setFocus( true );
}
void ScatterSpace::hoverLeaveEvent( QHoverEvent* event )
{
    Q_UNUSED( event )
    this->setFocus( false );
}
void ScatterSpace::hoverMoveEvent( QHoverEvent* event )
{
    _lasso->hoverMoveEvent( event );
}

void ScatterSpace::mousePressEvent( QMouseEvent* event )
{
    _lasso->mousePressEvent( event );
    if( _lasso->finished() ) {
        QPolygonF polygon = _lasso->normalizedPolygon();

        if( !( ( static_cast<int>( _rotation ) - 180 ) % 360 ) || !( ( static_cast<int>( _rotation ) + 90 ) % 360 ) ) {
            for( auto& point : polygon ) point.setX( 1.0 - point.x() );
        }

        for( int i = 0; i < polygon.size(); ++i )
            polygon[i] = QPointF( polygon[i].x() * 2.0 - 1.0, polygon[i].y() * 2.0 - 1.0 );

        if( static_cast<int>( _rotation / 90.0f ) & 0x01 ) { // YZ-plane
            _selectionYZ = std::move( polygon );
            if( _selectionXY.empty() )
                _selectionXY = QPolygonF( { { -1.0, -1.0 }, { 1.0, -1.0 }, { 1.0, 1.0 }, { -1.0, 1.0 } } );
        } else { // XY-plane
            _selectionXY = std::move( polygon );
            if( _selectionYZ.empty() )
                _selectionYZ = QPolygonF( { { -1.0, -1.0 }, { 1.0, -1.0 }, { 1.0, 1.0 }, { -1.0, 1.0 } } );
        }

        _lasso->clear();
        _clustersChanged = true;
        this->update();
    }
}
void ScatterSpace::keyPressEvent( QKeyEvent* event )
{
    if( _rotationAnimation.state() ) return;

    switch( event->key() )
    {
    case Qt::Key_Left:
        _rotationAnimation.setEndValue( _rotation + 90.0f );
        _rotationAnimation.start();
        _lasso->clear();
        break;
    case Qt::Key_Right:
        _rotationAnimation.setEndValue( _rotation - 90.0f );
        _rotationAnimation.start();
        _lasso->clear();
        break;
    }
}





ScatterSpace::Renderer::Renderer()
{
#ifdef __APPLE__
    return;
#endif
    const char* vertexSource = R"glsl( #version 130
                                uniform vec3 colors[20];
                                uniform mat3 view;

                                in vec3  inPosition;
                                in float inOpacity;
                                in uint  inCluster;

                                out vec4 color;

                                void main()
                                {
                                    if( ( inCluster >> 31 ) != 0u ) color = vec4( vec3( 1.0, 0.95, 0.0 ), inOpacity );
                                    else color = vec4( colors[inCluster], inOpacity );

                                    gl_Position = vec4( view * inPosition, 1.0 );
                                }
                               )glsl";

    const char* fragmentSource = R"glsl( #version 130
                                 in vec4 color;

                                 out vec4 outColor;

                                 void main()
                                 {
                                    outColor = color;
                                 }
                                )glsl";

    QOpenGLExtraFunctions* f = QOpenGLContext::currentContext()->extraFunctions();

    // Create shaders
    _vertexShader = f->glCreateShader( GL_VERTEX_SHADER );
    f->glShaderSource( _vertexShader, 1, &vertexSource, nullptr );
    f->glCompileShader( _vertexShader );

    _fragmentShader = f->glCreateShader(GL_FRAGMENT_SHADER);
    f->glShaderSource( _fragmentShader, 1, &fragmentSource, nullptr );
    f->glCompileShader( _fragmentShader );

    GLint status; char infolog[512];
    f->glGetShaderiv( _vertexShader, GL_COMPILE_STATUS, &status );
    if(status != GL_TRUE){
        f->glGetShaderInfoLog( _vertexShader, 512, nullptr, infolog );
        qDebug() << "[ Vertex-Shader compilation failed ]\n" << infolog;
        std::exit( EXIT_FAILURE );
    }
    f->glGetShaderiv( _fragmentShader, GL_COMPILE_STATUS, &status );
    if(status != GL_TRUE){
        f->glGetShaderInfoLog( _fragmentShader, 512, nullptr, infolog );
        qDebug() << "[ Fragment-Shader compilation failed ]\n" << infolog;
        std::exit( EXIT_FAILURE );
    }

    // Create shader program
    _shaderProgram = f->glCreateProgram();
    f->glAttachShader( _shaderProgram, _vertexShader );
    f->glAttachShader( _shaderProgram, _fragmentShader );
    f->glLinkProgram( _shaderProgram );

    // Create VBO
    f->glGenBuffers( 1, &_pointsVBO );

    // Create VAO
    f->glGenVertexArrays( 1, &_VAO );
    f->glBindVertexArray( _VAO );

    f->glBindBuffer( GL_ARRAY_BUFFER, _pointsVBO );

    f->glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat) + sizeof(GLuint), nullptr );
    f->glEnableVertexAttribArray( 0);

    f->glVertexAttribPointer( 1, 1, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat) + sizeof(GLuint), reinterpret_cast<GLvoid*>( 3 * sizeof(GLfloat) ) );
    f->glEnableVertexAttribArray( 1 );

    f->glVertexAttribIPointer( 2, 1, GL_UNSIGNED_INT, 4 * sizeof(GLfloat) + sizeof(GLuint), reinterpret_cast<GLvoid*>( 4 * sizeof(GLfloat) ) );
    f->glEnableVertexAttribArray( 2 );

    f->glBindVertexArray( 0 );
}
ScatterSpace::Renderer::~Renderer()
{
    QOpenGLExtraFunctions* f = QOpenGLContext::currentContext()->extraFunctions();
    f->glDeleteVertexArrays( 1, &_VAO );
    f->glDeleteBuffers( 1, &_pointsVBO );
    f->glDeleteProgram( _shaderProgram );
    f->glDeleteShader( _fragmentShader );
    f->glDeleteShader( _vertexShader );
}

QOpenGLFramebufferObject* ScatterSpace::Renderer::createFramebufferObject( const QSize& size )
{
    QOpenGLFramebufferObjectFormat format;
    format.setAttachment( QOpenGLFramebufferObject::CombinedDepthStencil );
    return new QOpenGLFramebufferObject( size, format );
}
void ScatterSpace::Renderer::synchronize( QQuickFramebufferObject* item )
{
    QOpenGLExtraFunctions* f = QOpenGLContext::currentContext()->extraFunctions();
    auto scatterSpace = static_cast<ScatterSpace*>( item );

    // Create view matrix
    const float radY = qDegreesToRadians( scatterSpace->_rotation ), cosY = std::cos( radY ), sinY = std::sin (radY );
    const float rotationYData[9] { cosY, 0.0f, sinY, 0.0f, 1.0f, 0.0f, -sinY, 0.0f, cosY };
    _viewMatrix = QMatrix3x3( rotationYData );

    if( scatterSpace->_clustersChanged ) {
        auto& clusters = scatterSpace->_clusters;

        // Collect colors & count total number of points
        _colors.resize( clusters.size() );
        int pointCount = 0;
        for( int i = 0; i < clusters.size(); ++i ) {
            _colors[i] = {
                clusters[i].color.red() / 255.0f,
                clusters[i].color.green() / 255.0f,
                clusters[i].color.blue() / 255.0f,
            };
            pointCount += clusters[i].points.size();
        }
        _pointCount = static_cast<GLsizei>( pointCount );

        // Fill dataPoints in format required by the renderer
        struct dataPoint { GLfloat x, y, z, opacity; GLuint cluster; };
        QVector<dataPoint> dataPoints( pointCount );
        QVector<int> selected;
        int dataPointIndex = 0;
        for( int i = 0; i < clusters.size(); ++i ) {
            for( const auto& point : clusters[i].points ) {

                GLuint cluster = static_cast<GLuint>( i );
                if( scatterSpace->_selectionXY.containsPoint( QPointF( point.position.x, point.position.y ), Qt::OddEvenFill )
                        && scatterSpace->_selectionYZ.containsPoint( QPointF( point.position.z, point.position.y ), Qt::OddEvenFill ) ) {
                    cluster |= ( 1u << 31 );
                    selected.push_back( point.pixelIndex );
                }

                dataPoints[dataPointIndex++] = dataPoint { point.position.x, point.position.y, point.position.z, point.opacity, cluster };
            }
        }

        f->glBindBuffer( GL_ARRAY_BUFFER, _pointsVBO );
        f->glBufferData( GL_ARRAY_BUFFER, dataPoints.size() * sizeof(dataPoint), dataPoints.data(), GL_STATIC_DRAW );
        f->glBindBuffer( GL_ARRAY_BUFFER, 0 );

        if( scatterSpace->_outputGroup ) scatterSpace->_outputGroup->setPixels( std::move( selected ) );
        scatterSpace->_clustersChanged = false;
    }
}
void ScatterSpace::Renderer::render()
{
    QOpenGLExtraFunctions* f = QOpenGLContext::currentContext()->extraFunctions();
    f->glBindVertexArray( _VAO );

    f->glUseProgram( _shaderProgram );
    f->glEnable( GL_DEPTH_TEST );

    f->glUniformMatrix3fv( f->glGetUniformLocation( _shaderProgram, "view" ), 1, GL_FALSE, _viewMatrix.data() );
    f->glUniform3fv( f->glGetUniformLocation( _shaderProgram, "colors" ), _colors.size(), reinterpret_cast<const GLfloat*>( _colors.data() ) );

    f->glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
    f->glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    f->glDrawArrays( GL_POINTS, 0, _pointCount );

    f->glBindVertexArray( 0 );
}
