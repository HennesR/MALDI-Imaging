#ifndef BOXPLOTSPACE_H
#define BOXPLOTSPACE_H

#include <QQuickPaintedItem>
#include <memory>
#include "dataset.h"

class QCustomPlot;

class BoxPlotSpace : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY( int dimensionCount MEMBER _dimensionCount NOTIFY dimensionCountChanged )
public:
    ~BoxPlotSpace() override;
    void setDataSet( const DataSet* pDataSet );

    Q_INVOKABLE void toggleInputGroup( Group* pGroup );
    Q_INVOKABLE bool containsInputGroup( Group* pGroup ) const;

signals:
    void dimensionCountChanged();
    void dimensionsChanged( const QVector<int>& dimensions );

private slots:
    void updateGroupData();
    void updateGroupColors();

private:
    void componentComplete() override;
    void paint( QPainter* painter ) override;

    const DataSet*          _dataSet = nullptr;
    QCustomPlot*            _customPlot = nullptr;
    QVector<const Group*>   _inputGroups {};

    std::unique_ptr<Group> _allPixelsGroup;
    bool _usingAllPixelGroup = false;

    int _dimensionCount = 1;

};

#endif // BOXPLOTSPACE_H
