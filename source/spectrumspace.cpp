#include "spectrumspace.h"
#include "./external/qcustomplot.h"

#include <algorithm>

SpectrumSpace::~SpectrumSpace()
{
    delete _customPlot;
}
void SpectrumSpace::setDataSet( const DataSet* pDataSet )
{
    _dataSet = pDataSet;

    QObject::connect( pDataSet, &DataSet::dataChanged, [this]{
        _customPlot->clearGraphs();

        // Prepare labels
        QVector<double> positions( _dataSet->dimensionCount() );
        QVector<QString> labels( _dataSet->dimensionCount() );
        for( int i = 0; i < _dataSet->dimensionCount(); ++i ) {
            positions[i] = i + 1;
            labels[i] = QString::number( static_cast<double>( _dataSet->mzValue( i ) ) );
        }

        // Sum up intensities for all mz-values
        QVector<double> intensities( _dataSet->dimensionCount(), 0.0 );
        for( int i = 0; i < _dataSet->dimensionCount(); ++i )
            for( int j = 0; j < _dataSet->pixelCount(); ++j )
                intensities[i] += static_cast<double>( _dataSet->intensity( j, i ) );

        // Fill bar chart with average intensity
        for( auto& intensity : intensities ) intensity /= _dataSet->pixelCount();
        _barChart->setData( positions, intensities );

        // Prepare x- and y-axis
        QSharedPointer<QCPAxisTickerText> ticker( new QCPAxisTickerText );
        ticker->setTicks( positions, labels );
        _customPlot->xAxis->setTicker( ticker );
        _customPlot->xAxis->setRange( 0, _dataSet->dimensionCount() + 1 );

        _customPlot->yAxis->rescale();
    });

    QObject::connect( pDataSet, &DataSet::groupsCleared, [this]{
        for( auto group : _inputGroups ) QObject::disconnect( group, nullptr, this, nullptr );
        _inputGroups.clear();
        this->updateGroupData();
    });
}

void SpectrumSpace::toggleInputGroup( Group* pGroup )
{
    if( this->containsInputGroup( pGroup ) ) {
        QObject::disconnect( pGroup, nullptr, this, nullptr );

        _inputGroups.removeOne( pGroup );
    } else {
        QObject::connect( pGroup, SIGNAL( pixelsChanged() ), this, SLOT( updateGroupData() ) );
        QObject::connect( pGroup, SIGNAL( colorChanged() ), this, SLOT( updateGroupColors() ) );

        _inputGroups.push_back( pGroup );
    }

    this->updateGroupData();
}
bool SpectrumSpace::containsInputGroup( Group* pGroup ) const
{
    return _inputGroups.contains( pGroup );
}



void SpectrumSpace::updateGroupData()
{
    _customPlot->clearGraphs();

    // Create graph for every group
    for( auto& group : _inputGroups )
    {
        auto upper = _customPlot->addGraph();
        auto lower = _customPlot->addGraph();

        // Set graph properties
        QColor color = group->color();
        color.setAlpha( 0x64 );
        upper->setBrush( color );
        upper->setPen( color );
        upper->setChannelFillGraph( lower );

        // Fill graphs
        QVector<float> intensities( group->pixels().size() );
        const auto begin = intensities.begin(), end = intensities.end();
        for( int i = 0; i < _dataSet->dimensionCount(); ++i )
        {
            // Gather intensities
            for( int j = 0; j < group->pixels().size(); ++j )
                intensities[j] = _dataSet->intensity( group->pixels()[j], i );


            // Add maximum to graph
            auto nth = end - 1;
            std::nth_element( begin, nth, end );
            upper->addData( i + 1.0, static_cast<double>( *nth ) );

            // Add minimum to graph
            nth = begin;
            std::nth_element( begin, nth, end );
            lower->addData( i + 1.0, static_cast<double>( *nth ) );
        }
    }

    this->updateGroupColors();
}
void SpectrumSpace::updateGroupColors()
{
    for( int i = 0; i < _inputGroups.size(); ++i ) {
        QColor color = _inputGroups[i]->color();
        color.setAlpha( 0x64 );
        _customPlot->graph( 2 * i )->setBrush( color );
        _customPlot->graph( 2 * i + 1 )->setPen( color );
    }
    this->update();
}

void SpectrumSpace::componentComplete()
{
    QQuickPaintedItem::componentComplete();

    this->setAcceptedMouseButtons( Qt::LeftButton | Qt::RightButton );

    _customPlot = new QCustomPlot;
    _customPlot->setSelectionTolerance( 5 );

    _customPlot->xAxis->setTickLength( 0, 2 );
    _customPlot->xAxis->setTickLabelRotation( 60.0 );
    _customPlot->xAxis->setSubTicks( false );
    _customPlot->xAxis->grid()->setVisible( false );
    _customPlot->xAxis->setTickLabels( false );

    _barChart = new QCPBars( _customPlot->xAxis, _customPlot->yAxis );
    _barChart->setSelectable( QCP::stMultipleDataRanges );
    _barChart->setBrush( QColor( 65, 70, 205 ) );
    _barChart->setPen( QColor( 65, 70, 205 ) );
    _barChart->selectionDecorator()->setBrush( QColor( 255, 240, 0 ) );
    _barChart->selectionDecorator()->setPen( QColor( 255, 240, 0 ) );
}
void SpectrumSpace::paint( QPainter* painter )
{
    painter->drawPixmap( QPoint(), _customPlot->toPixmap( static_cast<int>( this->width() ), static_cast<int>( this->height() ) ) );
}
void SpectrumSpace::wheelEvent( QWheelEvent* event )
{
    const double MIN = 0.0, MAX = _dataSet->dimensionCount() + 1;
    double min = _customPlot->xAxis->range().lower, max = _customPlot->xAxis->range().upper;

    // Shift range or zoom in/out
    if( event->modifiers() & Qt::ShiftModifier ) { // Shift
        if( event->delta() > 0 ) min -= 10.0, max -= 10.0;
        else min += 10.0, max += 10.0;
    }
    else { // Zoom
        if( event->delta() > 0 ) min += 20.0, max -= 20.0;
        else min -= 20.0, max += 20.0;
    }

    // Check bounds of range
    const double distance = max - min;
    if( distance > MAX - MIN ) { // Maximum distance
        min = MIN; max = MAX;
    }
    else if( distance <= 50.0 ) { // Minimum distance
        const double offset = ( 50.0 - distance ) / 2.0;
        min -= offset; max += offset;
        _customPlot->xAxis->setTickLabels( true );
    } else _customPlot->xAxis->setTickLabels( false );

    if( min < MIN ) { // Cap left
        const double offset = MIN - min;
        min += offset; max += offset;
    } else if( max > MAX ) { // Cap right
        const double offset = max - MAX;
        min -= offset; max -= offset;
    }

    _customPlot->xAxis->setRange( min, max );
    this->update();
}
void SpectrumSpace::mousePressEvent( QMouseEvent* event )
{
    QVariant details;
    _barChart->selectTest( event->pos(), false, &details );
    auto selection = details.value<QCPDataSelection>();

    QVector<int> selected( selection.dataPointCount() );
    if( selection.dataPointCount() ) {
        int current = 0;

        for( auto& range : selection.dataRanges( ))
            for( int i = range.begin(); i < range.end(); ++i )
                selected[current++] = i;
    }

    emit selectedDimensionsChanged( selected );
    _barChart->setSelection( selection );
    this->update();
}
