#ifndef GROUP_H
#define GROUP_H

#include <QObject>
#include <QColor>
#include <QVector>
#include <fstream>

class Group : public QObject
{
    Q_OBJECT
    Q_PROPERTY( QString name READ name WRITE setName NOTIFY nameChanged )
    Q_PROPERTY( QColor color READ color WRITE setColor NOTIFY colorChanged )
public:
    Group( QString name = "Missing Name", QColor color = QColor( 15, 210, 70 ) );

    bool loadFromFile( std::ifstream& in );
    void saveToFile( std::ofstream& out ) const;

    const QString& name() const;
    void setName( QString name );

    QColor color() const;
    void setColor( QColor color );

    const QVector<int>& pixels() const;
    void setPixels( QVector<int> pixels );
    void addPixels( const QVector<int>& pixels );
    void removePixels( const QVector<int>& pixels );

signals:
    void nameChanged();
    void colorChanged();
    void pixelsChanged();

private:
    QString         _name {};
    QColor          _color {};
    QVector<int>    _pixels {};
};

#endif // GROUP_H
