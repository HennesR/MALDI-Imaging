#include "projectionspace.h"

#include "hierachicalSNE/hierachicalSNE.hpp"
#include "CyteGuideTreeNode/CyteGuideTreeNode.hpp"
#include "CyteGuideTreeNode/tSNE/tSNE.h"
#include "shared.h"

#include <iostream>
#include <cmath>
#include <limits.h>
#include <string>
#include <limits>
#include <array>
#include <vector>
#include <cstdio>
#include <ctime>
#include <fstream>

#ifdef __APPLE__
#define BASE_DIR "/Users/florian/Desktop/GitLab/MALDI-Imaging/"
#else
#define BASE_DIR "../"
#endif

#define PI 3.14159265

const unsigned int AMOUNT_OF_BELONGING_POINTS_MODE = 0;
const unsigned int STD_DEV_OF_MZ_SPACE_MODE = 1;
const unsigned int STD_DEV_OF_SPATIAL_SPACE_MODE = 2;

bool checkFileExistent(std::string fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}

void ProjectionSpace::initialize()
{
    this->displayMode = AMOUNT_OF_BELONGING_POINTS_MODE;
    this->pointRadius = 1.0;
    this->lineWidth = 1.0;

    this->setAcceptHoverEvents(true);
    this->setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton);

    this->lasso = this->findChild<LassoTool*>("projectionSpace_lasso");
}

ProjectionSpace::~ProjectionSpace()
{
    delete this->hsneResults;
}

CyteGuideTreeNode* getSelectedCyteGuideNode(CyteGuideTreeNode* cyteGuideTreeRoot, std::vector<unsigned int> selectedCyteGuidePath)
{
    CyteGuideTreeNode* currentNode = cyteGuideTreeRoot;

    for(std::vector<unsigned int>::size_type i=0; i != selectedCyteGuidePath.size(); ++i)
    {
        unsigned int nextIndex = selectedCyteGuidePath.at(i);
        if(currentNode->children.size() > nextIndex)
        {
            currentNode = &(currentNode->children.at(nextIndex));
        }
        else
        {
            std::cout << "get selected cyte guide node went wrong!"<< std::endl;
            return currentNode;
        }
    }

    return currentNode;
}

std::pair<double, double> rotatePoint(double pointX, double pointY, double originX, double originY, double angle)
{
    std::pair<double, double> result;

    result.first = cos(angle) * (pointX - originX) - sin(angle) * (pointY - originY) + originX;
    result.second = sin(angle) * (pointX - originX) + cos(angle) * (pointY - originY) + originY;

    return result;
}

void ProjectionSpace::setDataSet(DataSet* dataSet)
{
    this->dataSet = dataSet;

    this->hoveredClusterIndex = -1;
    this->zoom = 1.0;

    const unsigned int pointsSize = dataSet->spectrumsSize;
    const unsigned int pointsDimension = dataSet->mzValuesSize;

    Point* dataPoints = new Point[pointsSize];
    initializePointsArray(dataPoints, pointsDimension, pointsSize);

    for(unsigned int i=0; i<pointsSize; i++)
    {
        for(unsigned int j=0; j<pointsDimension; j++)
        {
            double value = (double)(dataSet->mzIntensities[i * pointsDimension + j]);
            dataPoints[i].coordinates[j] = value;
        }
    }

    unsigned int maximumScale = 3;
    unsigned int numberOfneighbours = 15;

    this->hsneResults = new HSNEResults(maximumScale);

    hierachicalSNE(this->hsneResults, std::string(BASE_DIR) + std::string("saves/"), dataPoints, maximumScale, pointsSize, pointsDimension, numberOfneighbours, 1.5);

    if(checkFileExistent(std::string(BASE_DIR) + std::string("saves/cyte guide/cyteguidenode_0_0.txt")))
    {
        this->cyteGuideTreeRoot.loadFromFile(std::string(BASE_DIR) + std::string("saves/cyte guide/"), 0, 0, dataPoints, pointsDimension, this->dataSet->width());
    }
    else
    {
        this->cyteGuideTreeRoot = CyteGuideTreeNode(maximumScale - 1, maximumScale - 1, dataPoints, pointsSize, pointsDimension, this->hsneResults, 0, 0, this->dataSet->width());
        this->cyteGuideTreeRoot.saveToFile(std::string(BASE_DIR) + std::string("saves/cyte guide/"));
    }

    delete[] dataPoints;
}

void ProjectionSpace::activateAmountOfBelongingPointsDisplayMode()
{
    this->displayMode = AMOUNT_OF_BELONGING_POINTS_MODE;
}

void ProjectionSpace::activateStdDevOfMzSpaceDisplayMode()
{
    this->displayMode = STD_DEV_OF_MZ_SPACE_MODE;
}

void ProjectionSpace::activateStdDevOfSpatialSpaceDisplayMode()
{
    this->displayMode = STD_DEV_OF_SPATIAL_SPACE_MODE;
}

void ProjectionSpace::setLineWidth(double width)
{
    this->lineWidth = width;
}

void ProjectionSpace::setPointRadius(double radius)
{
    this->pointRadius = radius;
}

void ProjectionSpace::paintCyteGuideNodeEmbedding(QPainter* p, CyteGuideTreeNode* cyteGuideNode, qreal x, qreal y, qreal size, qreal centerX, qreal centerY, double angle, unsigned int depth)
{
    // padding
    size = size - (size / 10.0);

    for(unsigned int i=0; i<cyteGuideNode->embeddedPoints.size(); i++)
    {
        double realX = cyteGuideNode->embeddedPoints[i].first * size + x - (size / 2.0);
        double realY = cyteGuideNode->embeddedPoints[i].second * size + y - (size / 2.0);

        std::pair<double, double> rotatedPoint = rotatePoint(realX, realY, centerX, centerY, -angle + (PI/2.0));

        qreal pointsRadius = this->pointRadius;
        double ratio = 0.0;

        if(this->displayMode == AMOUNT_OF_BELONGING_POINTS_MODE)
        {
            double min = cyteGuideNode->amountOfContainingPointsMin;
            double max = cyteGuideNode->amountOfContainingPointsMax;

            ratio = (cyteGuideNode->children.size() == 0 || min == max) ? 0.0 :
                (cyteGuideNode->amountOfContainingPoints.at(i) -  min) / (max - min);
        }

        if(this->displayMode == STD_DEV_OF_MZ_SPACE_MODE)
        {
            double min = cyteGuideNode->mzStandardDeviationsMin;
            double max = cyteGuideNode->mzStandardDeviationsMax;

            ratio = (cyteGuideNode->children.size() == 0 || min == max) ? 0.0 :
                (cyteGuideNode->mzStandardDeviations.at(i) -  min) / (max - min);
        }

        if(this->displayMode == STD_DEV_OF_SPATIAL_SPACE_MODE)
        {
            double min = cyteGuideNode->spatialStandardDeviationsMin;
            double max = cyteGuideNode->spatialStandardDeviationsMax;

            ratio = (cyteGuideNode->children.size() == 0 || min == max) ? 0.0 :
                (cyteGuideNode->spatialStandardDeviations.at(i) -  min) / (max - min);
        }

        pointsRadius += ratio * (this->pointRadius * 2.5);

        if(depth == 0 && this->lasso->polygon().isEmpty())
        {
            if(cyteGuideNode->clusterMap.at(i) == this->hoveredClusterIndex)
            {
                pointsRadius *= 1.5;
            }
        }

        /*
        // colors from http://colorbrewer2.org
        static const std::array<QColor, 8> COLORS
        {
            QColor::fromRgb(0xe4, 0x1a, 0x1c),
            QColor::fromRgb(0x37, 0x7e, 0xb8),
            QColor::fromRgb(0x4d, 0xaf, 0x4a),
            QColor::fromRgb(0x98, 0x4d, 0xa3),

            QColor::fromRgb(0xff, 0x7f, 0x00),
            QColor::fromRgb(0xff, 0xff, 0x33),
            QColor::fromRgb(0xa6, 0x56, 0x28),
            QColor::fromRgb(0xf7, 0x81, 0xbf)
        };
        */

        p->setPen(Qt::NoPen); // no stroke
        QColor color = (cyteGuideNode->children.size() == 0) ?
                QColor::fromHsvF((1.0 / (cyteGuideNode->numberOfClusters * 2.0)) * (cyteGuideNode->clusterMap[i] * 2 + cyteGuideNode->depth % 2), 1.0, 0.8, 1.0) :
                QColor::fromHsvF((1.0 / (cyteGuideNode->numberOfClusters * 2.0)) * (cyteGuideNode->clusterMap[i] * 2 + cyteGuideNode->depth % 2), 1.0, 0.8, 1.0 - cyteGuideNode->amountOfContainingPoints.at(i) / 100.0);

        p->setBrush(color);
        p->drawEllipse(QPointF(rotatedPoint.first, rotatedPoint.second), pointsRadius, pointsRadius);
    }
}

void ProjectionSpace::paintCyteGuideNode(QPainter* p, CyteGuideTreeNode* cyteGuideNode, double centerX, double centerY, unsigned int depth, qreal radius, double startAngle, double endAngle)
{
    if(cyteGuideNode->embeddedPoints.size() > 0)
    {
        unsigned int numberOfChildren = cyteGuideNode->children.size();

        double r1 = radius * depth;
        double r2 = radius * (depth + 1);

        double squareSizeA = (2.0/5.0) * (-2 * r1 + sqrt(5 * pow(r2, 2) - pow(r1, 2))); // TODO: Doku - Beweis
        double squareSizeB = sqrt(pow((cos(startAngle) - cos(endAngle)) * r1, 2) + pow((sin(startAngle) - sin(endAngle)) * r1, 2));
        double squareSize = std::min(squareSizeA, squareSizeB);

        double squareCenterX = centerX;
        double squareCenterY = centerY - r1 - squareSize / 2.0;

        QRectF outerCircleRect(centerX - r2, centerY - r2, 2 * r2, 2 * r2);
        QRectF embeddingRect(squareCenterX - squareSize / 2.0, squareCenterY - squareSize / 2.0, squareSize, squareSize);

        paintCyteGuideNodeEmbedding(p, cyteGuideNode, squareCenterX, squareCenterY, squareSize, centerX, centerY, (startAngle + endAngle) / 2.0, depth);

        p->setPen(QPen(QBrush(QColor::fromHsvF(0.0, 0.0, 0.8, 1.0)), this->lineWidth));
        p->drawLine(centerX + cos(-startAngle) * r1, centerY + sin(-startAngle) * r1, centerX + cos(-startAngle) * r2, centerY + sin(-startAngle) * r2);
        p->drawLine(centerX + cos(-endAngle) * r1, centerY + sin(-endAngle) * r1, centerX + cos(-endAngle) * r2, centerY + sin(-endAngle) * r2);

        if(numberOfChildren == 0)
        {
            p->setPen(QPen(QBrush(QColor::fromHsvF(0.0, 0.0, 0.8, 1.0)), this->lineWidth));
            p->drawArc(outerCircleRect, startAngle / (2*PI) * 360 * 16, (endAngle - startAngle) / (2*PI) * 360 * 16);
        }
        else
        {
            for(unsigned int i=0; i<numberOfChildren; i++)
            {
                CyteGuideTreeNode* child = &(cyteGuideNode->children.at(i));

                double nextStartAngle = startAngle + i * ((endAngle - startAngle) / numberOfChildren);
                double nextEndAngle = nextStartAngle + ((endAngle - startAngle) / numberOfChildren);

                QColor color = QColor::fromHsvF((1.0 / (numberOfChildren * 2.0)) * (i * 2 + cyteGuideNode->depth % 2), 1.0, 0.8, 1.0);
                p->setPen(QPen(QBrush(color), this->lineWidth));
                p->drawArc(outerCircleRect, nextStartAngle / (2*PI) * 360 * 16, (nextEndAngle - nextStartAngle) / (2*PI) * 360 * 16);
                paintCyteGuideNode(p, child, centerX, centerY, depth + 1, radius, nextStartAngle, nextEndAngle);
            }
        }
    }
}

void paintMiniMap(QPainter* p, CyteGuideTreeNode* cyteGuideNode, std::vector<unsigned int> selectedCyteGuidePath, qreal size)
{
    double centerX = size / 2.0;
    double centerY = size / 2.0;

    unsigned int maximumScale = 3;
    qreal radius = size / (maximumScale * 2.5);

    double startAngle = 0.0;
    double endAngle = 2 * PI;

    p->setPen(QPen(QBrush(Qt::black), 1.0));

    const QPointF center(centerX, centerY);
    p->drawEllipse(center, radius, radius);

    unsigned int counter = 0;

    for(std::vector<unsigned int>::iterator it = selectedCyteGuidePath.begin(); it != selectedCyteGuidePath.end(); ++it)
    {
        unsigned int nextIndex = *it;
        double lastStartAngle = startAngle;
        double lastAngleSpan = endAngle - startAngle;

        startAngle = lastStartAngle + nextIndex * (lastAngleSpan / cyteGuideNode->children.size());
        endAngle = lastStartAngle + (nextIndex + 1) * (lastAngleSpan / cyteGuideNode->children.size());
        double angleSpan = endAngle - startAngle;

        double outerRectSize = radius * 2 * (counter + 2.0);
        double outerRectX = centerX - outerRectSize / 2.0;
        double outerRectY = centerY - outerRectSize / 2.0;
        QRectF outerRect(outerRectX, outerRectY, outerRectSize, outerRectSize);

        p->drawArc(outerRect, startAngle / (2*PI) * 360 * 16, angleSpan / (2*PI) * 360 * 16);

        double r1 = radius * (counter + 1.0);
        double r2 = radius * (counter + 2.0);

        p->drawLine(centerX + cos(-startAngle) * r1, centerY + sin(-startAngle) * r1, centerX + cos(-startAngle) * r2, centerY + sin(-startAngle) * r2);
        p->drawLine(centerX + cos(-endAngle) * r1, centerY + sin(-endAngle) * r1, centerX + cos(-endAngle) * r2, centerY + sin(-endAngle) * r2);

        cyteGuideNode = &(cyteGuideNode->children.at(nextIndex));

        counter++;
    }
}

void ProjectionSpace::paint(QPainter* p)
{
    this->lasso->paint(p);

    qreal width = size().width();
    qreal height = size().height();

    unsigned int maximumScale = 3;

    double size = (width < height) ? width : height;
    double radius = size / (maximumScale * 2.5);

    paintMiniMap(p, &(this->cyteGuideTreeRoot), this->selectedCyteGuidePath, size / 8.0);

    p->translate(width / 2.0, height / 2.0);
    p->scale(this->zoom, this->zoom);
    p->translate(-width / 2.0, -height / 2.0);

    double centerX = width / 2.0;
    double centerY = height / 2.0;

    double outerRectSize = radius * 2;
    double outerRectX = centerX - outerRectSize / 2;
    double outerRectY = centerY - outerRectSize / 2;

    double innerRectSize = cos(PI / 4.0) * radius * 2;
    double innerRectX = centerX - innerRectSize / 2.0;
    double innerRectY = centerY - innerRectSize / 2.0;

    QRectF outerRect(outerRectX, outerRectY, outerRectSize, outerRectSize);
    QRectF innerRect(innerRectX, innerRectY, innerRectSize, innerRectSize);

    CyteGuideTreeNode* selectedCyteGuideNode = getSelectedCyteGuideNode(&(this->cyteGuideTreeRoot), selectedCyteGuidePath);

    paintCyteGuideNodeEmbedding(p, selectedCyteGuideNode, centerX, centerY, innerRectSize, centerX, centerY, 0, 0);

    if(!selectedCyteGuideNode->children.empty())
    {
        for(int i=0; i<selectedCyteGuideNode->children.size(); i++)
        {
            CyteGuideTreeNode* child = &(selectedCyteGuideNode->children.at(i));

            double nextStartAngle = i * (2*PI / selectedCyteGuideNode->children.size());
            double nextEndAngle = (i+1) * (2*PI / selectedCyteGuideNode->children.size());

            QColor color = QColor::fromHsvF((1.0 / (selectedCyteGuideNode->children.size() * 2.0)) * (i * 2 + selectedCyteGuideNode->depth % 2), 1.0, 0.8, 1.0);
            p->setPen(QPen(QBrush(color), 1.5));
            p->drawArc(outerRect, nextStartAngle / (2*PI) * 360 * 16, (nextEndAngle - nextStartAngle) / (2*PI) * 360 * 16);

            paintCyteGuideNode(p, child, centerX, centerY, 1, radius, ((double)(i)/selectedCyteGuideNode->children.size()) * 2 * PI, ((i+1.0)/selectedCyteGuideNode->children.size()) * 2 * PI);
        }
    }
}

void ProjectionSpace::hoverMoveEvent(QHoverEvent* event)
{
    this->lasso->hoverMoveEvent(event);

    qreal width = size().width();
    qreal height = size().height();

    unsigned int BRACHING_FACTOR = 4;
    unsigned int maximumScale = 3;

    double size = (width < height) ? width : height;
    double radius = size / (maximumScale * 2.5);

    double centerX = width / 2.0;
    double centerY = height / 2.0;

    double outerRectX = centerX - radius;
    double outerRectY = centerY - radius;
    double outerRectSize = radius * 2;

    double innerRectSize = cos(PI / 4) * radius * 2;
    double innerRectX = centerX - innerRectSize / 2;
    double innerRectY = centerY - innerRectSize / 2;

    innerRectSize = innerRectSize - (innerRectSize / 10.0);

    QPointF mousePosition = event->posF();
    double mousePositionX = centerX + (mousePosition.x() - centerX) * (1.0 / this->zoom);
    double mousePositionY = centerY + (mousePosition.y() - centerY) * (1.0 / this->zoom);

    unsigned int minIndex = 0;
    double minDistance = std::numeric_limits<double>::max();

    CyteGuideTreeNode* selectedCyteGuideNode = getSelectedCyteGuideNode(&(this->cyteGuideTreeRoot), selectedCyteGuidePath);

    for(unsigned int i=0; i<selectedCyteGuideNode->embeddedPoints.size(); i++)
    {
        double realX = selectedCyteGuideNode->embeddedPoints[i].first * innerRectSize + centerX - (innerRectSize / 2.0);
        double realY = selectedCyteGuideNode->embeddedPoints[i].second * innerRectSize + centerY - (innerRectSize / 2.0);

        std::pair<double, double> rotatedPoint = rotatePoint(realX, realY, centerX, centerY, PI / 2.0);

        double distance = sqrt(pow(abs(mousePositionX - rotatedPoint.first), 2) + pow(abs(mousePositionY - rotatedPoint.second), 2));

        if(distance < minDistance)
        {
            minDistance = distance;
            minIndex = i;
        }
    }

    int lastClusteredIndex = this->hoveredClusterIndex;

    this->hoveredClusterIndex = selectedCyteGuideNode->clusterMap.at(minIndex);

    if(minDistance < 5.0 && !this->lasso->finished())
    {
        if(lastClusteredIndex != this->hoveredClusterIndex)
        {
            if(selectedCyteGuideNode->children.size() > 0)
            {
                std::vector<std::pair<unsigned int, double>> areaOfInfluence = selectedCyteGuideNode->areasOfInfluence.at(this->hoveredClusterIndex);
                QVector<std::pair<unsigned int, double>> qvec = QVector<std::pair<unsigned int, double>>::fromStdVector(areaOfInfluence);
                QColor color = QColor::fromHsvF((1.0 / (BRACHING_FACTOR * 2.0)) * (this->hoveredClusterIndex * 2 + selectedCyteGuideNode->depth % 2), 1.0, 0.8, 1.0);

                emit hoveredLandmarksCluster(qvec, color);
            }
            else
            {
                std::vector<std::pair<unsigned int, double>> areaOfInfluence;

                for(unsigned int i=0; i<selectedCyteGuideNode->globalLandmarkIndices.size(); i++)
                {
                    if(selectedCyteGuideNode->clusterMap.at(i) == this->hoveredClusterIndex)
                    {
                        areaOfInfluence.push_back(std::make_pair(selectedCyteGuideNode->globalLandmarkIndices.at(i), 1.0));
                    }
                }

                QVector<std::pair<unsigned int, double>> qvec = QVector<std::pair<unsigned int, double>>::fromStdVector(areaOfInfluence);
                QColor color = QColor::fromHsvF((1.0 / (BRACHING_FACTOR * 2.0)) * (this->hoveredClusterIndex * 2 + selectedCyteGuideNode->depth % 2), 1.0, 0.8, 1.0);

                emit hoveredLandmarksCluster(qvec, color);
            }
        }
    }
    else
    {
        if(lastClusteredIndex != this->hoveredClusterIndex)
        {
            QVector<std::pair<unsigned int, double>> qvec;
            emit hoveredLandmarksCluster(qvec, Qt::white);
        }

        this->hoveredClusterIndex = -1;
    }

    this->update();
}

void ProjectionSpace::mousePressEventForMiniMap(qreal x, qreal y, qreal size)
{
    double centerX = size / 2.0;
    double centerY = size / 2.0;

    double diffX = centerX - x;
    double diffY = centerY - y;

    unsigned int maximumScale = 3;
    double radius = size / (maximumScale * 2.5);

    double distance = sqrt(pow(diffX, 2) + pow(diffY, 2));

    if(distance < radius)
    {
        this->selectedCyteGuidePath = std::vector<unsigned int>();
        this->zoom = 1.0;
    }
}

void ProjectionSpace::mousePressEvent(QMouseEvent* event)
{
    qreal width = size().width();
    qreal height = size().height();

    unsigned int maximumScale = 3;

    double size = (width < height) ? width : height;
    double radius = size / (maximumScale * 2.5);

    double centerX = width / 2.0;
    double centerY = height / 2.0;

    QPointF mousePosition = event->pos();
    double mousePositionX = centerX + (mousePosition.x() - centerX) * (1.0 / this->zoom);
    double mousePositionY = centerY + (mousePosition.y() - centerY) * (1.0 / this->zoom);

    QPoint diff(mousePositionX - centerX, mousePositionY - centerY);

    double distance = sqrt(pow(diff.x(), 2) + pow(diff.y(), 2));
    double alpha = (diff.y() < 0.0) ? acos(diff.x() / distance) : PI + acos(-diff.x() / distance);

    CyteGuideTreeNode* currentNode = getSelectedCyteGuideNode(&(this->cyteGuideTreeRoot), this->selectedCyteGuidePath);

    if(currentNode->children.size() > 0 && distance > radius && distance < radius * 2)
    {
        unsigned int nextIndex = (unsigned int)(alpha / (2*PI / currentNode->children.size()));
        if(currentNode->children.at(nextIndex).embeddedPoints.size() > 0)
        {
            this->selectedCyteGuidePath.push_back(nextIndex);
            this->lasso->clear();
        }
    }

    if(distance < radius)
    {
        this->lasso->mousePressEvent(event);

        if(this->lasso->finished())
        {
            QRect boundingRect = this->lasso->polygon().boundingRect();

            if(boundingRect.width() == 0 || boundingRect.height() == 0)
            {
                this->lasso->clear();
            }
        }

        if(this->lasso->finished())
        {
            QPolygon polygon = this->lasso->polygon();
            std::vector<unsigned int> selectedIndicesStack;

            for(unsigned int i=0; i<currentNode->embeddedPoints.size(); i++)
            {
                double innerRectSize = cos(PI / 4) * radius * 2;

                double realX = currentNode->embeddedPoints[i].first * innerRectSize + centerX - (innerRectSize / 2.0);
                double realY = currentNode->embeddedPoints[i].second * innerRectSize + centerY - (innerRectSize / 2.0);

                realX = centerX + (realX - centerX) * this->zoom;
                realY = centerY + (realY - centerY) * this->zoom;

                std::pair<double, double> rotatedPoint = rotatePoint(realX, realY, centerX, centerY, PI/2.0);

                if(polygon.containsPoint(QPoint(rotatedPoint.first, rotatedPoint.second), Qt::OddEvenFill))
                {
                    selectedIndicesStack.push_back(i);
                }
            }

            if(selectedIndicesStack.size() > 0)
            {
                /*

                struct vec3
                {
                    float x, y, z;
                };

                struct point
                {
                    vec3 position;
                    int pixelIndex;
                    float opacity;
                };

                struct pointCluster
                {
                    QColor color;
                    QVector<point> points;
                };

                */

                const double TSNE_TRESHHOLD = 0.15;

                const unsigned int pointsSize = this->dataSet->spectrumsSize;
                const unsigned int pointsDimension = this->dataSet->mzValuesSize;

                Point* dataPoints = new Point[pointsSize];
                initializePointsArray(dataPoints, pointsDimension, pointsSize);

                for(unsigned int i=0; i<pointsSize; i++)
                {
                    for(unsigned int j=0; j<pointsDimension; j++)
                    {
                        double value = (double)(this->dataSet->mzIntensities[i * pointsDimension + j]);
                        dataPoints[i].coordinates[j] = value;
                    }
                }

                std::vector<std::tuple<unsigned int, double, double, double>> embeddedPoints = this->cyteGuideTreeRoot.calculateTSNEEmbeddingByLandmarkIndices((maximumScale - 1) - this->selectedCyteGuidePath.size(), selectedIndicesStack, hsneResults, dataPoints, pointsDimension, TSNE_TRESHHOLD);

                pointCluster pc;

                for(unsigned int i=0; i<embeddedPoints.size(); i++)
                {
                    point p;

                    p.pixelIndex = std::get<0>(embeddedPoints.at(i));
                    p.opacity = std::get<1>(embeddedPoints.at(i));
                    p.position.x = std::get<2>(embeddedPoints.at(i));
                    p.position.y = std::get<3>(embeddedPoints.at(i));
                    p.position.z = 0.0;

                    pc.points.push_back(p);
                }

                pc.color = Qt::red;
                QVector<pointCluster> pcv;

                pcv.push_back(pc);

                emit selectScatterSpaceTSNEPoints(pcv);
            }
        }
    }

    this->mousePressEventForMiniMap(mousePosition.x(), mousePosition.y(), size / 8.0);

    this->update();
}

void ProjectionSpace::wheelEvent(QWheelEvent* event)
{
    const double SCALE_FACTOR = 1.15;

    this->lasso->clear();

    this->zoom *= (event->delta() > 0) ? SCALE_FACTOR : 1.0 / SCALE_FACTOR;

    this->update();
}

