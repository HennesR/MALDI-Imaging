#include "parallelcoordinatesspace.h"
#include "./external/qcustomplot.h"
#include <algorithm>

ParallelCoordinatesSpace::~ParallelCoordinatesSpace()
{
    delete _customPlot;
}
void ParallelCoordinatesSpace::setDataSet( const DataSet* pDataSet )
{
    _dataSet = pDataSet;

    QObject::connect( pDataSet, &DataSet::dataChanged, [this]{
        _customPlot->clearPlottables();
        _sections.clear();
        this->setDimensions( {} );
    } );

    QObject::connect( pDataSet, &DataSet::groupsCleared, [this]{
        this->setOutputGroup( nullptr );
    });

}
void ParallelCoordinatesSpace::setDimensions( QVector<int> dimensions )
{
    _dimensions = std::move( dimensions );

    // prepare x axis
    QSharedPointer<QCPAxisTickerText> textTicker(new QCPAxisTickerText);
    for( int i = 0; i < _dimensions.size(); ++i )
        textTicker->addTick( i + 1, QString::number( _dataSet->mzValue( _dimensions[i] ) ) );
    _customPlot->xAxis->setTicker( textTicker );
    _customPlot->xAxis->setTickLabelRotation( 60 );
    _customPlot->xAxis->setRange( 0, _dimensions.size() + 1 );

    this->sectionsUpdated();
}

void ParallelCoordinatesSpace::setOutputGroup( Group* pGroup )
{
    _outputGroup = pGroup;
    for( auto plottable : _customPlot->selectedPlottables() )
        plottable->setSelection( QCPDataSelection() );
    this->update();
}

void ParallelCoordinatesSpace::sectionsUpdated()
{
    // Sort intensities
    QVector<QVector<float>> intensities( _dimensions.size(), QVector<float>( _dataSet->pixelCount() ) );
    for( int i = 0; i < _dimensions.size(); ++i ) {
        for( int j = 0; j < _dataSet->pixelCount(); ++j ) {
            intensities[i][j] = _dataSet->intensity( j, _dimensions[i] );
        }
        std::sort( intensities[i].begin(), intensities[i].end() );
    }

    _customPlot->clearPlottables();
    for( int d = 0; d < _dimensions.size(); ++d )
    {
        const float maximum = intensities[d].back();

        for( int s = 0; s < _sections.size(); ++s )
        {
            // Create new bar chart
            QCPBars* barChart = new QCPBars( _customPlot->xAxis, _customPlot->yAxis );
            barChart->addData( { static_cast<double>( d + 1 ) }, { static_cast<double>( _sections[s].end - _sections[s].begin ) }, true );
            barChart->setBaseValue( static_cast<double>( _sections[s].begin ) );

            // Find lower- and upper bound
            const auto lower_bound = std::lower_bound( intensities[d].begin(), intensities[d].end(), _sections[s].begin * maximum );
            const auto upper_bound = std::upper_bound( intensities[d].begin(), intensities[d].end(), _sections[s].end * maximum );
            const float pixelDensity = (upper_bound - lower_bound) / static_cast<float>( _dataSet->pixelCount() );

            // Color code from blue to orange base on pixel density
            const int r = 0x3f + pixelDensity * ( 0xec - 0x3f );
            const int g = 0x48 + pixelDensity * ( 0x1c - 0x1c );
            const int b = 0xcc + pixelDensity * ( 0x24 - 0xcc );

            QColor color( r, g, b );
            barChart->setBrush( color );
            barChart->setPen( QColor( Qt::transparent ) );
            barChart->selectionDecorator()->setBrush( color );
            barChart->selectionDecorator()->setPen( QPen( QColor( 255, 240, 0 ), 3 ) );
            barChart->setWidth( 0.5 );
        }
    }

    this->update();
}
void ParallelCoordinatesSpace::selectionUpdated()
{
    // Find maximum intensity for every dimension
    QVector<float> maxIntensities( _dimensions.size(), 0.0f );
    for( int i = 0; i < _dimensions.size(); ++i )
        for( int j = 0; j < _dataSet->pixelCount(); ++j )
            if( _dataSet->intensity( j, _dimensions[i] ) > maxIntensities[i] ) maxIntensities[i] = _dataSet->intensity( j, _dimensions[i] );


    // Find selected sections per dimension
    QVector<QPair<int, QVector<QPair<float, float>>>> dimensions;
    for( int i = 0; i < _dimensions.size(); ++i )
    {
        QPair<int, QVector<QPair<float, float>>> dimension;
        dimension.first = _dimensions[i];

        for( int j = 0; j < _sections.size(); ++j )
        {
            const auto plottable = _customPlot->plottable( i * _sections.size() + j );
            if( !_customPlot->selectedPlottables().contains( plottable ) ) continue;

            QPair<float, float> range;
            range.first = _sections[j].begin * maxIntensities[i];
            range.second = _sections[j].end * maxIntensities[i];
            dimension.second.push_back( range );
        }

        if( dimension.second.size() )
            dimensions.push_back( std::move( dimension ) );
    }

    // Find selected pixels and those to visualize
    QSet<int> selectedPixels, visualizedPixels;
    QVector<QSet<int>> dimensionPixels( dimensions.size() );
    for( int i = 0; i < dimensions.size(); ++i )
    {
        const auto& dimension = dimensions[i];

        QSet<int> dimensionPixels;
        for( int j = 0; j < _dataSet->pixelCount(); ++j )
        {
            for( const auto& range : dimension.second ) {
                const float intensity = _dataSet->intensity( j, dimension.first );
                if( intensity >= range.first && intensity <= range.second ) {
                    dimensionPixels << j;
                    break;
                }
            }
        }

        if( _outputGroup ) selectedPixels.unite( dimensionPixels );

        if( !visualizedPixels.size() ) visualizedPixels = std::move( dimensionPixels );
        else visualizedPixels.intersect( dimensionPixels );
    }

    if( _outputGroup ) _outputGroup->setPixels( visualizedPixels.toList().toVector() );

    // Create min/max-band
    _customPlot->clearGraphs();
    QCPGraph* upper = _customPlot->addGraph( _customPlot->xAxis, _customPlot->yAxis );
    upper->setSelectable( QCP::SelectionType::stNone );
    upper->setBrush( QColor( 255, 240, 0, 100 ) );
    upper->setPen( QColor( 255, 240, 0, 100 ) );
    QCPGraph* lower = _customPlot->addGraph( _customPlot->xAxis, _customPlot->yAxis );
    lower->setSelectable( QCP::SelectionType::stNone );
    lower->setPen( QColor( 255, 240, 0, 100 ) );
    upper->setChannelFillGraph( lower );

    QVector<double> keys( _dimensions.size() ), upperValues( _dimensions.size() ), lowerValues( _dimensions.size() );
    for( int i = 0; i < _dimensions.size(); ++i ) {
        keys[i] = i + 1.0;

        float maxIntensity = 0.0f, minIntensity = std::numeric_limits<float>::max();
        for( auto pixel : visualizedPixels ) {
            const float intensity = _dataSet->intensity( pixel, _dimensions[i] );
            if( intensity > maxIntensity ) maxIntensity = intensity;
            if( intensity < minIntensity ) minIntensity = intensity;
        }

        upperValues[i] = static_cast<double>( maxIntensity / maxIntensities[i] );
        lowerValues[i] = static_cast<double>( minIntensity / maxIntensities[i] );
    }
    upper->setData( keys, upperValues, true );
    lower->setData( keys, lowerValues, true );

    this->update();
}

void ParallelCoordinatesSpace::componentComplete()
{
    QQuickPaintedItem::componentComplete();

    this->setAcceptedMouseButtons( Qt::LeftButton | Qt::RightButton );

    _customPlot = new QCustomPlot;
    _customPlot->xAxis->grid()->setPen( QPen( Qt::gray ) );
    _customPlot->yAxis->setRange( 0.0, 1.0 );
    _customPlot->yAxis->grid()->setVisible( false );

    // Color legend
    QCPColorScale* colorScale = new QCPColorScale( _customPlot );
    _customPlot->plotLayout()->addElement( 0, 1, colorScale );
    colorScale->axis()->setLabel( "pixel density" );
    colorScale->axis()->setRange( 0.0, 1.0 );
    colorScale->setBarWidth( 10 );

    QCPColorGradient colorGradient;
    colorGradient.setColorStopAt( 0.0, QColor( 65, 70, 205 ) );
    colorGradient.setColorStopAt( 1.0, QColor( 235, 30, 35 ) );
    colorScale->setGradient( colorGradient );

    QCPMarginGroup* marginGroup = new QCPMarginGroup( _customPlot );
    _customPlot->axisRect()->setMarginGroup( QCP::msTop | QCP::msBottom, marginGroup );
    colorScale->setMarginGroup( QCP::msTop | QCP::msBottom, marginGroup );
}
void ParallelCoordinatesSpace::paint( QPainter* painter )
{
    painter->drawPixmap( QPoint(), _customPlot->toPixmap( static_cast<int>( this->width() ), static_cast<int>( this->height() ) ) );

    if( _selectBegin.x() != -1 ) {
        painter->setBrush( QColor( 255, 240, 0 ) );
        painter->setPen( QColor( 255, 240, 0 ) );

        painter->drawEllipse( _selectBegin, 3, 3 );
        painter->drawLine( _selectBegin, _selectEnd );
        painter->drawEllipse( _selectEnd, 3, 3 );
    }
}

void ParallelCoordinatesSpace::mousePressEvent( QMouseEvent* event )
{
    if( QCPAbstractPlottable* plottable = _customPlot->plottableAt( event->pos(), true ) ) {
        static_cast<QCPBars*>( plottable )->setSelection( event->button() == Qt::LeftButton? QCPDataSelection( QCPDataRange( 0, _dimensions.size() + 2 ) ) : QCPDataSelection() );
        this->selectionUpdated();
    }
    else {
        _selectBegin = _selectEnd = event->pos();
    }

    this->update();
}
void ParallelCoordinatesSpace::mouseMoveEvent( QMouseEvent* event )
{
    if( _selectBegin.x() != -1 ) {
        _selectEnd.setY( event->pos().y() );
        this->update();
    }
}
void ParallelCoordinatesSpace::mouseReleaseEvent( QMouseEvent* event )
{
    if( _selectBegin.x() == -1 ) return;

    float begin = _customPlot->yAxis->pixelToCoord( _selectBegin.y() );
    float end = _customPlot->yAxis->pixelToCoord( _selectEnd.y() );

    if( begin > end ) std::swap( begin, end );
    if( begin < 0.0f ) begin = 0.0f;
    if( end > 1.0f ) end = 1.0f;

    // Remove / change overlapping sections
    QVector<int> remove {};
    const int sectionCount = _sections.size();
    for( int i = 0; i < sectionCount; ++i ) {
        section& s = _sections[i];
        if( begin <= s.begin ) {
            if( end < s.begin );
            else if( end < s.end ) s.begin = end;
            else remove.push_back( i );
        } else if( begin <= s.end ) {
            if( end < s.end ) {
                _sections.push_back( section{ end, s.end } );
                _sections.push_back( section{ s.begin, begin } );
                remove.push_back( i );
            }
            else s.end = begin;
        }
    }
    for( int i = 0; i < remove.size(); ++i )
        _sections.remove( remove[i] - i );

    // Add new section
    if( event->button() == Qt::LeftButton ) {
        _sections.push_back( section{ begin, end } );
    }

    _selectBegin = QPoint( -1, -1 );
    this->sectionsUpdated();
}
