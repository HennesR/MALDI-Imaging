#include "dataset.h"
#include "utility.h"
#include "alglib/statistics.h"
#include "alglib/ap.h"

#include <algorithm>
#include <iostream>
#include <list>
#include <numeric>
#include <random>
#include <iostream>
#include <fstream>
#include <vector>


template <typename T>
std::vector<size_t> sort_indexes(const std::vector<T> &v)
{
  // initialize original index locations
  std::vector<size_t> idx(v.size());
  iota(idx.begin(), idx.end(), 0);

  // sort indexes based on comparing values in v
  sort(idx.begin(), idx.end(), [&v](size_t i1, size_t i2) {return v[i1] < v[i2];});

  return idx;
}

void DataSet::loadFromFile(const char* filepath)
{
    std::ifstream fileStream( filepath );
    std::string line;

    if( !fileStream.is_open() )
    {
        std::cout << "Can't open file '" << filepath << "'" << std::endl;
        std::exit( EXIT_FAILURE );
    }

    // ignore first two lines
    std::getline( fileStream, line );
    std::getline( fileStream, line );

    // just get mz-values from third line
    if( getline( fileStream, line ) )
    {
        std::vector<std::string> token = tokenizeString( line, "\t" );

        mzValuesSize = std::stoi( token.back() );
        mzValues = new float[mzValuesSize];
    }

    // get mz-values from fourth line
    if( std::getline( fileStream, line ) )
    {
        std::vector<std::string> token = tokenizeString( line, "\t" );

        for( std::vector<std::string>::size_type i = 0; i < token.size(); ++i )
        {
            if( i >= 3 )
            {
                mzValues[i - 3] = std::stof( token[i] );
            }
        }
    }

    // get spectrums/intensities for the mz-values
    std::vector<float*> spectrums;
    std::vector<std::string> calibrationYCoordinatesAsText;

    int counter = 0;
    while( std::getline( fileStream, line ) )
    {
        std::vector<std::string> tokens = tokenizeString( line, "\t" );

        spectrums.push_back( new float[mzValuesSize] );

        for( std::vector<std::string>::size_type i = 0; i != mzValuesSize + 3 && tokens.size() > 1; i++ )
        {
            if( i == 2 )
            {
                calibrationYCoordinatesAsText.push_back(tokens[i]);
            }

            if( i >= 3 )
            {
                spectrums.back()[i - 3] = std::stof( tokens[i] );
            }
        }
        counter ++;
    }

    fileStream.close();

    spectrumsSize = (unsigned int) ( spectrums.size() );
    mzIntensities = new float[mzValuesSize * spectrumsSize];

    for( std::vector<float*>::size_type i = 0; i != spectrums.size(); i++ )
    {
        for( unsigned int j=0; j < mzValuesSize; j++ )
        {
            mzIntensities[i * mzValuesSize + j] = spectrums.at( i )[j];
        }

        delete[] spectrums.at( i );
    }

    imageWidth = 0;
    imageHeight = 0;

    unsigned int widthCounter = 0;
    std::string lastYCoordinateAsText;

    for( std::vector<std::string>::size_type i = 0; i != calibrationYCoordinatesAsText.size(); i++ )
    {
       if( i == 0 )
       {
           lastYCoordinateAsText = calibrationYCoordinatesAsText[i];
       }
       else
       {
           if( lastYCoordinateAsText.compare(calibrationYCoordinatesAsText[i]) != 0 )
           {
                imageWidth = widthCounter;
                imageHeight = spectrumsSize / imageWidth;
                break;
           }
       }

       if( i == spectrums.size() - 1 )
       {
           std::cout << "Unable to guess width of Image by Y-coordinates" << std::endl;
           std::exit( EXIT_FAILURE );
       }

       widthCounter++;
    }

    this->sortByMzValues();
    emit dataChanged();
}
void DataSet::sortByMzValues()
{
    std::vector<float> mzValuesVector(this->mzValues, this->mzValues + this->mzValuesSize);
    std::vector<size_t> indicesVector = sort_indexes<float>(mzValuesVector);

    float* orderedMzValues = new float[this->mzValuesSize];
    float* orderedMzIntensities = new float[this->mzValuesSize * this->spectrumsSize];

    for( std::vector<std::string>::size_type i = 0; i != indicesVector.size(); i++ )
    {
        size_t nextIndex = indicesVector[i];
        orderedMzValues[i] = this->mzValues[nextIndex];

        for(unsigned int j=0; j<this->spectrumsSize; j++)
        {
            orderedMzIntensities[i + j*this->mzValuesSize] = this->mzIntensities[nextIndex + j*this->mzValuesSize];
        }
    }

    delete[] this->mzValues;
    delete[] this->mzIntensities;

    this->mzValues = orderedMzValues;
    this->mzIntensities = orderedMzIntensities;
}
void DataSet::freeResources()
{
    delete[] mzValues;
    delete[] mzIntensities;

    if( _groups.size() ) this->clearGroups();
}



int DataSet::width() const
{
    return this->imageWidth;
}
int DataSet::height() const
{
    return this->imageHeight;
}
int DataSet::pixelCount() const
{
    return this->spectrumsSize;
}
int DataSet::dimensionCount() const
{
    return this->mzValuesSize;
}

float DataSet::mzValue( int dimension ) const
{
    return this->mzValues[dimension];
}
float DataSet::intensity( int pixel, int dimension ) const
{
    return this->mzIntensities[ pixel * this->dimensionCount() + dimension ];
}

void DataSet::loadGroups( QUrl fileUrl )
{
    std::ifstream in( fileUrl.toLocalFile().toLocal8Bit().constData(), std::ios::in | std::ios::binary );
    if( !in ) {
        qDebug() << "Failed to open file '" << fileUrl << "'";
        std::exit( EXIT_FAILURE );
    }

    this->clearGroups();
    do
    {
        Group* group = new Group;
        if( !group->loadFromFile( in ) ) {
            qDebug() << "Failed to load groups from '" << fileUrl << "'";
            std::exit( EXIT_FAILURE );
        }
        _groups.push_back( group );
    } while( in.peek() != EOF );

    in.close();
    emit groupsChanged();
}
void DataSet::saveGroups( QUrl fileUrl ) const
{
    std::ofstream out( fileUrl.toLocalFile().toLocal8Bit().constData(), std::ios::out | std::ios::binary );
    if( !out ) {
        qDebug() << "Failed to save groups to '" << fileUrl << "'";
        std::exit( EXIT_FAILURE );
    }

    for( auto group : _groups )
        group->saveToFile( out );

    out.close();
}


QList<QObject*> DataSet::groups()
{
    QList<QObject*> list;
    for( auto group : _groups ) list.append( group );
    return list;
}
Q_INVOKABLE void DataSet::addGroup()
{
    this->addGroup( QString( "Group " ) + QString::number( _groups.size() + 1 ) );
}
void DataSet::addGroup( QString name )
{
    _groups.append( new Group( std::move( name ) ) );
    emit groupsChanged();
}

QVector<int> DataSet::findImportantDimensions( const Group* a, const Group* b ) const
{
    QVector<int> pixelsA = a->pixels();
    QVector<int> pixelsB = b->pixels();

    std::vector<double> pValues;

    for(unsigned int i=0; i<this->mzValuesSize; i++)
    {
        std::vector<double> intensitiesA;
        std::vector<double> intensitiesB;

        for(unsigned int j=0; j<pixelsA.size(); j++)
        {
            intensitiesA.push_back(this->intensity(pixelsA.at(j), i));
        }

        for(unsigned int j=0; j<pixelsB.size(); j++)
        {
            intensitiesB.push_back(this->intensity(pixelsB.at(j), i));
        }

        alglib::ae_int_t n = intensitiesA.size();
        alglib::real_1d_array x;
        x.setcontent(n, intensitiesA.data());

        alglib::ae_int_t m = intensitiesB.size();
        alglib::real_1d_array y;
        y.setcontent(m, intensitiesB.data());

        double bothtails;
        double lefttail;
        double righttail;

        alglib::studentttest2(x, n, y, m, bothtails, lefttail, righttail);

        pValues.push_back(bothtails);
    }

    std::vector<size_t> sortedIndices = sort_indexes(pValues);

    QVector<int> importantDimensions;

    for(unsigned int i=0; i<sortedIndices.size(); i++)
    {
        importantDimensions.push_back((int)(sortedIndices.at(i)));
    }

    return importantDimensions;
}

void DataSet::clearGroups()
{
    for( auto group : _groups ) delete group;
    _groups.clear();
    emit groupsCleared();
}
