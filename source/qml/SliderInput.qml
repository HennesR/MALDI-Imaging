import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4

ColumnLayout {
    id: root

    property string name

    property alias value : slider.value
    property alias min : slider.minimumValue
    property alias max : slider.maximumValue
    property alias step: slider.stepSize

    Text { Layout.fillWidth: true; text: root.name + ": " + slider.value }
    Slider {
        id: slider
        Layout.fillWidth: true; Layout.maximumHeight: 20
        Layout.leftMargin: 10; Layout.rightMargin: 10;

        updateValueWhileDragging: false
    }
}
