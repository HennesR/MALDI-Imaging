import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.4

ColumnLayout {
    id: root

    property Item module
    property alias groups : comboBox.model

    Text { Layout.fillWidth: true; text: "Output Group" }
    ComboBox {
        id: comboBox
        Layout.fillWidth: true; Layout.maximumHeight: 30
        Layout.leftMargin: 10; Layout.rightMargin: 10
        textRole: "name"
        property int selected : -1

        onModelChanged: {
            currentIndex = selected
        }
        onActivated: {
            selected = currentIndex
            root.module.setOutputGroup( model[currentIndex] )
        }
    }
}
