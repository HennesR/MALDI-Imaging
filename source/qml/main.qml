import QtQuick.Layouts 1.12
import QtQuick 2.12
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.3

import MALDI_Imaging 1.0

RowLayout {
    id: root
    width: 1080; height: 720

    DataSet {
        id: dataSet; objectName: "dataSet"
    }
    signal openFile( url fileUrl )

    Flickable {
        Layout.minimumWidth: 0.2 * parent.width; Layout.fillHeight: true
        Layout.leftMargin: 5.0
        contentHeight: children[1].implicitHeight
        ScrollIndicator.vertical: ScrollIndicator {}

        ColumnLayout {
            anchors.fill: parent

            Button {
                Layout.fillWidth: true
                text: "Open File..."
                onClicked: fileDialog_openFile.open()

                FileDialog {
                    id: fileDialog_openFile; title: "Please choose a file"
                    folder: shortcuts.desktop; selectMultiple: false
                    onAccepted: root.openFile( fileUrl )
                }
            }

            MenuSeparator { Layout.fillWidth: true }

            Settings {
                name: "Parallel Coordinates"; module: parallelCoordinatesSpace
                content : ColumnLayout {
                    GroupOutput{ module: parallelCoordinatesSpace; groups: dataSet.groups }
                }
            }

            Settings {
                name: "Sunburst"; module: projectionSpace
                content : Rectangle { Layout.fillWidth: true; Layout.preferredHeight: 200; color: "red" }
            }

            Settings {
                name: "Scatter Plot"; module: scatterSpace;
                content : ColumnLayout {
                    GroupOutput{ module: scatterSpace; groups: dataSet.groups }
                }
            }

            Settings {
                name: "Box Plot"; module: boxPlotSpace;
                content : ColumnLayout {
                    GroupInput{ module: boxPlotSpace; groups: dataSet.groups }
                    SliderInput{ name: "Dimension count"; min: 1; max: 20; step: 1; onValueChanged: boxPlotSpace.dimensionCount = value }
                }
            }

            Settings {
                name: "Spectrum"; module: spectrumSpace;
                content : ColumnLayout {
                    GroupInput{ module: spectrumSpace; groups: dataSet.groups }
                }
            }

            Settings {
                name: "Image"; module: imageSpace;
                content : ColumnLayout {
                    GroupInput{ module: imageSpace; groups: dataSet.groups }
                    GroupOutput{ module: imageSpace; groups: dataSet.groups }
                }
            }

            MenuSeparator { Layout.fillWidth: true }

            RowLayout {
                Button {
                    Layout.fillWidth: true; Layout.maximumHeight: 20
                    text: "Load groups"
                    onClicked: fileDialog_loadGroups.open()
                    FileDialog {
                        id: fileDialog_loadGroups; title: "Please choose a file"
                        folder: shortcuts.desktop; selectMultiple: false
                        onAccepted: dataSet.loadGroups( fileUrl )
                    }
                }
                Button {
                    Layout.fillWidth: true; Layout.maximumHeight: 20
                    text: "Save groups"
                    onClicked: fileDialog_saveGroups.open()
                    FileDialog {
                        id: fileDialog_saveGroups; title: "Please choose a file"
                        folder: shortcuts.desktop; selectMultiple: false; selectExisting: false
                        onAccepted: dataSet.saveGroups( fileUrl )
                    }
                }
            }

            Button {
                Layout.fillWidth: true; Layout.maximumHeight: 20
                text: "Add Group"
                onClicked: dataSet.addGroup()
            }
            Repeater {
                model: dataSet.groups
                RowLayout {
                    Layout.fillWidth: true

                    Button {
                        Layout.fillWidth: true; Layout.maximumHeight: 20
                        text: modelData.name
                    }
                    Button {
                        Layout.maximumWidth: 40; Layout.maximumHeight: 20
                        background: Rectangle { color: modelData.color }
                        onClicked: colorDialog.open()

                        ColorDialog {
                            id: colorDialog; title: "Please choose a color"
                            onAccepted: modelData.color = color
                        }
                    }
                }
            }

            Rectangle { Layout.fillHeight: true }
        }
    }

    ColumnLayout {
        Layout.fillWidth: true; Layout.fillHeight: true

        RowLayout {
            ParallelCoordinatesSpace {
                id: parallelCoordinatesSpace; objectName: "parallelCoordinatesSpace"
                Layout.fillWidth: true; Layout.fillHeight: true
            }
            ProjectionSpace {
                id: projectionSpace; objectName: "projectionSpace"
                Layout.fillWidth: true; Layout.fillHeight: true

                LassoTool { objectName: "projectionSpace_lasso"; anchors.fill: parent }
            }
            ScatterSpace {
                id: scatterSpace; objectName: "scatterSpace"
                Layout.minimumWidth: height; Layout.fillHeight: true

                LassoTool { objectName: "scatterSpace_lasso"; anchors.fill: parent }
            }
        }
        RowLayout {
            BoxPlotSpace {
                id: boxPlotSpace; objectName: "boxPlotSpace"
                Layout.fillWidth: true; Layout.fillHeight: true
            }

            SpectrumSpace {
                id: spectrumSpace; objectName: "spectrumSpace"
                Layout.fillWidth: true; Layout.fillHeight: true
            }

            ImageSpace {
                id: imageSpace; objectName: "imageSpace"
                Layout.minimumWidth: height; Layout.fillHeight: true

                LassoTool { objectName: "imageSpace_lasso"; anchors.fill: parent }
            }
        }
    }
}
