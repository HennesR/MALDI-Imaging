import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.4

ColumnLayout {
    id: root

    property Item module
    property alias groups : repeater.model

    Text { Layout.fillWidth: true; text: "Input Groups" }
    ColumnLayout {
        spacing: 1.0

        Repeater {
            id: repeater
            Button {
                property bool selected: root.module.containsInputGroup( modelData )
                Layout.fillWidth: true; Layout.maximumHeight: 20
                Layout.leftMargin: 10; Layout.rightMargin: 10; Layout.topMargin: 0

                text: modelData.name
                background: Rectangle { color: selected? "#999999" : "#bbbbbb" }
                onClicked: {
                    root.module.toggleInputGroup( modelData )
                    selected = root.module.containsInputGroup( modelData )
                }
            }
        }
    }
}
