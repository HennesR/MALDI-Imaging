import QtQuick.Layouts 1.12
import QtQuick.Controls 2.4
import QtQuick 2.12

ColumnLayout {
    id: root

    property Item module
    property alias name: button.text
    property alias content: content.data

    RowLayout {
        Layout.fillWidth: true
        spacing: 0.0

        Button {
            id: button
            Layout.fillWidth: true
            Layout.maximumHeight: 20
            onClicked: content.visible = !content.visible
        }

        Button {
            Layout.maximumWidth: 20
            Layout.maximumHeight: 20
            text: root.module.visible? "-" : "+"
            onClicked: root.module.visible = !root.module.visible
        }
    }

    ColumnLayout {
        id: content
        visible: false
        Layout.fillWidth: true
    }
}
