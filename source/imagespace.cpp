#include "imagespace.h"
#include "lassotool.h"
#include <algorithm>

void ImageSpace::setDataSet( const DataSet* pDataSet )
{
    _dataSet = pDataSet;

    QObject::connect( pDataSet, &DataSet::dataChanged, [this]{
        _lasso->clear();

        _backgroundData.resize( _dataSet->pixelCount() );
        std::fill( _backgroundData.begin(), _backgroundData.end(), 0xff000000 );
        _backgroundImage = QImage( reinterpret_cast<const uchar*>(_backgroundData.data()), _dataSet->width(), _dataSet->height(), QImage::Format_ARGB32 );

        _groupsData.resize( _dataSet->pixelCount() );
        std::fill( _groupsData.begin(), _groupsData.end(), 0 );
        _groupsImage = QImage( reinterpret_cast<const uchar*>(_groupsData.data()), _dataSet->width(), _dataSet->height(), QImage::Format_ARGB32 );

        _highlightData.resize( _dataSet->pixelCount() );
        std::fill( _highlightData.begin(), _highlightData.end(), 0 );
        _highlightImage = QImage( reinterpret_cast<const uchar*>(_highlightData.data()), _dataSet->width(), _dataSet->height(), QImage::Format_ARGB32 );
    });

    QObject::connect( pDataSet, &DataSet::groupsCleared, [this]{
        for( auto group : _inputGroups ) QObject::disconnect( group, nullptr, this, nullptr );
        _inputGroups.clear();
        this->updateGroupData();

        this->setOutputGroup( nullptr );
    });
}

void ImageSpace::setDimensions( const QVector<int>& dimensions )
{
    const QVector<QColor> COLORS {
        0xfafafa, 0xe6194b, 0x3cb44b, 0xffe119, 0x4363d8, 0xf58231, 0x911eb4, 0x42d4f4,
        0xf032e6, 0xbfef45, 0xfabebe, 0x469990, 0xe6beff, 0x9A6324, 0xfffac8, 0x800000,
        0xaaffc3, 0x808000, 0xffd8b1, 0x000075, 0xa9a9a9
    };
    if( dimensions.size() > COLORS.size() ) {
        qDebug() << "Not enough colors specified in ImageSpace::setDimensions( ... )";
        return;
    }

    // Fill background image data
    std::fill( _backgroundData.begin(), _backgroundData.end(), 0xff000000 );
    if( dimensions.size() )
    {
        // Find maximum intensities for each dimension
        QVector<float> maxIntensities( dimensions.size(), 0.0f );
        for( int i = 0; i < _dataSet->pixelCount(); ++i )
            for( int j = 0; j < dimensions.size(); ++j )
                if( _dataSet->intensity( i, dimensions[j] ) > maxIntensities[j] ) maxIntensities[j] = _dataSet->intensity( i, dimensions[j] );

        for( int i = 0; i < _dataSet->pixelCount(); ++i )
        {
            // Find dimension with maximum intensity at this pixel
            int dimension = 0;
            float maxIntensity = 0.0f;
            for( int j = 0; j < dimensions.size(); ++j ) {
                if( _dataSet->intensity( i, dimensions[j] ) > maxIntensity ) {
                    maxIntensity = _dataSet->intensity( i, dimensions[j] );
                    dimension = j;
                }
            }

            // Color the pixel according to normalized intensity
            const float opacity = maxIntensity / maxIntensities[dimension];

            QColor color = COLORS[dimension];
            color.setAlpha( color.alpha() * opacity );

            _backgroundData[i] = color.rgba();
        }
    }

    this->update();
}
void ImageSpace::setHighlight( const QVector<pixelCluster>& clusters )
{
    std::fill( _highlightData.begin(), _highlightData.end(), 0 );
    for( auto& cluster : clusters ) {
        QColor color = cluster.color;
        for( auto& pixel : cluster.pixels ) {
            color.setAlpha( 0xff * pixel.opacity );
            _highlightData[pixel.index] = color.rgba();
        }
    }

    this->update();
}

void ImageSpace::toggleInputGroup( Group* pGroup )
{
    if( this->containsInputGroup( pGroup ) ) {
        QObject::disconnect( pGroup, nullptr, this, nullptr );

        _inputGroups.removeOne( pGroup );
    }
    else {
        QObject::connect( pGroup, SIGNAL( colorChanged() ), this, SLOT( updateGroupData() ) );
        QObject::connect( pGroup, SIGNAL( pixelsChanged() ), this, SLOT( updateGroupData() ) );

        _inputGroups.push_back( pGroup );
    }

    this->updateGroupData();
}
bool ImageSpace::containsInputGroup( Group* pGroup ) const
{
    return _inputGroups.contains( pGroup );
}
void ImageSpace::setOutputGroup( Group* pGroup )
{
    _outputGroup = pGroup;
}

void ImageSpace::updateGroupData()
{
    std::fill( _groupsData.begin(), _groupsData.end(), 0 );
    for( auto& group : _inputGroups )
        for( auto i : group->pixels() )
            _groupsData[i] = group->color().rgba();

    this->update();
}

void ImageSpace::componentComplete()
{
    QQuickPaintedItem::componentComplete();

    this->setAcceptHoverEvents( true );
    this->setAcceptedMouseButtons( Qt::LeftButton | Qt::RightButton );

    _lasso = this->findChild<LassoTool*>( "imageSpace_lasso" );
}
void ImageSpace::paint( QPainter* painter )
{
    // Draw background and image
    const QRectF fillRect = QRectF( QPointF( 0.0, 0.0 ), size() );
    painter->fillRect( fillRect, Qt::black );
    painter->drawImage( fillRect, _backgroundImage );
    painter->drawImage( fillRect, _groupsImage );
    painter->drawImage( fillRect, _highlightImage );
}
void ImageSpace::hoverMoveEvent( QHoverEvent* event )
{
    if( _outputGroup ) {
        _lasso->hoverMoveEvent( event );
        this->update();
    }
}
void ImageSpace::mousePressEvent( QMouseEvent* event )
{
    if( !_outputGroup ) return;

    _lasso->mousePressEvent( event );

    if( _lasso->finished() )
    {
        const auto& polygon = _lasso->normalizedPolygon();
        const QRectF rect = polygon.boundingRect();
        const qreal stepX = 1.0 / _dataSet->width(), stepY = 1.0 / _dataSet->height();

        QVector<int> pixels;
        for( qreal x = rect.left(); x < rect.right(); x += stepX ) {
            for( qreal y = rect.top(); y < rect.bottom(); y += stepY ) {
                if( polygon.containsPoint( QPointF( x, y ), Qt::OddEvenFill ) ) {
                    int px = x * _dataSet->width(), py = y * _dataSet->height();
                    pixels.append( py * _dataSet->width() + px );
                }
            }
        }

        if( event->button() == Qt::LeftButton )
            _outputGroup->addPixels( pixels );
        else _outputGroup->removePixels( pixels );

        _lasso->clear();
        this->update();
    }
}
