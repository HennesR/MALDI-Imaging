#ifndef SHARED_H
#define SHARED_H

#include <QVector>
#include <QColor>

struct vec3
{
    float x, y, z;
};

struct pixel
{
    int index;
    float opacity;
};
struct pixelCluster
{
    QColor color;
    QVector<pixel> pixels;
};

struct point
{
    vec3 position;
    int pixelIndex;
    float opacity;
};
struct pointCluster
{
    QColor color;
    QVector<point> points;
};

#endif // SHARED_H
