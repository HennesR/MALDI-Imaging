\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{hyperref} 
\usepackage[figure]{hypcap} 

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Visualisierung von multidimensionalen spektralen Feldern\\}

\author
{
    \IEEEauthorblockN{Florian Fechner}
    \IEEEauthorblockA{\textit{Westfälische Wilhelms-Universität Münster} \\
    Münster, Deutschland \\
    f\_fech03@uni-muenster.de}
\and
    \IEEEauthorblockN{Hennes Rave}
    \IEEEauthorblockA{\textit{Westfälische Wilhelms-Universität Münster} \\
    Münster, Deutschland \\
    h\_rave01@uni-muenster.de}
}

\maketitle

\begin{abstract}
Die Visualisierung von multidimensionalen spektralen Feldern ist für viele Anwendungsbereiche 
aus Naturwissenschaften relevant. Dieser Abschlussbericht soll einen Überblick über verschiedene 
Methoden liefern die Daten multidimensionale spektrale Felder in einer für den Anwender sinnvollen 
Art und Weise aufzubereiten und darzustellen. Hierbei liegt der Fokus im Rahmen dieses Projektberichts 
auf dem sogenannten MALDI-Imaging. 
\end{abstract}

\section{Einleitung}
Bei dem bereits im Abstract erwähntem MALDI-Imaging handelt es sich um unterschiedliche Vorgehensweisen, die 
Daten, welche im Zuge der MALDI-TOF Analyse entstehen, visuell aufzubereiten. Bei der MALDI-TOF Analyse werden 
Proben (z.B. Gewebetypen) auf ihre chemischen Bestandteile hin untersucht. Hierbei liefert die Analyse für jedes Element (im Folgenden auch "Pixel" genannt)
eines zweidimensionalen Feldes (im Folgendem auch "Bildraum" genannt) ein Spektrum von Masse-zu-Ladung Werten (im Folgendem "m/z-Werte" genannt) zu 
den im Feldelement vorkommenden Intensitäten. 

Nach der MALDI-TOF Analyse folgt eine Vorverarbeitung der Daten. Hierbei findet eine Normalisierung der Daten statt. Danach 
werden Messungenauigkeiten durch baseline correction und Glättung ausgeglichen. Anschließend wird die Menge der m/z-Werte im sogenannten 
alignment reduziert. \cite{lib:bioinf}

Die vorverarbeiteten Daten sollen nun zugänglich gemacht werden in einer Art und Weise, in welcher der Anwender 
der Software möglichst schnell die für ihn relevanten Informationen erhält. Zu den Hauptzielen, die der Anwender 
im klassischem Fall verfolgt, gehört zum einen das Finden von Regionen mit ähnlichen m/z-Intensitäten (Clustering) 
und zum anderen das Vergleichen von zwei oder mehr Regionen, um relevante m/z-Werte zu finden, die zu ihrer Unterscheidung 
beitragen. Außerdem kann für den Nutzer relevant sein, wie dicht sich die einzelnen Bereiche im Feld befinden und ob sie 
einen geschlossenen Bereich bilden oder an mehreren Stellen im Feld verteilt sind. 


\section{Bisherige Methoden}

Ein bislang verwendeter, manueller Ansatz der Analyse besteht darin, ein Bild anhand ausgewählter m/z-Werte farbzukodieren, um so auffällige Regionen zu entdecken.
Ausgewählt werden die jeweiligen m/z-Werte anhand eines Spektrums, welches die durchschnittlichen Intensitäten bei allen m/z-Werten selektierter Pixel anzeigt.
\cite{lib:bioinf}

Dieser manuelle Ansatz hat mehrere Nachteile. Zum einen ist er sehr zeitaufwendig, da praxisrelevante Datensätze oftmals mehrere hunderte bis tausende m/z-Werte aufweisen, zum anderen sind multidimensionale Regionen schwierig zu erkennen, da sich diese aus mehreren m/z-Werten zusammensetzen.

Um diese Probleme zu beheben werden zum Beispiel Algorithmen wie princical component analysis (PCA) oder hierarchisches Clustering verwendet. Diese helfen dabei, die Pixel anhand der Ähnlichkeit ihrer Spektren zu gruppieren. 
Das Anwenden solcher Algorithmen verringert die Analysezeit drastisch und erlaubt auch das einfachere Auffinden multidimensionaler Regionen. Allerdings ist hierbei keineswegs sichergestellt, dass tatsächlich alle Regionen entdeckt werden.
\cite{lib:maldireview}



\section{Im Projekt verwendete Methoden}


\subsection{Stochastic Neighbour Embedding}

Beim Stochastic-Neighbour-Embedding (kurz SNE) handelt es sich um eine Methode der nichtliniearen Dimensionalitätsreduktion. Hierbei werden Punkte in einem hochdimensionalen Raum auf einen niedrigdimensionalen Raum abgebildet, wobei die Ähnlichkeitsinformationen der Punkte untereinander so gut es geht erhalten bleiben sollen. 

Dieses Kapitel beginnt mit den Grundlagen von SNE. Anschließend werden Varianten des SNE vorgestellt und ihre Vor- und Nachteile beleuchtet. 


\subsubsection{Grundlagen}

Beim SNE wird im hochdimensionalen Raum eine Ähnlichkeitsmatrix von jedem Punkt zu jedem anderen Punkt berechnet. Die Formel für die Ähnlichkeit zwischen von einem Punkt im hochdimensionalen Raum mit dem Index $i$ zu einem anderen Punkt im selben Raum mit dem Index $j$ ist hierbei gegeben durch

\[ p_{i|j}  = \frac{exp(-||x_i-x_j||/2\sigma^2_i)}{\sum_{k \neq i}exp(-||x_i-x_k||/2\sigma^2_i)} \]

und im niedrigdimensionalem Zielraum 

\[ q_{i|j}  = \frac{exp(-||x_i-x_j||)}{\sum_{k \neq i}exp(-||x_i-x_k||)} \]

Anschließend wird im niedrigdimensionalem Zielraum für jeden Punkt im hochdimensionalen Raum zufällig ein Punkt im niedrigdimensionalen Raum platziert. In jedem Iterationsschritt wird die Kullback-Leibler Divergenz 

\[ C = KL(P||Q) = \sum_{i}\sum_{j}p_{ij} log(\frac{p_{ij}}{q_{ij}}) \]

zwischen den Ähnlichkeiten im hochdimensionalen und dem niedrigdimensionalen Raum durch ein Gradientenabstiegsverfahren reduziert. Auf diese Art und Weise nähern sich die Ähnlichkeiten des niedrigdimensionalen Raums den Ähnlichkeiten des hochdimensionalen an. Ist die Annäherung gut genug oder wurde die Zahl der maximalen Iteration überschritten, stoppt SNE und gibt die Positionen der Punkte im niedrigdimensionalen Raum aus. 
\cite{lib:tsne}


\subsubsection{t-SNE}

SNE besitzt allerdings ein paar Schwächen. So entsteht in der Mitte des niedrigdimensionalen Zielraums oft ein "Gedränge". Um diesen Effekt zu reduzieren, wird t-SNE verwendet, wodurch die Punkte tendenziell gleichmäßiger verteilt werden. Die Formel zur Berechnung der Ähnlichkeitswerte verändert sich hierbei im hochdimensionsionalen Raum zu

\[ p_{i|j}  = \frac{exp(-||x_i-x_j||^2/2\sigma^2)}{\sum_{k \neq l}exp(-||x_k-x_l||^2/2\sigma^2)} \]

Im niedrigdimensionalem Zielraum verändert sich die Formel zur Berechnung der paarweisen Ähnlichkeit zu

\[ q_{i|j}  = \frac{(1 + ||x_i-x_j||^2)^{-1}}{\sum_{k \neq l}(1 + ||x_k-x_l||^2)^{-1}} \]

SNE und damit auch t-SNE haben aufgrund des Umstands, dass eine Ähnlichkeitsmatrix aufgebaut wird, bei der die Ähnlichkeiten von jedem Punkt zu jedem anderen Punkt berechnet wird eine Laufzeitkomplexität von $O(n^2)$, wobei n die Anzahl der Punkte darstellt. 
Eine Möglichkeit die Laufzeitkomplexität von t-SNE auf $O(n \cdot log(n))$ zu reduzieren, ist durch die Verwendung der Barnes-Hut Approximation. Hierbei weicht das Ergebnis allerdings geringfügig von dem originalen t-SNE Algorithmus ab. 
Bei der Barnes-Hut Approximation werden die Positionen von Punkten, die sich weiter entfernt von dem aktuell betrachteten Punktposition befinden zu einer Punktposition zusammengefasst. Dies verfälscht das Ergebnis nur geringfügig, da beim t-SNE Algorithmus weit entfernte Punkte einen wesentlich geringeren Einfluss auf den aktuell betrachteten Punkt ausüben. 
[Barnes-Hut t-SNE Paper]


\subsubsection{Hierachisches SNE}

Eine weitere Option das Problem der ähnlichkeitsbasierten Dimensionsreduktion approximativ zu lösen liegt in der Verwendung von hierarchischem SNE (oder kurz HSNE). Die Idee hierbei ist es die Punkte im hochdimensionalen Raum so in einer Hierarchie anzuordnen, dass Punkte auf einer höheren Hierarchieebene Punkte auf der darunterliegenden Ebene durch einen hohen Ähnlichkeitswert zu ihnen repräsentieren. Auf diese Art und Weise kann die Zahl der Punkte, die durch den t-SNE Algorithmus eingebettet werden müssen, reduziert werden. 

Hierarchisches SNE beginnt mit der Konstruktion eines k-Nearest-Neighbours Graphen (kurz kNN-Graph) bezüglich der Punkte im hochdimensionalen Raum. Die übliche Laufzeit hierfür wäre bereits $O(n^2)$, da alle Punkte mindestens einmal paarweise miteinander verglichen werden müssten. Allerdings gibt es approximative Methoden zur Konstruktion eines kNN-Graphen, die in $O(n \cdot log(n))$ liegen. Eine dieser approximativen Methoden wird in der Fast-Library-for-Approximate-Nearest-Neighbours (kurz FLANN) 
\cite{lib:flann} realisiert. Hierbei wird grundsätzlich zwischen hochdimensionalen, niedrigdimensionalen und binären Daten unterschieden. Beim MALDI-Imaging liegen sehr häufig hochdimensionale Daten aufgrund der hohen Menge der m/z-Werte vor. 

Für diese Art von Daten wird nach dem Paper "Scalable Nearest Neighbour Algorithms for High Dimensional Data" \cite{lib:flann} ein Priority-Search-k-Means-Tree Algorithmus empfohlen. Bei diesem Algorithmus wird zuerst ein Baum konstruiert, indem auf die Punkte ein k-Means-Clustering angewendet wird. Danach wird der Prozess in den Kind-Knoten des Baumes rekursiv für die jeweiligen Cluster fortgesetzt, bis ein Knoten weniger als $k$ Punkte enthält. Dabei wird für jeden Knoten die durchschnittliche Position der enthaltenen Knoten berechnet. Werden nun die k-nächsten Nachbarn eines Punktes $q$ gesucht, wird im k-Means-Baum stets der Knoten verfolgt, bei welchem die durchschnittliche Position aller enthaltenen Punkte dem Punkt $q$ am nächsten ist. Alle anderen Knoten werden in eine Prioritätswarteschlange bezüglich der Distanz von der durchschnittlichen Position der enthaltenen Punkte zu $q$ eingefügt.  Wird ein Blatt erreicht, werden die enthaltenen Punkte in eine zweite Prioritätswarteschlange eingefügt, wobei die Priorität höher ist, wenn die Distanz des Punktes zum Punkt $q$ geringer ist. Danach werden der zweiten Prioritätswarteschlange $k$ Elemente entnommen. Dies sind dann die approximativen $k$ nächsten Nachbarn zu Punkt $q$. 

Ein zentraler Punkt beim HSNE sind die sogenannten Landmarks. Hierbei handelt es sich um Punkte, welche mit einer höheren Skalierungsstufe in der Form reduziert werden, dass sie den Datensatz dennoch so gut es geht repräsentieren können. Die Landmarks einer Skalierungstufe haben also eine hohe Ähnlichkeit zu den Landmarks der nächst höheren Stufe. 
Nachdem die nächsten Nachbarpunkte für jeden Punkt ermittelt wurden, wird eine dünnbesetzte Ähnlichkeitsmatrix berechnet, welche die Ähnlichkeit eines jeden Punktes zu seinen Nachbarn enthält. Hierbei wird, um keine quadratische Laufzeit zu erhalten, für jeden Punkt, der nicht zu den $k$ nächsten Nachbarn eines Punktes gehört eine Ähnlichkeit von $0$ angenommen. Die hierbei entstehende Ähnlichkeitsmatrix stellt die Übergangsmatrix erster Ordnung dar. 
Anschließend wird ein Monte-Carlo-Algorithmus \cite{monte-carlo} für finite Markov-Ketten verwendet, um den stabilen Vektor der Markov-Kette, welche die Übergangsmatrix darstellt, approximativ zu berechnen. Alle Punkte, dessen Werte hierbei einen gegebenen Treshhold überschreiten, werden dann in die Menge der Landmarks der nächst höheren Skalierungsstufe aufgenommen. 
Danach wird die Einflussmatrix berechnet, durch welche dargestellt wird, wie gut ein Landmark auf einer skalierungsstufe von einem Landmark der nächst höheren Skalierungsstufe verglichen mit den anderen Landmarks repräsentiert ist. Auf Basis der Landmarks und der Einflussmatrix wird dann die nächste Übergangsmatrix berechnet und die Prozedur wird fortgesetzt, bis die gewünschte maximale Skalierung erreicht ist. 
Durch die sogenannte "Drill Down"-Filterung werden auf Basis der Einflussmatrix Landmarks einer Skalierungsstufe bestimmt, welche von einer ausgewählten Gruppe Landmarks einer höheren Skalierungsstufe gut repräsentiert werden. 
\cite{lib:hsne}

HSNE benötigt eine höhere Berechnungszeit als t-SNE. Allerdings bietet die berechnete Struktur dem Nutzer die Möglichkeit die Daten im Sinne von Selektion und Zoom interaktiv zu erkunden. So können Landmarks selektiert und die mit ihnen verbundenen Punkte schnell gefiltert werden. Bei t-SNE würde eine neue Selektion eine komplette Neuberechnung mit den in ihr enthaltenen Punkte mit sich ziehen. 

\subsubsection{Implementierung}

In der diesem Abschlussbericht beigefügten Software wird als Distanzmetrik, welche zur Bestimmung der Ähnlichkeiten verwendet wird, der euklidische Abstand 

\[ d(p, q) = \sqrt{\sum_{i=1}^n (q_i - p_i)^2  } \]

verwendet. Hierbei bildet jeder m/z-Wert eine eigene Dimension und der Wert in jeder Dimension ist die zugehörige Intensität. Jeder Bildpunkt stellt einen Datenpunkt im hochdimensionalen Raum dar. Diese Distanzmetrik ist nicht optimal, da sich bestimmte m/z-Werte im chemischen Sinne näher sein können als andere, wodurch die Sicht, dass jeder m/z-Wert eine eigene unabhängige Dimension ist, eine starke Vereinfachung darstellt. Allerdings ist es nicht trivial, und auch nicht Fokus dieses Projekts, eine geeignetere Distanzmetrik hierfür zu finden. 

In der Software wird bei jeder Verwendung von t-SNE eine Abfrage zur Menge der einzubettenden Punkte durchgeführt. Unterschreitet sie einen Schwellwert, wird die Laufzeitkomplexität von $O(n^2)$ in Kauf genommen und der originale t-SNE Algorithmus wird ausgeführt. 
Entspricht die Punktmenge allerdings dem Schwellwert oder wird dieser überschritten, wird die Barnes-Hut Approximation genutzt. Dies soll ein ausgewogenes Gleichgewicht zwischen Qualität und Laufzeit schaffen. Für die Barnes-Hut Approximation wurde eine Bibliothek verwendet, 
der korrekte wurde selbstständig implementiert. 

In der diesem Bericht beigefügten Software wurde HSNE sequenziell implementiert. Eine parallelisierte Implementierung hätte die Berechnungsdauer reduzieren können. Ebenso hätte man die Dünnbesetzung der Matrizen algorithmisch besser ausnutzen können.  



\subsection{Ansicht - CyteGuide}

In Abbildung \ref{fig:cyteguide} wird die CyteGuide-Ansicht dargestellt. Die CyteGuide Darstellung der diesem Bericht beigefügten Software orientiert sich stark am Paper "CyteGuide: Visual Guidance for Hierarchical Single-Cell Analysis" \cite{lib:cyteguide} 
und bietet eine hierachische Ansicht der algorithmisch bestimmten Cluster sowie ihre Ähnlichkeiten zueinander.

Hierfür wird ein Baum auf Basis der Ergebnisse des HSNE Algorithmus gebaut. Hierzu werden alle Landmarks der höchsten Skalierungsstufe dem Wurzelknoten hinzugefügt. Anschließend wird ein Clustering 
der sich in der Wurzel befindenen Punkte mit dem $k$-Means Algorithmus durchgeführt. Danach wird für die Punkte jedes Clusters ein drill down durchgeführt und der Prozess wird für die Kinder des aktuellen 
Knoten rekursiv fortgeführt. 

Zur Darstellung des so entstehenden Baumes wird ein Sunburst-Layout genutzt. Für jeden Knoten dieses Baumes wird nun für die in ihm enthaltenen Punkte eine t-SNE Einbettung durchgeführt. 
Die Punkte werden als Kreise dargestellt. Die Zugehörigkeit der Punkte zu ihrem Cluster wird durch ihre Farbe gekennzeichnet. Außerdem wird die Anzahl der Punkte, die durch ein Landmark repräsentiert werden, durch die Größe der Kreise dargestellt.  
Ebenso wird, zur besseren Unterscheidbarkeit der Punkte, die Transparenz der Kreise erhöht, wenn ihre Größe zunimmt. 

\begin{figure}[htbp]
    \centerline{\includegraphics[width=60mm]{images/cyteguide.png}}
    \caption{CyteGuide - Ansicht}
    \label{fig:cyteguide}
\end{figure}

Da eine normalisierte t-SNE Einbettung Punkte innerhalb eines Quadrats abbildet, die t-SNE Einbettungen in der Sunburst-Darstellung allerdings 
im Falle des Wurzel-Knotens in einem Kreis und im Falle eines Nicht-Wurzel-Knotens in einem Kreisring angelegt werden, entsteht zwangsläufig ein ungenutzter Raum. 
Eine Verzerrung der t-SNE Einbettung wäre nicht ratsam, da durch diese die Abstände der Punkte zueinander fehlinterpretiert werden könnten. Der 
ungenutzte Raum kann allerdings minimiert werden, indem für die Wurzelknoteneinbettung das Quadrat mit dem Mittelpunkt des Kreise und der Seitenlänge 
$s = 2 \cdot cos(\frac{\pi}{4}) * r$ nimmt, wobei $r$ der Radius des inneren Sunburstkreises ist. Für die Einbettungen in den Kreisringen, bietet 

\[ s = \frac{3}{5} * (-2 \cdot r_1 + \sqrt{5 \cdot r_2^2 - r_1^2}) \]

die platzoptimale Seitengröße des einzubettenden Quadrats, wobei $r_2$ der Radius des äußeren und $r_1$ des inneren Kreises vom Kreisring darstellt. 
Ein Quadratmittelpunkt ist dann $r_1 + \frac{s}{2}$ vom Kreismittelpunkt entfernt. 



\subsection{Ansicht - Spektrum}

In Abbildung \ref{fig:spectrum} wird die Spektrum-Ansicht dargestellt. Das Spektrum zeigt die durchschnittliche Intensitäten aller Pixel in allen Dimensionen. Einzelne Dimensionen können ausgewählt werden, um die Bild-Ansicht (siehe Abbildung~\ref{fig:image}) nach diesen farbzukodieren.

Außerdem ist es möglich, sich die min/max-Intensitätsbänder selektierter Regionen anzuschauen, dies kann ein erster Ansatz zur Unterscheidung von wichtigen Dimensionen der jeweiligen Regionen sein.

\begin{figure}[htbp]
    \centerline{\includegraphics[width=60mm]{images/spectrum.png}}
    \caption{Spectrum - Ansicht}
    \label{fig:spectrum}
\end{figure}


\subsection{Ansicht - Image}

In Abbildung \ref{fig:image} wird die Image-Ansicht dargestellt. Die im Spektrum selektierte Dimension wird hier zur Farbkodierung verwendet, d.h. jeder Pixel wird entsprechend seiner Intensität in der Dimension in Graustufen gefärbt. 
Weiter können die bislang gefundenen Regionen im Bild angezeigt und manuell mithilfe eines Lasso-Werkzeuges abgeändert werden.

\begin{figure}[htbp]
    \centerline{\includegraphics[width=60mm]{images/image.png}}
    \caption{Image - Ansicht}
    \label{fig:image}
\end{figure}


\subsection{Ansicht - Boxlot}

In Abbildung \ref{fig:boxplot} wird die Boxplot-Ansicht dargestellt. Im Boxplot können entweder zwei Regionen miteinander oder eine Region mit der Gesamtheit aller Pixel verglichen werden. 
Dabei werden die zur Unterscheidung der beiden Regionen wichtigsten Dimensionen durch stochastische Tests ermittelt und jeweils Maximum, Minimum, Median, oberes- und unteres Quartil der Intensitäten angezeigt. 
Im anbeigefügten Projekt wurde der studentische t-Test verwendet. 

\begin{figure}[htbp]
    \centerline{\includegraphics[width=60mm]{images/boxplot.png}}
    \caption{Boxplot - Ansicht}
    \label{fig:boxplot}
\end{figure}


\subsection{Ansicht - Parallele Koordinaten}

In Abbildung \ref{fig:parallelcoordinates} wird die Parallele-Koordinaten-Ansicht dargestellt. Die im Boxplot gefundenen Dimensionen werden hier als parallele Koordinaten gezeigt. Es können auf der y-Achse, welche die normalisierte Intensität anzeigt, Bereiche erstellt oder entfernt werden. Daraufhin wird in jeder Dimension durch eine Farbkodierung sichtbar gemacht, wie viele Pixel sich in diesem Intensitätsbereich befinden.
Durch das Auswählen von Bereichen lassen sich die dort befindlichen Pixel zu einer Region zusammenfassen. Dabei wird bei Bereichen in der gleichen Dimension die Vereinigung und bei Dimensionen unterschiedlicher Dimensionen der Schnitt gebildet.
Außerdem wird durch ein Band angedeutet, wie sich bereits ausgewählte Bereiche in anderen Dimensionen fortsetzen.

\begin{figure}[htbp]
    \centerline{\includegraphics[width=60mm]{images/parallel_coordinates.png}}
    \caption{Parallel Coordinates - Ansicht}
    \label{fig:parallelcoordinates}
\end{figure}


\section{Fazit}

Stochastic Neighbour Embedding stellt generell eine gute Möglichkeit dar, um dem Nutzer ein Werkzeug zu bieten, 
mit welchem er mehrere Spektren anhand ihrer Ähnlichkeit miteinander vergleichen kann. Die SNE Variante t-SNE verhindert das 
"Gedränge" in der Mitte des Zielraums, welches beim klassischen SNE entstehen würde. Leider haben SNE und t-SNE eine
quadratische Laufzeit, daher kann es sinnvoll sein auf Kosten der Korrektheit eine Approximation für den t-SNE Algorithmus 
zu verwenden, wie beispielsweise die Barnes-Hut Approximation, um die Laufzeit des Algorithmus zu verringern. 

Hierachisches SNE benötigt eine hohe initiale Aufbauzeit. Es kann sinnvoll sein die hohe Aufbauzeit in Kauf zunehmen, wenn auf 
einem Datensatz bestimmte Strukturen näher untersucht werden müssen und der Nutzer häufig zwischen einer 
detailierteren und gröberen Ansicht wechseln möchte. 

Leider weist die CyteGuide Darstellung einige Schwächen auf. So kann beim Nutzer fälschlicherweise eine Verbindung von einem 
ähnlich gefärbten Cluster von einem Knoten zu einem anderen Cluster in einem anderen Knoten hergestellt werden. Ebenso wird der Platz  
nicht optimal genutzt, da die normalisierte t-SNE Einbettungen quadratisch, das Sunburst-Layout aber radial ist. Ansonsten bietet sie 
allerdings eine gute Möglichkeit schnell Strukturen und Unterstrukutren zu finden, sowie ein Gefühl für ihre Ähnlichkeit 
untereinander zu gewinnen. 

Zur Bestimmung der m/z-Werte, welche maßgeblich zur Unterscheidung von zwei Gruppen beiträgt, kann die 
Boxplot-Ansicht ein nützliches Werkzeug darstellen. Dem Nutzer wird direkt vermittelt, welche Dimensionen zur Unterscheidung 
relevant sind und wie die Intensitäten in den jeweiligen Gruppen in den jeweiligen m/z-Werten pro Gruppe verteilt sind. 
Für die räumliche Einordnung im Bildraum liefert die Scatterplot-Ansicht dem Nutzer eine gute Hilfe. Durch Sie wird schnell 
klar ob eine Struktur im Bildraum zusammenhängend ist oder ob eine Struktur eher über den gesamten Bildraum verteilt ist. 


\listoffigures  

\begin{thebibliography}{00}
\bibitem{lib:maldireview} Emine B. Yalcin and Suzanne M. de la Monte: Review of Matrix-Assisted Laser Desorption Ionization-Imaging Mass Spectrometry for Lipid Biochemical Histopathology. Journal of Histochemistry \& Cytochemistry 2015, Vol. 63(10) 762–771
\bibitem{lib:bioinf} Alecandrov T: MALDI imaging mass spectrometry: statistical data analysis and current computational challenges. Alexandrov BMC Bioinformatics 2012, 13(Suppl 16):S11
\bibitem{lib:tsne} Van der Maaten L, Hinton G: Visualizing Data using t-SNE. Journal of Machine Learning Research 9 (2008) 2579-2605
\bibitem{lib:hsne} Pezzotti N, Höllt T, Lelieveldt B, Eisenmann E, Vilanova A: Hierachical Stochastic Neighbour Embedding. Eurographics Conference on Visualization (EuroVis) 2016
\bibitem{lib:cyteguide} Höllt T, Penzzotti N, van Unen V, Koning F, Lelieveldt B, Vilanova A: CyteGuide: Visual Guidance for Hierarchical Single-Cell Analysis. 
\bibitem{lib:flann} Muja M, Lowe D G: Scalable Nearest Neighbour Algorithms for High Dimensional Data. IEEE Transactions on Pattern Analysis and Machine Intelligence (2014)
\bibitem{lib:barnes-hut} van der Maaten L: Barnes-Hut-SNE (2013) vol. 1301.3342 
\bibitem{lib:monte-carlo} Geyer C: Introduction to markov chain monte carlo. Handbook of Markov Chain Monte Carlo (2011), 3-48. 4
\end{thebibliography}

\end{document}
